# Variational Meta Reinforcement Learning

Companion code of the RBF-PEARL implementation for the paper: [Variational Meta Reinforcement Learning for Social Robotics](https://dl.acm.org/doi/10.1007/s10489-023-04691-5).

Authors: [Anand BALLOU](https://team.inria.fr/robotlearn/team-members/anand-ballou/), [Chris Reinke](https://www.scirei.net/), [Xavier Alameda-Pineda](http://xavirema.eu/)

Copyright: [INRIA](https://www.inria.fr/fr), 2022

License: [GNU General Public License v3.0 or later](https://gitlab.inria.fr/robotlearn/varmetarl/-/blob/main/LICENSE)

Contact: [Anand Ballou](anand.ballou@inria.fr)

## Abstract

With the increasing presence of robots in our every-day environments, improving their social skills is of utmost importance. Nonetheless, social robotics still faces many challenges. One bottleneck is that robotic behaviors need to be often adapted as social norms depend strongly on the environment. For example, a robot should navigate more carefully around patients in a hospital compared to workers in an office. In this work, we investigate meta-reinforcement learning (meta-RL) as a potential solution. Here, robot behaviors are learned via reinforcement learning where a reward function needs to be chosen so that the robot learns an appropriate behavior for a given environment. We propose to use a variational meta-RL procedure that quickly adapts the robots' behavior to new reward functions. As a result, given a new environment different reward functions can be quickly evaluated and an appropriate one selected. The procedure learns a vectorized representation for reward functions and a meta-policy that can be conditioned on such a representation. Given observations from a new reward function, the procedure identifies its representation and conditions the meta-policy to it. While investigating the procedures' capabilities, we realized that it suffers from posterior collapse where only a subset of the dimensions in the representation encode useful information resulting in a reduced performance. Our second contribution, a radial basis function (RBF) layer, partially mitigates this negative effect. The RBF layer lifts the representation to a higher dimensional space, which is more easily exploitable for the meta-policy. We demonstrate the interest of the RBF layer and the usage of meta-RL for social robotics on four robotic simulation tasks. 


## Installation

Requirements:
 - Python 3.8 or higher
 - Linux (developed under Ubuntu 18.04) or MacOS

To run experiments all python packages under the *varmetarl* folder must be installed.
It is recommended to install the package in developer mode (*-e*), so that changes to the source code can be directly used without the need to reinstall the package:

`pip install -e ./src/varmetarl`

`pip install -e ./src/socialgym`


## Overview

The source code has 2 components:

 - varmetarl package: The varmetarl package contains the code of the different version of PEARL and Multitask-SAC used for the experiments.

 - socialgym package: The socialgym package contains the code of the three environments used in the paper.
  
## Requirements

To run the experiments, you need to install the following packages:

- https://gitlab.inria.fr/spring/wp6_robot_behavior/multiparty_interaction_simulator
  
## BibTeX Citation

If you use VarMetaRL implementation in a scientific publication, we would appreciate using the following citations:

```
@article{ballou2023variational,
  title={Variational meta reinforcement learning for social robotics},
  author={Ballou, Anand and Alameda-Pineda, Xavier and Reinke, Chris},
  journal={Applied Intelligence},
  volume={53},
  number={22},
  pages={27249--27268},
  year={2023},
  publisher={Springer}
}
```

