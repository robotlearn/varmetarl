from gymnasium.envs.registration import register

# Hook to load plugins from entry points

# robotics
# ----------------------------------------
register(
    id="TwodRacer-v0",
    entry_point="social_gym.envs.robotics.rbf_twod_infinite_track_racer:RBFStateTwoDInfiniteTrackRacerContinuousActionEnv",
    max_episode_steps=200,
)

register(
    id="TwodObjectCollection-v0",
    entry_point="social_gym.envs.robotics.rbf_twod_object_collection:RBFStateTwoDObjectCollectionContinuousActionEnv",
    max_episode_steps=200,
)

register(
    id="GazeControl-v0",
    entry_point="social_gym.envs.robotics.synthetic:SyntheticEnv",
    max_episode_steps=1024,
)

register(
    id="GazeControlRandom-v0",
    entry_point="social_gym.envs.robotics.synthetic_random_reward:SyntheticEnv",
    max_episode_steps=1024,
)

register(
    id="SocialNavigation-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"curriculum_learning": "distance_to_goal"}
)


register(
    id="TestSocialNavigation-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"test": True}
)#ToDo: replace with a wrapper for testing

register(
    id="SocialNavigationImprovement-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"termination_reward": 'improvement', "curriculum_learning": "distance_to_goal"}
)

register(
    id="TestSocialNavigationImprovement-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"test": True, "termination_reward": 'improvement'}
)

register(
    id="SocialNavigationImprovementFullInfo-v0",
    entry_point="social_gym.envs.robotics.minimal_mpi_env_no_map:MpiEnvNoMap",
    max_episode_steps=200,
    kwargs={"termination_reward": 'improvement', "curriculum_learning": "distance_to_goal"}
)

register(
    id="TestSocialNavigationImprovementFullInfo-v0",
    entry_point="social_gym.envs.robotics.minimal_mpi_env_no_map:MpiEnvNoMap",
    max_episode_steps=200,
    kwargs={"test": True, "termination_reward": 'improvement'}
)

register(
    id="ScenarioSimulation-v0",
    entry_point="social_gym.envs.robotics.scenario_simulation:ScenarioSimulationEnv",
    max_episode_steps=200,
)

register(
    id="SocialNavigationNoHuman-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"n_max_humans": 0},
)

register(
    id="SocialNavigationNoTerminationReward-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"termination_reward": None},
)

register(
    id="SocialNavigationNoTerminationRewardNoHuman-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"n_max_humans": 0, "termination_reward": None},
)

register(
    id="SocialNavigationCurriculumLearning-v0",
    entry_point="social_gym.envs.robotics.social_navigation:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"curriculum_learning": "distance_to_goal"},
)
# multitasks
# ----------------------------------------

register(
    id="MultiTaskTwodRacer-v0",
    entry_point="social_gym.envs.multitasks.multitask_track_racer:RacerEnv",
    max_episode_steps=200,
    kwargs={"n_tasks": 120}
)

register(
    id="MultiTaskGazeControl-v0",
    entry_point="social_gym.envs.multitasks.multitask_synthetic:SyntheticEnv",
    max_episode_steps=1024,
    kwargs={"n_tasks": 120}
)

register(
    id="MultiTaskGazeControlRandom-v0",
    entry_point="social_gym.envs.multitasks.multitask_synthetic_random_reward:SyntheticEnv",
    max_episode_steps=1024,
    kwargs={"n_tasks": 120}
)

register(
    id="MultiTaskSocialNavigation-v0",
    entry_point="social_gym.envs.multitasks.multitask_socialnav:SocialNavigationEnv",
    max_episode_steps=200,
    kwargs={"n_tasks": 120}
)