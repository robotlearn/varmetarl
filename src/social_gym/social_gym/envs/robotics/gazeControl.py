import numpy as np

import gymnasium as gym
from gymnasium import spaces
import random

class GazeControlEnv(gym.Env):

    #######################
    ### Required by gym ###
    #######################
    def __init__(self):
        self.setPrimaryVariables()
        self.setSecondaryVariables()

    def setPrimaryVariables(self):
        """
        Primary variables to be set for any GazeControl environment.
        """
        self.A_FOV_L = 0.0
        self.A_FOV_T = 0.0
        self.A_FOV_R = 1.0
        self.A_FOV_B = 1.0

        self.V_FOV_WIDTH = 0.3
        self.V_FOV_HEIGHT = 0.3

        self.OBS_SSL_WIDTH = 7
        self.OBS_SSL_HEIGHT = 4
        self.OBS_SSL_DIM = 1
        self.OBS_FOV_WIDTH = 7
        self.OBS_FOV_HEIGHT = 7
        self.OBS_FOV_DIM = 1
        self.OBS_FOV_POS = 2

        self.NB_ACTION_HORIZONTAL = 10
        self.NB_ACTION_VERTICAL = 6

        self.NB_ACTIONS = 5

    def setSecondaryVariables(self):
        """
        Secondary variables to be set for any GazeControl environment.
        These variables are computed from primary variables.
        """

        self.x = (self.A_FOV_L + self.A_FOV_R) / 2.0
        self.y = (self.A_FOV_T + self.A_FOV_B) / 2.0

        self.DX = (self.A_FOV_R - self.A_FOV_L - self.V_FOV_WIDTH) / \
            float(self.NB_ACTION_HORIZONTAL)
        self.DY = (self.A_FOV_B - self.A_FOV_T - self.V_FOV_HEIGHT) / \
            float(self.NB_ACTION_VERTICAL)

        self.MIN_X = self.A_FOV_L + self.V_FOV_WIDTH / 2
        self.MAX_X = self.A_FOV_R - self.V_FOV_WIDTH / 2
        self.MIN_Y = self.A_FOV_T + self.V_FOV_HEIGHT / 2
        self.MAX_Y = self.A_FOV_B - self.V_FOV_HEIGHT / 2

        self.facesSize = self.OBS_FOV_WIDTH * self.OBS_FOV_HEIGHT * self.OBS_FOV_DIM
        self.SSLSize = self.OBS_SSL_WIDTH * self.OBS_SSL_HEIGHT * self.OBS_SSL_DIM
        self.posSize = self.OBS_FOV_POS
        self.obsSize = self.facesSize + self.SSLSize + self.posSize
        high = np.ones((self.obsSize,))
        low = np.zeros((self.obsSize,))

        self.action_space = spaces.Box(low=np.array([-1.0, -1.0]), high=np.array([1.0, 1.0]), dtype=np.float32)
        self.observation_space = spaces.Box(low, high, dtype=np.float32)

    def getTLBR(self):
        """
        Compute FOV limits.
        """
        left = self.x - self.V_FOV_WIDTH / 2
        top = self.y - self.V_FOV_HEIGHT / 2
        right = self.x + self.V_FOV_WIDTH / 2
        bottom = self.y + self.V_FOV_HEIGHT / 2
        return top, left, bottom, right

    # #######################
    # ### Required by gym ###
    # #######################
    # def seed(self, seed=None):
    #     self.np_random, seed = seeding.np_random(seed)
    #     return [seed]

    #######################
    ### Required by gym ###
    #######################
    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        self.setInitialPosition()
        reward, obs, *_ = self.getRewardAndObservation()
        return obs

    def setInitialPosition(self):
        """
        Set an initial FOV position respecting the environment constraints.
        """
        self.y = random.uniform(self.MIN_Y, self.MAX_Y)
        self.x = random.uniform(self.MIN_X, self.MAX_X)
        self.previous_x = self.x
        self.previous_y = self.y
        self.direction = np.array([0, 0])
        self.previous_direction = np.array([0, 0])

    #######################
    ### Required by gym ###
    #######################
    def step(self, action):
        self.move(action)
        reward, obs = self.getRewardAndObservation()
        done = False
        return obs, reward, done, False, {}

    def move(self, action):
        """
        Compute the new position after an action
        """
        self.previous_direction = self.direction
        self.previous_x = self.x
        self.previous_y = self.y
        #print("\n----DEBUT----")
        self.x = max(min(action[0]*self.DX + self.x, self.MAX_X), self.MIN_X)
        #print(action[0]*self.DX)
        self.y = max(min(action[1]*self.DY + self.y, self.MAX_Y), self.MIN_Y)
        #print(action[1]*self.DY)
        #print("\n----FIN----")
        current = np.array([self.x, self.y])
        previous = np.array([self.previous_x, self.previous_y])
        self.direction = current-previous

    def getRewardAndObservation(self):
        """
        Compute the environment reward and observations.
        MUST BE ADAPTED TO EACH ENVIRONMENT !
        """
        raise NotImplementedError

    #######################
    ### Required by gym ###
    #######################
    def render(self):
        raise NotImplementedError
    
    
if __name__ == "__main__":
    env = GazeControlEnv()
    env.reset()
    for _ in range(1000):
        #env.render()
        env.step(env.action_space.sample())
    env.close()