import exputils as eu
from social_gym.envs.robotics.syntheticFB import SyntFBEnv

class SyntheticEnv(SyntFBEnv):
    
    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.set_reward()
        self.num_features = 3

    def getReward(self):
        """
        The reward is the sum of the visual, audio, and movement rewards, each weighted by a constant
        :return: The reward, the visual reward, the audio reward, and the movement reward.
        """
        reward_vis = self.visual_reward()
        reward_aud = self.audio_reward()
        reward_mov, dir_reward = self.movement_reward()
        reward = self._vis * reward_vis + self._aud * reward_aud + \
            self._mov * reward_mov + self._mov * dir_reward
        return reward, reward_vis, reward_aud, reward_mov + dir_reward
    
    def set_reward(self, config = None):
        """
        > The function `set_reward` sets the reward for the agent
        
        :param config: a dictionary of parameters
        """
        default_config = eu.AttrDict(
            vis = 0.1,
            aud = 0.8,
            mov = 0.1
        )
        config = eu.combine_dicts(config, default_config)   
        self._vis = config.vis
        self._aud = config.aud
        self._mov = config.mov
        self._goal = (self._vis, self._aud, self._mov)       
    
if __name__ == "__main__":
    env = SyntheticEnv()
    env.reset()
    for _ in range(1000):
        #env.render()
        env.step(env.action_space.sample())
    env.close()