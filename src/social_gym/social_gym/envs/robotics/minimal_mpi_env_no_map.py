import os
import time
import uuid
import gymnasium as gym
import exputils as eu
import math
import numpy as np
import mpi_sim
import mpi_sim.core.module_library as modlib
import cv2
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap
import matplotlib.animation as animation
import random as random
from social_gym.envs.robotics.minimal_mpi_env import MpiEnv

class MpiEnvNoMap(MpiEnv):

    metadata = {
    "render_modes": ["dict", "human", "rgb_array"],
}
    
    def  __init__(self, config=None, render_mode = None, **kwargs):
        
        super().__init__(config=config, render_mode=render_mode)
        self.observation_space =  gym.spaces.Box(low=-1, high=1, shape=(self.config.n_max_humans*3+2,))
        self.set_reward()
        self.num_features = 5
        
    def get_observation(self):
        
        observation = np.hstack([np.append(human.position, human.orientation)
            for human in self.simulation.get_objects_by_type(modlib.get_object('Human'))])
        observation = np.hstack((observation, self.compute_input_features().reshape(2,)))
        
        return observation
    
    def get_reward(self):
        goal_reward = self.get_goal_reward()
        collision_reward = self.get_collision_reward()
        social_reward = self.get_social_reward()
        approaching_reward = self.get_approaching_reward()
        velocity_reward = self.get_velocity_reward()
        reward = self.weight_goal*goal_reward + \
                self.weight_collision*collision_reward + \
                self.weight_social*social_reward + \
                self.weight_approaching*approaching_reward + \
                self.weight_velocity*velocity_reward

        return reward, goal_reward, collision_reward, social_reward, approaching_reward, velocity_reward        
    
    def set_reward(self, config = None):
        """
        > The function `set_reward` sets the reward for the agent
        
        :param config: a dictionary of parameters
        """
        default_config = eu.AttrDict(
            weight_goal = 0.6,
            weight_collision = 0.1,
            weight_social = 0.1,
            weight_approaching = 0.1,
            weight_velocity = 0.1
        )
        config = eu.combine_dicts(config, default_config)   
        self.weight_goal = config.weight_goal
        self.weight_collision = config.weight_collision
        self.weight_social = config.weight_social
        self.weight_approaching = config.weight_approaching
        self.weight_velocity = config.weight_velocity
        self._goal = (self.weight_goal, self.weight_collision, \
            self.weight_social, self.weight_approaching, self.weight_velocity)           