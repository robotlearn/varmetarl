"""
The class `SyntheticEnv` is a subclass of the class `SyntFBEnv` that implements a reward function
that is a combination of the visual, audio, and movement rewards
"""
import exputils as eu
from social_gym.envs.robotics.syntheticFB import SyntFBEnv

class SyntheticEnv(SyntFBEnv):
    def __init__(self, config=None, **kwargs):
        super(SyntheticEnv, self).__init__(config=config, **kwargs)
        self.set_reward()
        self.num_features = 3

    def getReward(self):
        """
        The reward is a function  of the visual, audio, and movement rewards
        :return: The reward, the visual reward, the audio reward, and the movement reward.
        """
        reward_vis = self.visual_reward()
        reward_aud = self.audio_reward()
        reward_mov, dir_reward = self.movement_reward()
        reward = self._reward(
            [reward_vis, reward_aud, -reward_mov - dir_reward])
        return reward, reward_vis, reward_aud, reward_mov, dir_reward
       
    def set_reward(self, config=None):
        """
        > The function `set_reward` takes in a configuration dictionary and sets the reward function to
        the reward function specified in the configuration dictionary. If no reward function is
        specified, it sets the reward function to the default reward function
        
        :param config: a dictionary of parameters
        """
        default_config = eu.AttrDict(
            reward = lambda x,y,z : 0.33*x + .33*y + .33*z
        )
        config = eu.combine_dicts(config, default_config)   
        self._reward = config.reward_function
        self._goal = self._reward