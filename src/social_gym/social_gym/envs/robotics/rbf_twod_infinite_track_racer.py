import numpy as np
import gymnasium as gym
import exputils as eu
from social_gym.envs.robotics.twod_infinite_track_racer import TwoDInfiniteTrackRacerContinuousActionEnv


def sample_component_independent_rfunc(n_markers, config=None, **kwargs):

    default_config = eu.AttrDict(
        n_possible_gauss_components=[1, 1],
        mu_range=[0.0, 0.7],
        sigma_range=[0.001, 0.01],
        weight_range=[1.0, 1.0],
        
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    def sample_range(range_def):
        return range_def[0] + (range_def[1] - range_def[0]) * np.random.rand()

    # placeholders: mu, sigma
    gauss_template = 'np.exp(-(<VALUE> - {:.3f})**2 / {:.5f})'

    component_functions = []
    component_strings = []
    components = []

    # marker distance features
    for feature_dim in range(n_markers):

        n_gauss_components = np.random.choice(
            config.n_possible_gauss_components)

        weight = sample_range(config.weight_range) / n_markers

        component_str = '{:.3f} * np.max(['.format(weight)

        for gauss_component_idx in range(n_gauss_components):

            mu = sample_range(config.mu_range)
            sigma = sample_range(config.sigma_range)
            
            component_str += gauss_template.format(
                mu,
                sigma
            )

            components.append(mu)
            components.append(sigma)
            
            if gauss_component_idx < n_gauss_components - 1:
                component_str += ', '

        component_str += '])'

        component_functions.append(
            eval('lambda feature_val: ' + component_str.replace('<VALUE>', 'feature_val')))
        component_strings.append(component_str.replace(
            '<VALUE>', 'features[{}]'.format(feature_dim)))

    # create final reward function from components
    lambda_str = 'lambda features: ' + ' + '.join(component_strings)
    r_func = eval(lambda_str)

    return r_func, lambda_str, component_functions, tuple(components)


class RBFStateTwoDInfiniteTrackRacerContinuousActionEnv(TwoDInfiniteTrackRacerContinuousActionEnv):
    """
    Infinite Racer with continuous actions.
    """

    @staticmethod
    def default_config():
        dc = TwoDInfiniteTrackRacerContinuousActionEnv.default_config()

        dc.observation_space = eu.AttrDict()

        # create list of all components for the gaussian map
        dim1_components = np.linspace(dc.area[0][0], dc.area[0][1], 10)
        dim2_components = np.linspace(dc.area[1][0], dc.area[1][1], 10)
        dc.observation_space.position_components = np.array(
            np.meshgrid(dim1_components, dim2_components)).T.reshape(-1, 2)
        dc.observation_space.position_components_sigma = 0.1

        dc.observation_space.direction_components = np.linspace(
            -np.pi, np.pi, num=20, endpoint=False)
        dc.observation_space.direction_components_sigma = np.pi*0.2

        return dc

    def __init__(self, config=None, **kwargs):

        super().__init__(config=config, **kwargs)

        self.observation_space = gym.spaces.Box(
                low=-np.inf,
                high=np.inf,
                shape=(self.config.observation_space.position_components.shape[0]
                       + self.config.observation_space.direction_components.shape[0] + 1,)
        )
        self.set_reward()
        self.num_features = 3

    def get_obs(self, agent_state=None):
        """
        Returns the associated observation for the internal state defined by the x,y position of the agent and if certain objects still exists.

        :param agent_state: numpy array with [x, y, angle] of the agent
        :return: observation as numpy array
        """

        obs = super(RBFStateTwoDInfiniteTrackRacerContinuousActionEnv,
                    self).get_obs(agent_state=agent_state)

        if agent_state is None:
            agent_pos = self.state_pos
            agent_angle = self.state_angle
        else:
            agent_pos = agent_state[:2]
            agent_angle = agent_state[2]

        # compute gaussian map
        diff_x, diff_y = self.calc_min_difference(
            agent_pos, self.config.observation_space.position_components)

        rbf_pos = np.exp(-1 * (diff_x**2 + diff_y**2)
                         / self.config.observation_space.position_components_sigma)

        # get objects that have been picked up

        angle_distance = np.abs(
            agent_angle - self.config.observation_space.direction_components)
        angle_distance = np.minimum(angle_distance, 2*np.pi - angle_distance)

        rbf_angle = np.exp(-1 * angle_distance**2
                           / self.config.observation_space.position_components_sigma)

        # concatenate both with a an constant term
        rbf_state = np.concatenate((rbf_pos, rbf_angle, [1.0]))

        obs['observation'] = rbf_state

        return obs

    def set_reward(self, config=None):
        self.reward_function, lambda_str, component_functions, components = sample_component_independent_rfunc(
                self.config.markers.shape[0], config=config)
        print(self.reward_function, lambda_str, component_functions, components)

if __name__ == "__main__":
    env = RBFStateTwoDInfiniteTrackRacerContinuousActionEnv()
    env.reset()
    for _ in range(1000):
        #env.render()
        env.step(env.action_space.sample())
    env.close()