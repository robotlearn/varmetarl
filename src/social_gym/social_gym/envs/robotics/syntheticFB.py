import json
import os
import uuid

import cv2
import numpy as np
import exputils as eu

from social_gym.envs.robotics.gazeControl import GazeControlEnv
from social_gym.lib.data.generator import SyntheticGenerator
from social_gym.lib.poseEstimation.utils import annotateImage
from social_gym.lib.utils import VideoMaker

def vis_time(x):
    return 1 * np.exp(-x)

def vis_distance(x):
    return 0

def mov_penalty(x, y):
    return np.sqrt(np.sum((x - y)**2))

class SyntFBEnv(GazeControlEnv):

    metadata = {
        "render_modes": ["video"],
    }
    @staticmethod
    def default_config():
        dc = eu.AttrDict()

        dc.vis_max = 2.0
        dc.vis_time = vis_time
        dc.vis_distance = vis_distance
        
        dc.aud_max = 2.0
        dc.aud_penalty = -0.5

        dc.mov_max = 16.66
        dc.mov_penalty = mov_penalty

        dc.detectAccuracy = 0.99
        dc.sslAccuracy = 0.99
        dc.MaxNbPeople = 2
        dc.pMoving = 0.9
        dc.pStaying = 0.2

        return dc

    def __init__(self, config= None, render_mode=None, **kwargs):
        super(SyntFBEnv, self).__init__()

        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.generator = SyntheticGenerator(
            self.config.MaxNbPeople, self.config.pMoving, self.config.pStaying)
        # Saving videos (variables used here, in _reset and in _render)
        self.savedVideo = None
        self.episode_reward = []

        assert render_mode is None or render_mode in self.metadata["render_modes"]
        self.render_mode = render_mode

    def setPrimaryVariables(self):
        super(SyntFBEnv, self).setPrimaryVariables()

        # 640x480: Nao resolution
        self.camResWidth = 640
        self.camResHeight = 480
        self.panoramaWidth = 3384
        self.panoramaHeight = 1632

        self.V_FOV_WIDTH = float(self.camResWidth) / self.panoramaWidth
        self.V_FOV_HEIGHT = float(self.camResHeight) / self.panoramaHeight

        self.OBS_SSL_WIDTH = 14
        self.OBS_SSL_HEIGHT = 8

        self.OBS_FOV_DIM = 18

        self.NB_ACTION_HORIZONTAL = 18
        self.NB_ACTION_VERTICAL = 6

    # def set_reward_parameters(self, json_file):

    #     with open(json_file) as f:
    #         data = json.load(f)
    #     self.vis = data["vis"]["weight"]
    #     self.vis_max = data["vis"]["max"]
    #     if data["vis"]["time"] == "exponential":
    #         self.vis_time = lambda x: data["vis"]["time_coeff"]*np.exp(-x)
    #     else:
    #         self.vis_time = lambda x:0
    #     if data["vis"]["distance"] == "exponential":
    #         self.vis_distance = lambda x:1-np.exp(-x)
    #     else:
    #         self.vis_distance = lambda x:0

    #     self.aud = data["aud"]["weight"]
    #     self.aud_max = data["aud"]["max"]
    #     self.aud_penalty = data["aud"]["penalty"]

    #     self.mov = data["mov"]["weight"]
    #     self.mov_max = data["mov"]["max"]
    #     if data["mov"]["penalty"] == "square_norm":
    #         self.mov_penalty = lambda x, y: np.sqrt(np.sum((x - y)**2))
    #     else:
    #         self.mov_penalty = lambda x, y:0

    def step(self, action):
        if self.action_space.contains(action):
            self.move(action)
            t, l, b, r = self.getTLBR()
            self.generator.step(t, l, b, r)
            reward, obs, *all_reward = self.getRewardAndObservation()
            done = False
            info = {"features": all_reward, "x": self.x, "y": self.y}

            if self.render_mode == "video":
                self.render()
            return obs, reward, done, False, info
        else:
            raise ValueError("action value out of bound" + str(action))
        # self.timeCurrentEpisode+=1

    def getRewardAndObservation(self):

        t, l, _, _ = self.getTLBR()

        # Scale x,y so that each value in [0,1] is possible for both
        scaled_x = (self.x - self.MIN_X) / (self.MAX_X - self.MIN_X)
        scaled_y = (self.y - self.MIN_Y) / (self.MAX_Y - self.MIN_Y)

        pos = np.array([scaled_x, scaled_y])
        ssl = self.generator.getBlurredSSL(
            self.OBS_SSL_WIDTH, self.OBS_SSL_HEIGHT, self.config.sslAccuracy)
        vis = self.generator.getVisual(self.OBS_FOV_WIDTH, self.OBS_FOV_HEIGHT, self.OBS_FOV_DIM,
                                       self.panoramaWidth, self.panoramaHeight, t, l,
                                       self.V_FOV_HEIGHT, self.V_FOV_WIDTH, self.config.detectAccuracy)

        obs = np.append(ssl, np.append(np.stack(vis, axis=2), pos))

        reward, r_obs, r_aud, r_mov = self.getReward()

        return reward, obs, r_obs, r_aud, r_mov


    def getReward(self):
        reward_vis = self.visual_reward()
        reward_aud = self.audio_reward()
        reward_mov, dir_reward = self.movement_reward()
        reward = 0.33 * reward_vis + 0.33 * reward_aud + 0.33 * reward_mov + 0.33 * dir_reward
        return reward, reward_vis, reward_aud, reward_mov + dir_reward


    def visual_reward(self):
        t, l, b, r = self.getTLBR()
        reward = 0
        for person in self.generator.people:
            if person.isVisible(t, l, b, r):
                time_weight = self.config.vis_time(self.generator.time - person.last_time)
                distance = np.sqrt(
                    np.sum((np.array([person.i, person.j]) - np.array(person.last_position))**2))
                distance_weight = self.config.vis_distance(distance)
                reward += self.config.vis_max - time_weight * (1 - distance_weight)
                person.last_time = self.generator.time
                person.last_position = (person.i, person.j)
        return reward

    def audio_reward(self):
        t, l, b, r = self.getTLBR()
        reward = 0
        for person in self.generator.people:
            if person.speaking and person.inField():
                if person.isVisible(t, l, b, r):
                    reward += self.config.aud_max
                else:
                    reward += self.config.aud_penalty
        return reward

    def movement_reward(self):
        """
        add direction reward to jsson file
        """
        current = np.array([self.x, self.y])
        previous = np.array([self.previous_x, self.previous_y])
        mov_reward = -self.config.mov_max * self.config.mov_penalty(current, previous)
        change = self.direction * self.previous_direction < 0
        if change.any():
            dir_reward = -.5
        else:
            dir_reward = 0
        return mov_reward, dir_reward

    # def render(self, mode='human', close=False, name_video=""):
    def render(self):

        t, l, b, r = self.getTLBR()
        # Convert to pixel coordinates
        left_top = int(l * self.panoramaWidth), int(t * self.panoramaHeight)
        right_bottom = int(
            r * self.panoramaWidth), int(b * self.panoramaHeight)
        center = int(self.x * self.panoramaWidth), int(self.y *
                                                       self.panoramaHeight)

        displayedImage = np.zeros(
            (self.panoramaHeight, self.panoramaWidth, 3), dtype=np.uint8)
        cv2.rectangle(displayedImage, left_top,
                      right_bottom, (255, 255, 255), 6)
        cv2.circle(displayedImage, center, 2, (255, 255, 255))

        skeletons = []
        for pers in self.generator.people:
            skeletons.append(pers.getSkeleton(
                self.panoramaWidth, self.panoramaHeight))
        displayedImage = annotateImage(displayedImage, None, skeletons)
        for pers in self.generator.people:
            if pers.speaking:
                cv2.circle(displayedImage, (int(pers.j * self.panoramaWidth),
                                            int(pers.i * self.panoramaHeight)), 50, (255, 0, 255), 10)
        if self.savedVideo is None:
            if name_video == "":
                name_video = str(uuid.uuid4())
            videoPath = "SyntFBEnv/" + name_video + ".avi"
            self.savedVideo = VideoMaker(videoPath, fps=10.0)
        self.savedVideo.add(displayedImage)

    def reset(self, seed=None, options=None):
        obs = super().reset(seed=seed, options=options)
        if self.savedVideo is not None:
            if not os.path.exists("./video/SyntFBEnv/"):
                os.makedirs("./video/SyntFBEnv/")
            self.savedVideo.write()
            self.savedVideo = None
        self.generator.reset(self.config.MaxNbPeople)
        if self.render_mode == "video":
            self.render()
        return obs, {}

    def close(self):
        if self.savedVideo is not None:
            if not os.path.exists("./video/SyntFBEnv/"):
                os.makedirs("./video/SyntFBEnv/")
            self.savedVideo.write()
            self.savedVideo = None

        super().close()
