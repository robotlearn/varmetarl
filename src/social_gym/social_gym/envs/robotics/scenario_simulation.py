import os
import time
import uuid
import gymnasium as gym
import exputils as eu
import math
import numpy as np
import mpi_sim
import mpi_sim.core.module_library as modlib
import cv2
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap
import matplotlib.animation as animation
import random as random
from .minimal_mpi_env import MpiEnv

class ScenarioSimulationEnv(MpiEnv):
    
    metadata = {
    "render_modes": ["dict", "human", "rgb_array"],
}
        
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # Common parameters
            goal_position_radius = 0.3,
            
            # Humans parameters
            groups = [],
            
            humans = [],
            
            start_goal_infos = eu.AttrDict(
                    start_position = [-2.5, -2.5],
                    start_orientation = 0,
                    goal_position = [2.5, 2.5],
                    goal_orientation = 0.,
                ),
            termination_reward = 'final',
            curriculum_learning = None,
            reward_config = eu.AttrDict(
                social_space_threshold = 1,
                visibility_threshold = np.pi/3,
                disturbance_thresold = 3,
                target_speed = 0.75
            )            
        )   
        
    def  __init__(self, config=None, render_mode = None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        assert render_mode is None or render_mode in self.metadata["render_modes"]
        print(render_mode)

        self.render_mode = render_mode
         # Area to show in the global map 
        self.area = ((-3.2, 3.2), (-3.2, 3.2))
        
        world_config = eu.AttrDict(visible_area = self.area,
                                   objects = [
                    {'type': 'Wall', 'position': [-3.2, 0], 'orientation': np.pi, 'height': 6.4},
                    {'type': 'Wall', 'position': [3.2, 0], 'orientation': 0., 'height': 6.4},
                    {'type': 'Wall', 'position': [0., 3.2], 'orientation': np.pi / 2, 'height': 6.4},
                    {'type': 'Wall', 'position': [0., -3.2], 'orientation': -np.pi / 2, 'height': 6.4}
                ],
        )
        
        if render_mode == "human":
            world_config['processes'] = [{'type': 'GUI'}]
            
        # create simulation
        self.simulation = mpi_sim.Simulation(world_config = world_config)                   
        self.agent = mpi_sim.objects.ARIRobot(
            components=[
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='human_map',
                    object_filter = dict(types=mpi_sim.objects.Human),
                    depth = 3.0,
                    resolution = 0.2,
                    ),
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='robot_map',
                    object_filter = dict(types=mpi_sim.objects.ARIRobot),
                    depth = 3.0,
                    resolution = 0.2,
                    ),
            ],
                position = self.config.start_goal_infos.start_position,
                orientation = self.config.start_goal_infos.start_orientation
        )
        self.start_orientation = self.config.start_goal_infos.start_orientation
        
        self.simulation.add_object(self.agent)
        
        for human in self.config.humans:
            self.simulation.add_object(human)
            
        for group in self.config.groups:
            self.simulation.add_script(group)
            
        self.simulation.set_reset_point()

        self.observation_space =  gym.spaces.Dict(
            occupancy_grid = gym.spaces.Box(#todo change shape
                low=0, high=1, shape=(3, *self.agent.robot_map.global_map.shape)
            ),
            input_for_FC_net = gym.spaces.Box(low=-1, high=1, shape=(2,))
        )
        
        self.action_space = gym.spaces.Box(low=-1, high=1, shape=(2,))
        self.reward_range = (-1, 1)#todo change
        
        self.goal_position = self.config.start_goal_infos.goal_position
        self.former_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
        
    def reset(self, seed = None, options=None):
        
        # super().super().reset(seed=seed)
            
        self.simulation.reset()    
        
        observation = self.get_observation()
        
        self.simulation.step()
        
        info = self.get_info([])
        
        self.former_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            
        return observation, info
    
    def step(self, action):
        
        self.agent.forward_velocity = action[0] + 1
        self.agent.orientation_velocity = action[1]
        
        step_loop_scaling = 2
        for _ in range(step_loop_scaling):
            self.simulation.step()
            robot_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            if robot_distance_to_goal < self.config.goal_position_radius:
                terminated = True
                break
            
        else:
            robot_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            terminated = False
            
        reward, *features = self.get_reward()           
        
        observation = self.get_observation()
                                      
        info = self.get_info(features)
        
        self.former_distance_to_goal = robot_distance_to_goal
            
        return observation, reward, terminated, False, info