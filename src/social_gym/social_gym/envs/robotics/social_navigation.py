import exputils as eu
import mpi_sim
from social_gym.envs.robotics.minimal_mpi_env import MpiEnv

class SocialNavigationEnv(MpiEnv):
    
    def __init__(self, config=None, **kwargs):
        super().__init__(config=config, **kwargs)
        self.set_reward()
        self.num_features = 5
        
    def get_reward(self):
        goal_reward = self.get_goal_reward()
        collision_reward = self.get_collision_reward()
        social_reward = self.get_social_reward()
        approaching_reward = self.get_approaching_reward()
        velocity_reward = self.get_velocity_reward()
        reward = self.weight_goal*goal_reward + \
                self.weight_collision*collision_reward + \
                self.weight_social*social_reward + \
                self.weight_approaching*approaching_reward + \
                self.weight_velocity*velocity_reward

        return reward, goal_reward, collision_reward, social_reward, approaching_reward, velocity_reward        
    
    def set_reward(self, config = None):
        """
        > The function `set_reward` sets the reward for the agent
        
        :param config: a dictionary of parameters
        """
        default_config = eu.AttrDict(
            weight_goal = 0.6,
            weight_collision = 0.1,
            weight_social = 0.1,
            weight_approaching = 0.1,
            weight_velocity = 0.1
        )
        config = eu.combine_dicts(config, default_config)   
        self.weight_goal = config.weight_goal
        self.weight_collision = config.weight_collision
        self.weight_social = config.weight_social
        self.weight_approaching = config.weight_approaching
        self.weight_velocity = config.weight_velocity
        self._goal = (self.weight_goal, self.weight_collision, \
            self.weight_social, self.weight_approaching, self.weight_velocity)       