import os
import time
import uuid
import gymnasium as gym
import exputils as eu
import math
import numpy as np
import mpi_sim
import mpi_sim.core.module_library as modlib
import cv2
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap
import matplotlib.animation as animation
import random as random

class SpeedOnly(gym.Env):
    
    metadata = {
    "render_modes": ["dict", "human", "rgb_array"],
}
    @staticmethod
    def default_config():
        return eu.AttrDict(            
           # Parameters about ARI
            random_start_position = True,
            start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                    start_position = [-2.5, -2.5],
                    start_orientation = 0
                ),
            humans_start_region = [[-2.8, 2.8], [-2.8, 2.8]], # [[x_min, x_max], [y_min, y_max]]
            humans_goal_region = [[-2.8, 2.8], [-2.8, 2.8]], # [[x_min, x_max], [y_min, y_max]]
            human_goal_position_radius = 0.05,                 
        )
        
    def __init__(self, config=None, render_mode = None, **kwargs):
        
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.episodes = 0
        
        assert render_mode is None or render_mode in self.metadata["render_modes"]
        print(render_mode)

        self.render_mode = render_mode

         # Area to show in the global map 
        self.area = ((-3.2, 3.2), (-3.2, 3.2))
        
        world_config = eu.AttrDict(visible_area = self.area,
                                   objects = [
                    {'type': 'Wall', 'position': [-3.2, 0], 'orientation': np.pi, 'height': 6.4},
                    {'type': 'Wall', 'position': [3.2, 0], 'orientation': 0., 'height': 6.4},
                    {'type': 'Wall', 'position': [0., 3.2], 'orientation': np.pi / 2, 'height': 6.4},
                    {'type': 'Wall', 'position': [0., -3.2], 'orientation': -np.pi / 2, 'height': 6.4}
                ]
        )
        
        if render_mode == "human":
            world_config['processes'] = [{'type': 'GUI'}]
            
        # create simulation
        self.simulation = mpi_sim.Simulation(world_config = world_config)
                  
        self.agent = mpi_sim.objects.ARIRobot(
            components=[
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='human_map',
                    object_filter = dict(types=mpi_sim.objects.Human),
                    depth = 3.0,
                    resolution = 0.2,
                    ),
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='robot_map',
                    object_filter = dict(types=mpi_sim.objects.ARIRobot),
                    depth = 3.0,
                    resolution = 0.2,
                    ),
            ],
                position = self.config.start_goal_infos.start_position,
                orientation = self.config.start_goal_infos.start_orientation
        )
        
        self.simulation.add_object(self.agent)
        self.simulation.set_reset_point()
        # shape = 2 + self.config.n_max_humans*4
        # self.observation_space =  gym.spaces.Box(low=-10, high=10, shape=(2,))
        self.observation_space =  gym.spaces.Dict(
            occupancy_grid = gym.spaces.Box(#todo change shape
                low=0, high=1, shape=(2, *self.agent.robot_map.global_map.shape)
            ),
            input_for_FC_net = gym.spaces.Box(low=-1, high=1, shape=(2,))
        )
        
        self.action_space = gym.spaces.Box(low=-1, high=1, shape=(2,))
        self.reward_range = (-1, 1)#todo change
        
    
    def reset(self, seed = None, options=None):
        
        super().reset(seed=seed) 
        
        self.episodes += 1
            
        self.simulation.reset()

        self.generate_start_and_goal_position(episodes=None)  
        
        for _ in range(3):
            self.add_human_to_simulation()        
 
        observation = self.get_observation()
        
        self.simulation.step()
        
        info = self.get_info([])
                    
        return observation, info
    
    def step(self, action):
        
        self.agent.forward_velocity = action[0] + 1
        self.agent.orientation_velocity = action[1]
        
        step_loop_scaling = 2
        for _ in range(step_loop_scaling):
            self.simulation.step()
            
        reward, *features = self.get_reward()
        
        for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
            distance_to_goal = mpi_sim.utils.measure_center_distance(human, human.navigation.goal_position)
            if distance_to_goal < self.config.human_goal_position_radius:
                self.generate_new_human_goal_position(human)
                        
        observation = self.get_observation()
                                      
        info = self.get_info(features)
                    
        return observation, reward, False, False, info
    
    def generate_start_and_goal_position(self, episodes = None):
        """
        This function generates a start and goal position for an agent, either randomly or based on
        provided information.
        """
        if self.config.random_start_position:
            # start_position = [np.random.uniform(-3.0, 3.0), np.random.uniform(-2.8, -2.5)]
            start_position = [np.random.uniform(-2.8, 2.8), np.random.uniform(-2.8, 2.8)]
            # start_position = [np.random.uniform(-.5, .5), np.random.uniform(-2.8, -2.5)]            
            self.agent.position = start_position
            self.agent.orientation = np.random.uniform(-np.pi, np.pi)
            # self.agent.orientation = np.random.uniform(-np.pi/2, np.pi/2)
            self.start_orientation = self.agent.orientation
        else:
            self.agent.position = self.config.start_goal_infos.start_position
            self.agent.orientation = self.config.start_goal_infos.start_orientation
            self.start_orientation = self.agent.orientation
            
    def add_human_to_simulation(self):
        """
        This function adds a human object to a simulation with a random starting position and
        orientation.
        """
        start_pos = [np.random.uniform(self.config.humans_start_region[0][0], 
                                        self.config.humans_start_region[0][1]), 
                        np.random.uniform(self.config.humans_start_region[1][0], 
                                        self.config.humans_start_region[1][1])]
        
        start_orientation = np.random.uniform(-np.pi, np.pi)
        
        human = mpi_sim.objects.Human(
            position = start_pos,
            orientation = start_orientation)
        
        self.generate_new_human_goal_position(human)      
        self.simulation.add_object(human)
    
    def generate_new_human_goal_position(self, human):
        """
        This function generates a new goal position and orientation for a given human agent within a
        specified region.
        
        :param human: The "human" parameter is an instance of a class representing a human agent in a
        simulation. It contains information about the human's current position, velocity, and
        other attributes relevant to the simulation. 
        """
        goal_pos = [np.random.uniform(self.config.humans_goal_region[0][0], 
                                self.config.humans_goal_region[0][1]), 
            np.random.uniform(self.config.humans_goal_region[1][0], 
                                self.config.humans_goal_region[1][1])]  
        goal_orientation = np.random.uniform(-np.pi, np.pi) 
        human.navigation.goal_position = goal_pos
        human.navigation.goal_orientation = goal_orientation                
            

    def compute_input_features(self):
        """
        This function computes and returns an array of two input features based on the distance and
        relative angle between an agent and a goal position.
        :return: The function `compute_input_features` is returning a numpy array containing two
        elements. The first element is the distance between the agent and the goal position, measured
        using the `measure_center_distance` function from the `mpi_sim.utils` module. The second element
        is the relative angle between the agent and the goal position, measured using the
        `measure_relative_angle` function from the same module.
        """
        return np.array([mpi_sim.utils.measure_center_distance(self.agent, [0,0]), 
                     mpi_sim.utils.measure_relative_angle(self.agent, [0,0])
                ])   
        
        
    def get_reward(self):

        velocity_reward = self.get_velocity_reward()
        reward = velocity_reward
            
        return reward, velocity_reward
    
    
    def get_velocity_reward(self):
        # reward = 0
        robot_velocity = np.sqrt((self.agent.position_velocity[0])**2 + (self.agent.position_velocity[1])**2)
        # for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
        #     new_reward = -math.exp((robot_velocity -.5)**2/1.5) * visible(self.agent, human)
        #     # print(coming(self.agent, human))
        #     if new_reward < reward:
        #         reward = new_reward
        new_reward = 1-math.exp((robot_velocity -.75)**2/1.5)
        return new_reward            
            
    
    def get_observation(self):
        """
        This function returns an observation object containing an occupancy grid and input features for
        a neural network.
        :return: an observation object which contains two attributes:
        1. occupancy_grid: a numpy array that stacks the global map of the robot and the global map of
        the humans in the scene.
        2. input_for_FC_net: a numpy array that contains the input features computed by the function
        compute_input_features() and reshaped to a 2-dimensional array.
        """
        observation = eu.AttrDict(
            occupancy_grid = np.stack((self.agent.robot_map.global_map, self.agent.human_map.global_map)),
            input_for_FC_net = self.compute_input_features().reshape(2,)
        )
        
        return observation
    
    def get_info(self, features):
        """
        This function returns a dictionary containing local and global maps for both human and robot
        agents.
        :return: The function `get_info` is returning a dictionary `info` containing the following keys
        and their corresponding values:
        - `'human_map_local'`: the local map of the human agent
        - `'human_map_global'`: the global map of the human agent
        - `'robot_map_global'`: the global map of the robot agent
        - `'robot_map_local'`: the local map of the robot
        """
        info = {'human_map_local': self.agent.human_map.local_map,
                'human_map_global': self.agent.human_map.global_map,
                'robot_map_global': self.agent.robot_map.global_map,
                'robot_map_local': self.agent.robot_map.local_map,
                'features': features,
                'start_orientation': self.start_orientation,
                'speed': np.sqrt((self.agent.position_velocity[0])**2 + (self.agent.position_velocity[1])**2)
                }
        
        return info

    def render(self):
        if self.render_mode is None:
            gym.logger.warn(
                "You are calling render method without specifying any render mode. "
                "You can specify the render_mode at initialization, "
                f'e.g. gym("{self.spec.id}", render_mode="rgb_array")'
            )
        else:
            return self._render(self.render_mode)
        
            
    def _render(self, render_mode):
          
        assert render_mode in self.metadata["render_modes"]
        
        if render_mode == 'dict':
            render_dict = {'robot': self.agent.position}
            for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
                render_dict[human.id] = human.position
            return render_dict
        elif render_mode == 'rgb_array':
            image = np.stack([np.rot90(self.agent.human_map.global_map), 
                      np.rot90(self.agent.robot_map.global_map),
                      np.rot90(np.zeros(shape = self.agent.robot_map.global_map.shape))],
                      axis = -1)
            image = -1  * image + 1
            return image
                            
    def close(self):
        super().close()  
            
