import numpy as np
import gymnasium as gym
import exputils as eu
import matplotlib.pyplot as plt

# TODO: the agent can walk through walls as these are smaller than the movement distance


def keep_value_in_circular_range(val, min_val, max_val):
    """
    Keeps a value in a certain range which represents a circle where both ends are connected, for example the degrees on a circle.

    Examples:
        keep_value_in_circular_range(val=10, min_val=-180, max_val=180) --> 10
        keep_value_in_circular_range(val=190, min_val=-180, max_val=180) --> -170
        keep_value_in_circular_range(val=-200, min_val=-180, max_val=180) --> 160

    :param val: Value
    :param min_val: Minimum value of the range.
    :param max_val: Maximum value of the range.
    """

    if min_val <= val and val <= max_val:
        return val
    else:

        range_length = max_val - min_val

        if val < min_val:
            d_min = min_val - val
            overspell = d_min % range_length
            return max_val - overspell

        else:
            # val > max_val
            d_max = val - max_val
            overspell = d_max % range_length
            return min_val + overspell


class TwoDInfiniteTrackRacerContinuousActionEnv(gym.Env):
    """
        features: Normalized distance [0, 1] to each marker.
    """
    
    metadata = {
        "render_modes": ["video"],
    }    

    @staticmethod
    def default_config():
        dc = eu.AttrDict()

        # [[min_x, max_x], [min_y, max_y]]
        dc.area = [[0., 1.],
                   [0., 1.]]

        # either a point [x, y, angle] or an area [[min_x, max_x], [min_y, max_y], [min+_angle, max_angle]] in which a point is uniformly sampled
        dc.start_state = np.array([
            [0.1, 0.9],  # x
            [0.1, 0.9],  # y
            [-np.pi, np.pi]  # angle)
        ])

        # location of each marker with [x, y] position
        dc.markers = np.array([
            [0.25, 0.75],
            [0.75, 0.25],
            [0.75, 0.6],
        ])

        # number of max timesteps before an episode ends
        dc.n_max_time_steps = np.inf

        # list of actions with (direction in angles from previous direction, distance)
        dc.actions = eu.AttrDict(
            max_angle=np.pi / 7,
            max_angle_distance=0.06,
            zero_angle_distance=0.075,
        )

        # defines if the env is deterministic (stochasticity=None) or stochastic (position + standard normal error with stochasticity=[sigma_x, sigma_y, sigma_angle])
        dc.stochasticity = (0.005, 0.005, 0.005)

        # active reward function
        dc.reward_function = lambda feature_array: np.sum(feature_array)

        return dc

    def __init__(self, config=None, render_mode=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        area_x_min = self.config.area[0][0]
        area_x_max = self.config.area[0][1]
        area_y_min = self.config.area[1][0]
        area_y_max = self.config.area[1][1]

        max_possible_marker_distance = np.sqrt(
            (area_x_max - area_x_min)**2 + (area_y_max - area_y_min)**2) / 2.0
        self.marker_distance_normalization_coeff = 1.0 / max_possible_marker_distance

        self.observation_space = gym.spaces.Dict(
            observation=gym.spaces.Box(
                low=np.array([area_x_min, area_x_max, -np.pi],
                             dtype=np.float32),
                high=np.array([area_y_min, area_y_max, np.pi],
                              dtype=np.float32)
            ),
            feature=gym.spaces.Box(
                low=np.array(
                    [0.0] * (self.config.markers.shape[0]), dtype=np.float32),
                high=np.array(
                    [1.0] * (self.config.markers.shape[0]), dtype=np.float32)
            ),
        )
        self.action_space = gym.spaces.Box(
            low=-1.0,
            high=1.0,
            shape=(1, )
        )

        self.reward_function = self.config.reward_function

        self.state_pos = None
        self.state_angle = None
        self.n_steps = None
        
        assert render_mode is None or render_mode in self.metadata["render_modes"]
        self.render_mode = render_mode         

    def get_obs(self, agent_state=None):

        if agent_state is None:
            agent_pos = self.state_pos
            agent_angle = self.state_angle
        else:
            agent_pos = agent_state[:2]
            agent_angle = agent_state[2]

        return dict(
            observation=np.array([agent_pos[0], agent_pos[1], agent_angle]),
            feature=self.calc_distance_to_markers(agent_pos)
        )

    def calc_distance_to_markers(self, position):
        marker_distances = self.calc_min_distance(
            position, self.config.markers)
        return marker_distances * self.marker_distance_normalization_coeff

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)

        # all object exists
        if np.ndim(self.config.start_state) == 1:
            self.state_pos = self.config.start_state[:2]
            self.state_angle = self.config.start_state[2]
        else:
            # [[min_x, max_x, min_angle], [min_y, max_y, max_angle]] in which a point is uniformly sampled
            def sample_in_range(min_val, max_val):
                return min_val + (max_val - min_val) * np.random.rand()

            # sample x
            min_x = self.config.start_state[0][0]
            max_x = self.config.start_state[0][1]
            x = sample_in_range(min_x, max_x)

            min_y = self.config.start_state[1][0]
            max_y = self.config.start_state[1][1]
            y = sample_in_range(min_y, max_y)

            min_direc = self.config.start_state[2][0]
            max_direc = self.config.start_state[2][1]
            direc = sample_in_range(min_direc, max_direc)

            self.state_pos = np.array([x, y], dtype=np.float32)
            self.state_angle = direc

        self.n_steps = 0
        obs = self.get_obs()
        
        if self.render_mode == 'video':
            self.render()
        return obs['observation'], {}

    def step(self, action):

        self.n_steps += 1
        if self.n_steps >= self.config.n_max_time_steps:
            done = True
        else:
            done = False

        normalized_action = action[0]

        if normalized_action < -1.0:
            normalized_action = -1.0
        elif normalized_action > 1.0:
            normalized_action = 1.0

        action_angle = normalized_action * self.config.actions.max_angle
        action_distance = self.config.actions.zero_angle_distance + abs(normalized_action) * (
                self.config.actions.max_angle_distance - self.config.actions.zero_angle_distance)

        delta_x = 0.0
        delta_y = 0.0
        delta_dir = 0.0

        if self.config.stochasticity is not None:
            random_numbers = np.random.randn(len(self.config.stochasticity))

            delta_x = random_numbers[0] * self.config.stochasticity[0]
            delta_y = random_numbers[1] * self.config.stochasticity[1]

            if len(random_numbers) > 2:
                delta_dir = random_numbers[2] * self.config.stochasticity[2]

        # allow angle only to be between -pi and pi
        new_angle = keep_value_in_circular_range(
            self.state_angle + action_angle + delta_dir, -np.pi, np.pi)

        # get new position
        new_x = self.state_pos[0] + \
            np.cos(new_angle) * action_distance + delta_x
        new_y = self.state_pos[1] + \
            np.sin(new_angle) * action_distance + delta_y

        # check if the new position is inside the area
        # otherwise let agent reappear on the other side
        x_min, x_max = self.config.area[0]
        if new_x < x_min:
            new_x = x_max - (x_min - new_x)
        elif new_x > x_max:
            new_x = x_min + (new_x - x_max)

        y_min, y_max = self.config.area[1]
        if new_y < y_min:
            new_y = y_max - (y_min - new_y)
        elif new_y > y_max:
            new_y = y_min + (new_y - y_max)

        self.state_pos[:] = [new_x, new_y]
        self.state_angle = new_angle

        obs = self.get_obs()

        reward = self.reward_function(obs['feature'])
        info = eu.AttrDict(
            position=[new_x, new_y],
            direction=new_angle,
            features=obs['feature']
        )
        if self.render_mode == "video":
            self.render()
        
        return obs['observation'], reward, done, False, info

    def render(self):

        marker_colors = ['green', 'blue', 'red', 'yellow']

        is_calc_reward_map = False
        if not hasattr(self, 'renderer_figure'):
            self.renderer_figure, self.renderer_axis = plt.subplots()
            self.renderer_colorbar = None
            is_calc_reward_map = True
        else:
            self.renderer_axis.clear()

            if self.render_rendered_reward_function is not self.reward_function:
                is_calc_reward_map = True

        if is_calc_reward_map:
            # calculate the map to render the reward function
            self.render_rendered_reward_function = self.reward_function
            self.render_reward_map, self.render_x_map, self.render_y_map = self.calc_reward_map()

            if self.renderer_colorbar:
                self.renderer_colorbar.remove()
                self.renderer_colorbar = None

        # plot the reward map
        reward_plot = self.renderer_axis.pcolor(
            self.render_x_map, self.render_y_map, self.render_reward_map, shading='auto', cmap="Greys")
        if not self.renderer_colorbar:
            self.renderer_colorbar = plt.colorbar(reward_plot)

        # plot markers
        for marker_idx, marker in enumerate(self.config.markers):
            marker_plot_obj = plt.Circle(
                marker, radius=0.01, color=marker_colors[marker_idx])
            self.renderer_axis.add_artist(marker_plot_obj)

        # plot the agent position
        agent_circle = plt.Circle(
            self.state_pos, radius=0.02, color='orange', alpha=0.7)
        self.renderer_axis.add_artist(agent_circle)

        # display plot
        plt.draw()
        plt.pause(0.01)

    def calc_reward_map(self, n_points_x=100, n_points_y=100):

        x_points = np.linspace(
            self.config.area[0][0], self.config.area[0][1], n_points_x)
        y_points = np.linspace(
            self.config.area[1][0], self.config.area[1][1], n_points_y)

        x_map, y_map = np.meshgrid(x_points, y_points)

        reward_map = np.empty_like(x_map)
        for i in range(x_map.shape[0]):
            for j in range(y_map.shape[0]):
                obs = self.get_obs(agent_state=[x_map[i, j], y_map[i, j], 0.0])
                #marker_distances = self.calc_distance_to_markers(np.array([x_map[i, j], y_map[i, j]]))
                reward_map[i, j] = self.reward_function(obs['feature'])

        return reward_map, x_map, y_map

    def calc_min_distance(self, point1, point2):

        diff_x, diff_y = self.calc_min_difference(point1, point2)

        dist = (diff_x ** 2 + diff_y ** 2) ** (1. / 2)

        return dist

    def calc_min_difference(self, point1, point2):

        point1 = np.array(point1)
        point2 = np.array(point2)

        x_min, x_max = self.config.area[0]
        y_min, y_max = self.config.area[1]

        max_x_diff = (x_max - x_min) / 2.
        max_y_diff = (y_max - y_min) / 2.

        if point1.ndim == 1:
            point1 = np.array([point1])
            single_point = True
        else:
            single_point = False

        if point2.ndim == 1:
            point2 = np.array([point2])
        else:
            single_point = False

        diff_x = np.abs(point1[:, 0] - point2[:, 0])
        diff_y = np.abs(point1[:, 1] - point2[:, 1])

        diff_x_case = diff_x > max_x_diff
        diff_y_case = diff_y > max_y_diff

        diff_x[diff_x_case] = 2 * max_x_diff - diff_x[diff_x_case]
        diff_y[diff_y_case] = 2 * max_y_diff - diff_y[diff_y_case]

        if single_point:
            diff_x = diff_x[0]
            diff_y = diff_y[0]

        return diff_x, diff_y

    def plot_env(self, axis=None, plot_colorbar=True, cmap=plt.cm.coolwarm, vmin=None, vmax=None, n_points_per_dim=100):

        if axis is None:
            is_axis_given = False
            figure, axis = plt.subplots()
        else:
            is_axis_given = True

        marker_colors = ['green', 'blue', 'red', 'yellow']

        # calculate the map to render the reward function
        reward_map, x_map, y_map = self.calc_reward_map(
            n_points_x=n_points_per_dim, n_points_y=n_points_per_dim)

        # plot the reward map
        reward_plot = axis.pcolor(
            x_map, y_map, reward_map, cmap=cmap, shading='auto', vmin=vmin, vmax=vmax)
        if plot_colorbar:
            plt.colorbar(reward_plot, ax=axis)

        # plot markers
        for marker_idx, marker in enumerate(self.config.markers):
            marker_plot_obj = plt.Circle(
                marker, radius=0.01, color=marker_colors[marker_idx])
            axis.add_artist(marker_plot_obj)

        if not is_axis_given:
            return figure, axis
