import os
import time
import uuid
import gymnasium as gym
import exputils as eu
import math
import numpy as np
import mpi_sim
import mpi_sim.core.module_library as modlib
import cv2
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap
import matplotlib.animation as animation
import random as random
import exputils.data.logging as log
from mpi_sim.processes.gui import GUI

def visible(agent, human, visibility_threshold):
    is_agent_visible = np.abs(mpi_sim.utils.measure_relative_angle(human, agent))
    if is_agent_visible < visibility_threshold:
        return 1 - is_agent_visible/(visibility_threshold)
    return -1

def coming(agent, human, visibility_threshold):
    is_agent_visible = np.abs(mpi_sim.utils.measure_relative_angle(human, agent))
    is_agent_coming = np.abs(mpi_sim.utils.measure_relative_angle(agent, human))
    if is_agent_visible < visibility_threshold:
        return 1 - is_agent_coming/(math.pi/2)
    return 1 

class MpiEnv(gym.Env):
    
    metadata = {
    "render_modes": ["dict", "human", "rgb_array"],
}
    @staticmethod
    def default_config():
        return eu.AttrDict(
            # Common parameters
            goal_position_radius = 0.3,
            
            # Humans parameters
            # humans_start_region = [[-0.5, 0.5], [-0.5, 0.5]], # [[x_min, x_max], [y_min, y_max]]
            humans_start_region = [[-2.8, 2.8], [-2.8, 2.8]], # [[x_min, x_max], [y_min, y_max]]
            humans_goal_region = [[-2.8, 2.8], [-2.8, 2.8]], # [[x_min, x_max], [y_min, y_max]]
            human_goal_position_radius = 0.05,
            
            # Parameters about humans
            n_max_humans = 3, # used when random or not
            random_nb_humans = False,
            
           # Parameters about ARI
            random_start_position = True,
            random_goal_position = True,
            start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                    start_position = [-2.5, -2.5],
                    start_orientation = 0,
                    goal_position = [2.5, 2.5],
                    goal_orientation = 0.,
                ),
            termination_reward = 'final',
            curriculum_learning = None,
            reward_config = eu.AttrDict(
                social_space_threshold = 1,
                visibility_threshold = np.pi/3,
                disturbance_thresold = 3,
                target_speed = 0.75
            ),
            scaling_factor = eu.AttrDict(
                goal = 1,
                collision = 4,
                social = 4,
                approach = 1/2,
                velocity = 1
            ),
            test = False
        )
        
    def __init__(self, config=None, render_mode = None, **kwargs):
        
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.episodes = 0
        print(self.config.termination_reward)
        print(self.config.n_max_humans)
        
        assert render_mode is None or render_mode in self.metadata["render_modes"]
        print(render_mode)

        self.render_mode = render_mode

         # Area to show in the global map 
        self.area = ((-3.2, 3.2), (-3.2, 3.2))
        
        world_config = eu.AttrDict(visible_area = self.area,
                                   objects = [
                    {'type': 'Wall', 'position': [-3.2, 0], 'orientation': np.pi, 'height': 6.4},
                    {'type': 'Wall', 'position': [3.2, 0], 'orientation': 0., 'height': 6.4},
                    {'type': 'Wall', 'position': [0., 3.2], 'orientation': np.pi / 2, 'height': 6.4},
                    {'type': 'Wall', 'position': [0., -3.2], 'orientation': -np.pi / 2, 'height': 6.4}
                ],
        )
        
        if render_mode == "human":
            world_config['processes'] = [{'type': 'GUI'}]
            
        # create simulation
        self.simulation = mpi_sim.Simulation(world_config = world_config)
                  
        self.agent = mpi_sim.objects.ARIRobot(
            components=[
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='human_map',
                    object_filter = dict(types=mpi_sim.objects.Human),
                    depth = 3.0,
                    resolution = 0.2,
                    visibility_threshold = self.config.reward_config.visibility_threshold
                    ),
                mpi_sim.AttrDict(
                    type=mpi_sim.components.OccupancyGridMapping,
                    name='robot_map',
                    object_filter = dict(types=mpi_sim.objects.ARIRobot),
                    depth = 3.0,
                    resolution = 0.2,
                    ),
            ],
                position = self.config.start_goal_infos.start_position,
                orientation = self.config.start_goal_infos.start_orientation
        )
        
        self.simulation.add_object(self.agent)
        self.simulation.set_reset_point()

        self.observation_space =  gym.spaces.Dict(
            occupancy_grid = gym.spaces.Box(#todo change shape
                low=0, high=1, shape=(3, *self.agent.robot_map.global_map.shape)
            ),
            input_for_FC_net = gym.spaces.Box(low=-1, high=1, shape=(2,))
        )
        
        self.action_space = gym.spaces.Box(low=-1, high=1, shape=(2,))
        self.reward_range = (-1, 1)#todo change
        
        self.goal_position = self.config.start_goal_infos.goal_position
        self.former_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
        
        self.metrics = {}
        if self.config.test:
            self.reset_metrics()
    
    def reset(self, seed = None, options=None):
        """
        The function `reset` is used to reset the simulation environment, generate new start and goal
        positions, add humans to the simulation, get the observation, step the simulation, calculate the
        distance to the goal, log metrics (if in test mode), and return the observation and info.
        
        :param seed: The `seed` parameter is used to set the random seed for the simulation. By setting
        a specific seed, you can ensure that the simulation behaves in a deterministic manner, meaning
        that it will produce the same results every time it is run with the same seed. If no seed is
        provided, the simulation
        :param options: The `options` parameter is an optional argument that allows you to pass
        additional options or settings to the `reset` method. 
        :return: the variables `observation` and `info`.
        """
        
        super().reset(seed=seed) 
        
        self.episodes += 1
            
        self.simulation.reset()

        if self.config.curriculum_learning == 'distance_to_goal':
            self.generate_start_and_goal_position(episodes=self.episodes)  
            
        else:
            self.generate_start_and_goal_position(episodes=None)
        
        if self.config.curriculum_learning == 'numbers_of_humans':
            n_humans = min(self.config.n_max_humans, max(1, self.episodes//1000))
            
        else:
            n_humans = self.config.n_max_humans
        
        # Number of humans in simulation
        if self.config.random_nb_humans:
            n_humans = np.random.randint(1, n_humans + 1)
                   
        for _ in range(n_humans):
            self.add_human_to_simulation()
 
        observation = self.get_observation()
        
        self.simulation.step()
        
        info = self.get_info([])
        
        self.former_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
        
        if self.config.test:
            self.log_metrics()
            self.reset_metrics()
            
        return observation, info
    
    def step(self, action):
        """
        The `step` function updates the agent's velocity and orientation based on the given action,
        performs simulation steps, checks if the agent has reached the goal position, calculates the
        reward and features, updates the human's goal position if necessary, and returns the
        observation, reward, termination status, and info.
        
        :param action: The `action` parameter is a list containing two values. The first value
        represents the forward velocity of the agent, and the second value represents the orientation
        velocity of the agent
        :return: the following values:
        """
        
        self.agent.forward_velocity = action[0] + 1
        # print('ok')
        self.agent.orientation_velocity = action[1]
        
        step_loop_scaling = 2
        for _ in range(step_loop_scaling):
            self.simulation.step()
            robot_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            if robot_distance_to_goal < self.config.goal_position_radius:
                terminated = True
                break
            
        else:
            robot_distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            terminated = False
            
        reward, *features = self.get_reward()  
        
        if self.config.termination_reward == 'final':
            if terminated:
                reward += 4
        
        elif self.config.termination_reward == 'improvement':
            reward += self.former_distance_to_goal**2 - robot_distance_to_goal**2   
            
        for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
            distance_to_goal = mpi_sim.utils.measure_center_distance(human, human.navigation.goal_position)
            if distance_to_goal < self.config.goal_position_radius:
                self.generate_new_human_goal_position(human)
                        
        observation = self.get_observation()
                                      
        info = self.get_info(features)
        
        self.former_distance_to_goal = robot_distance_to_goal
        
        if self.config.test:
            self.update_metrics(features, terminated)
            
        return observation, reward, terminated, False, info
    
    def generate_start_and_goal_position(self, episodes = None):
        """
        This function generates a start and goal position for an agent, either randomly or based on
        provided information.
        """
        if self.config.random_start_position:
            # start_position = [np.random.uniform(-3.0, 3.0), np.random.uniform(-2.8, -2.5)]
            start_position = [np.random.uniform(-2.8, 2.8), np.random.uniform(-2.8, 2.8)]
            # start_position = [np.random.uniform(-.5, .5), np.random.uniform(-2.8, -2.5)]            
            self.agent.position = start_position
            self.agent.orientation = np.random.uniform(-np.pi, np.pi)
            # self.agent.orientation = np.random.uniform(-np.pi/2, np.pi/2)
            self.start_orientation = self.agent.orientation
        else:
            self.agent.position = self.config.start_goal_infos.start_position
            self.agent.orientation = self.config.start_goal_infos.start_orientation
            self.start_orientation = self.agent.orientation
        if self.config.random_goal_position:
            distance_start_goal = 0
            if episodes:
                while distance_start_goal > (episodes)//500 +1.5 or distance_start_goal < 1: 
                    self.goal_position = [np.random.uniform(-2.5, 2.5), np.random.uniform(-2.5, 2.5)]
                    distance_start_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
            else:              
                while distance_start_goal < 2:
                    self.goal_position = [np.random.uniform(-2.5, 2.5), np.random.uniform(-2.5, 2.5)]
                    # self.goal_position = [np.random.uniform(-3.0, 3.0), np.random.uniform(2.5, 2.8)]
                    # self.goal_position = [np.random.uniform(-.5, .5), np.random.uniform(2.5, 2.8)]
                    distance_start_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
        else:
            self.goal_position = self.config.start_goal_infos.goal_position
            
    def add_human_to_simulation(self):
        """
        This function adds a human object to a simulation with a random starting position and
        orientation.
        """
        start_pos = [np.random.uniform(self.config.humans_start_region[0][0], 
                                        self.config.humans_start_region[0][1]), 
                        np.random.uniform(self.config.humans_start_region[1][0], 
                                        self.config.humans_start_region[1][1])]
        
        start_orientation = np.random.uniform(-np.pi, np.pi)
        
        human = mpi_sim.objects.Human(
            position = start_pos,
            orientation = start_orientation)
        
        self.generate_new_human_goal_position(human)      
        self.simulation.add_object(human)
    
    def generate_new_human_goal_position(self, human):
        """
        This function generates a new goal position and orientation for a given human agent within a
        specified region.
        
        :param human: The "human" parameter is an instance of a class representing a human agent in a
        simulation. It contains information about the human's current position, velocity, and
        other attributes relevant to the simulation. 
        """
        goal_pos = [np.random.uniform(self.config.humans_goal_region[0][0], 
                                self.config.humans_goal_region[0][1]), 
            np.random.uniform(self.config.humans_goal_region[1][0], 
                                self.config.humans_goal_region[1][1])]  
        goal_orientation = np.random.uniform(-np.pi, np.pi) 
        human.navigation.goal_position = goal_pos
        human.navigation.goal_orientation = goal_orientation    

    def compute_input_features(self):
        """
        This function computes and returns an array of two input features based on the distance and
        relative angle between an agent and a goal position.
        :return: The function `compute_input_features` is returning a numpy array containing two
        elements. The first element is the distance between the agent and the goal position, measured
        using the `measure_center_distance` function from the `mpi_sim.utils` module. The second element
        is the relative angle between the agent and the goal position, measured using the
        `measure_relative_angle` function from the same module.
        """
        return np.array([mpi_sim.utils.measure_center_distance(self.agent, self.goal_position), 
                     mpi_sim.utils.measure_relative_angle(self.agent, self.goal_position)
                ])   
        
        
    def get_reward(self):
        goal_reward = self.get_goal_reward()
        collision_reward = self.get_collision_reward()
        social_reward = self.get_social_reward()
        approaching_reward = self.get_approaching_reward()
        velocity_reward = self.get_velocity_reward()
        reward = 0.2*goal_reward + 0.2*collision_reward + 0.2*social_reward + 0.2*approaching_reward + 0.2*velocity_reward
        
        return reward, goal_reward, collision_reward, social_reward, approaching_reward, velocity_reward
    
    def get_goal_reward(self):       
        """
        This function computes a reward based on the distance to the goal and updates the reward based
        on whether the distance has improved or not.
        
        :param distance_to_goal: The distance between the current position of the agent and the goal
        position
        :return: a reward value based on the distance to the goal. 
        The reward is the difference between the former distance and the current distance. 
        If the distance to the goal is less than the goal position radius, the reward is increased by 500.
        """
        
        distance_to_goal = mpi_sim.utils.measure_center_distance(self.agent, self.goal_position)
        
        reward = self.former_distance_to_goal - distance_to_goal
            
        return min(reward, 0)/self.config.scaling_factor.goal
    
    def get_collision_reward(self):
        for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
            if mpi_sim.utils.measure_center_distance(self.agent, human) < .6:
                # print(mpi_sim.utils.measure_center_distance(self.agent, human))
                reward = -1
                break
        else:
            reward = 0
        return reward/self.config.scaling_factor.collision
    
    def get_social_reward(self):
        reward = 0
        for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
            new_reward = 0
            if mpi_sim.utils.measure_center_distance(self.agent, human) < .6:
                new_reward = -1
            elif mpi_sim.utils.measure_center_distance(self.agent, human) < self.config.reward_config.social_space_threshold:
                coef = 1/(self.config.reward_config.social_space_threshold-.6)
                ordo = coef*self.config.reward_config.social_space_threshold
                new_reward = mpi_sim.utils.measure_center_distance(self.agent, human)*coef-ordo
            if new_reward < reward:
                reward = new_reward
        return reward/self.config.scaling_factor.social
    
    def get_approaching_reward(self):
        reward = 0
        for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
            if mpi_sim.utils.measure_center_distance(self.agent, human) < self.config.reward_config.disturbance_thresold:
                visibility = visible(self.agent, human, self.config.reward_config.visibility_threshold)
                is_coming = coming(self.agent, human, self.config.reward_config.visibility_threshold)
                # print(visibility, is_coming)
                new_reward = -visibility*is_coming
                new_reward = (new_reward-1)/2
                if new_reward < reward:
                    reward = new_reward
        return reward/self.config.scaling_factor.approach    
    
    
    def get_velocity_reward(self):
        # reward = 0
        robot_velocity = np.sqrt((self.agent.position_velocity[0])**2 + (self.agent.position_velocity[1])**2)
        # for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
        #     new_reward = -math.exp((robot_velocity -.5)**2/1.5) * visible(self.agent, human)
        #     # print(coming(self.agent, human))
        #     if new_reward < reward:
        #         reward = new_reward
        # new_reward = 1-math.exp((robot_velocity -.75)**2/1.5)
        new_reward = -3*abs(robot_velocity -.75)
        return new_reward/self.config.scaling_factor.velocity            
            
    
    def get_observation(self):
        """
        This function returns an observation object containing an occupancy grid and input features for
        a neural network.
        :return: an observation object which contains two attributes:
        1. occupancy_grid: a numpy array that stacks the global map of the robot and the global map of
        the humans in the scene.
        2. input_for_FC_net: a numpy array that contains the input features computed by the function
        compute_input_features() and reshaped to a 2-dimensional array.
        """
        observation = eu.AttrDict(
            occupancy_grid = np.stack((self.agent.robot_map.global_map, self.agent.human_map.global_map, self.agent.human_map.visibility_map)),
            # occupancy_grid = np.stack((self.agent.robot_map.global_map, self.agent.human_map.global_map)),
            input_for_FC_net = self.compute_input_features().reshape(2,)
        )
        
        return observation
    
    def get_info(self, features):
        """
        This function returns a dictionary containing local and global maps for both human and robot
        agents.
        :return: The function `get_info` is returning a dictionary `info` containing the following keys
        and their corresponding values:
        - `'human_map_local'`: the local map of the human agent
        - `'human_map_global'`: the global map of the human agent
        - `'robot_map_global'`: the global map of the robot agent
        - `'robot_map_local'`: the local map of the robot
        """
        info = {'human_map_local': self.agent.human_map.local_map,
                'human_map_global': self.agent.human_map.global_map,
                'robot_map_global': self.agent.robot_map.global_map,
                'robot_map_local': self.agent.robot_map.local_map,
                'features': features,
                'goal_position': self.goal_position,
                'start_orientation': self.start_orientation,
                'speed': np.sqrt((self.agent.position_velocity[0])**2 + (self.agent.position_velocity[1])**2),
                'visibility': self.agent.human_map.visibility_map
                }
        
        return info
    
    def update_metrics(self, features, terminated):
        """
        The function updates various metrics based on the given features and termination status.
        
        :param features: The `features` parameter is a list that contains the following values:
        :param terminated: The "terminated" parameter is a boolean value that indicates whether the
        agent's episode has terminated or not. It is used to determine if the agent has successfully
        completed its task or not
        """
        self.metrics['collision'].append(-features[1])
        self.metrics['social'].append(-features[2])
        self.metrics['disturbances'].append(-features[3])
        speed = np.sqrt((self.agent.position_velocity[0])**2 + (self.agent.position_velocity[1])**2)
        self.metrics['speed'].append(speed)
        self.metrics['steps'] += 1
        self.metrics['success'] = terminated
        
    def log_metrics(self):
        """
        The function `log_metrics` calculates various metrics such as collision, social behavior,
        disturbances, speed, steps, and success, and logs them without displaying them in TensorBoard.
        """
        if self.metrics['steps'] != 0:
            collision = 0
            previous_state = self.metrics['collision'][0]
            for state in self.metrics['collision']:
                if state == 1 and previous_state == 0:
                    collision += 1
                previous_state = state
            log.add_value('collision', collision, log_to_tb = False)
            social = np.mean(self.metrics['social'])
            log.add_value('social', social, log_to_tb = False)
            disturbances = np.mean(self.metrics['disturbances'])
            log.add_value('disturbances', disturbances, log_to_tb = False)
            speed = np.mean(self.metrics['speed'])
            log.add_value('speed', speed, log_to_tb = False)
            log.add_value('step', self.metrics['steps'], log_to_tb = False)
            log.add_value('success', 1 if self.metrics['success'] else 0, log_to_tb = False)
        
    def reset_metrics(self):
        """
        The function `reset_metrics` resets the values of various metrics in a dictionary.
        """
        self.metrics['collision'] = []
        self.metrics['social'] = []
        self.metrics['disturbances'] = []
        self.metrics['speed'] = []
        self.metrics['steps'] = 0
        self.metrics['success'] = False

    def render(self):
        """
        The function checks if a render mode is specified and returns the result of the _render method
        with the specified render mode.
        :return: The output of the `_render` method is being returned.
        """
        if self.render_mode is None:
            gym.logger.warn(
                "You are calling render method without specifying any render mode. "
                "You can specify the render_mode at initialization, "
                f'e.g. gym("{self.spec.id}", render_mode="rgb_array")'
            )
        else:
            return self._render(self.render_mode)
        
            
    def _render(self, render_mode):
        """
        The function `_render` returns either a dictionary or an image based on the specified render
        mode.
        
        :param render_mode: The `render_mode` parameter is a string that specifies the desired mode for
        rendering the environment. The available render modes are specified in the `metadata` attribute
        of the object that this method belongs to
        :return: either a dictionary or an image depending on the value of the `render_mode` parameter.
        If `render_mode` is 'dict', a dictionary containing the positions of the robot and any humans in
        the simulation is returned. If `render_mode` is 'rgb_array', an image is returned.
        """
          
        assert render_mode in self.metadata["render_modes"]
        
        if render_mode == 'dict':
            render_dict = {'robot': self.agent.position}
            for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
                render_dict[human.id] = human.position
            return render_dict
        elif render_mode == 'rgb_array':
            image = np.stack([np.rot90(self.agent.human_map.global_map), 
                      np.rot90(self.agent.robot_map.global_map),
                      np.rot90(np.zeros(shape = self.agent.robot_map.global_map.shape))],
                      axis = -1)
            image = -1  * image + 1
            return image
                            
    def close(self):
        super().close()  
            
    # def _plot(self, save_plots='', trajectory_grid_show=False):
        
    #     fig, ax = plt.subplots()
        
    #     for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
    #         self.trajectories[human.id] = self.trajectories[human.id].reshape(-1, 2)
    #         x = self.trajectories[human.id][:, 0]
    #         y = self.trajectories[human.id][:, 1]
    #         points = np.array([x, y]).T.reshape(-1, 1, 2)
    #         segments = np.concatenate([points[:-1], points[1:]], axis=1)
    #         norm = plt.Normalize(0, len(x))
    #         lc = LineCollection(segments, cmap='Greys', norm=norm, label = 'human_'+str(human.id))
    #         # Set the values used for colormapping
    #         lc.set_array(list(range(len(x))))
    #         lc.set_linewidth(3)
    #         line = ax.add_collection(lc)
            
    #     self.trajectories['robot'] = self.trajectories['robot'].reshape(-1, 2)
    #     x = self.trajectories['robot'][:, 0]
    #     y = self.trajectories['robot'][:, 1]
    #     points = np.array([x, y]).T.reshape(-1, 1, 2)
    #     segments = np.concatenate([points[:-1], points[1:]], axis=1)
        
    #     norm = plt.Normalize(0, len(x))
    #     lc = LineCollection(segments, cmap='viridis', norm=norm, label = 'robot')
    #     # Set the values used for colormapping
    #     lc.set_array(list(range(len(x))))
    #     lc.set_linewidth(4)
    #     line = ax.add_collection(lc)
                
    #     ax.set_yticks([])
    #     ax.set_xticks([])
        
    #     start = self.trajectories['robot'][0,:]
    #     ax.scatter(start[0], start[1], marker="^",color="black", label="start positions")
    #     for human in self.simulation.get_objects_by_type(modlib.get_object('Human')):
    #         start = self.trajectories[human.id][0,:]
    #         ax.scatter(start[0], start[1], marker="^",color="black")

    #     # Plot the goal point
    #     goal = self.goal_position
    #     ax.scatter(goal[0], goal[1], color="red", label="robot's goal position") 
    #     ax.set_xlim(-3, 3)
    #     ax.set_ylim(-3, 3)

    #     ax.legend(loc='upper right',bbox_to_anchor=(1, 0),fancybox=True, shadow=True, ncol=3)
    #     if save_plots :
    #         fig.savefig(save_plots)
    #     if trajectory_grid_show:
    #         plt.show()
                        
            
np.random.seed(0)
random.seed(0)        
if __name__ == "__main__":
    config = eu.AttrDict(
            random_start_position = False,
            random_goal_position = True,
            start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                    start_orientation = 0,
                    start_position = [0, -1.5],
                    goal_position = [1.5, 1.5],
                    goal_orientation = 0.,
                ))
    env = MpiEnv(config = config, render_mode='dict')
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, layout='constrained', sharey=True)
    _, info = env.reset()
    time.sleep(1)
    im1 = ax1.imshow(np.rot90(info['robot_map_global']), animated=True) 
    im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
    ax3.imshow(np.rot90(info['human_map_global']), animated=True)
    ax3.set_xticks(np.arange(-0.5, 31., 1), labels=[])
    ax3.set_yticks(np.arange(-0.5, 31., 1), labels=[])
    ax3.grid()
    ax1.xaxis.set_ticklabels([])
    ax2.xaxis.set_ticklabels([])
    step = 0
    ims1 = [[im1]]
    ims2 = [[im2]]
    col =0
    for _ in range(100):
        #env.render()
        obs, _, terminated, truncated, info = env.step(env.action_space.sample())
        step += 1
        time.sleep(.1)
        if info['features'][1] == -1:
            
            col += 1
        # print(env.agent.position, env.goal_position)
        # print(obs)
        #print(info['agent_map'].shape)
        im1 = ax1.imshow(np.rot90(info['robot_map_global']), animated=True)
        ims1.append([im1])
        im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
        ims2.append([im2])
        # if terminated or truncated:
        #     break
    # env.render()
    print(col)
    env.close()
    ani = animation.ArtistAnimation(fig, ims1, interval=250, blit=True,
                                repeat_delay=1000, repeat = True)
    ani2 = animation.ArtistAnimation(fig, ims2, interval=250, blit=True,
                                repeat_delay=1000, repeat = True)    
    plt.show()
    print(info['robot_map_global'].shape)
    print(info['human_map_global'].shape)
    print(np.stack((info['robot_map_global'], info['human_map_global'])).shape)
    env.reset()
    step = 0
    for _ in range(40):
        #env.render()
        obs, _, terminated, truncated, *_ = env.step([1, .5])
        step += 1
        time.sleep(.1)
        # print(env.agent.position, env.goal_position)
        # print(obs)
        if terminated or truncated:
            break
    print(step)
    # print(env.agent.mapping.local_map)
                