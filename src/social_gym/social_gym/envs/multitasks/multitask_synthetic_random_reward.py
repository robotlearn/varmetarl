import numpy as np
from social_gym.envs.robotics.syntheticFB import SyntFBEnv
from social_gym.lib.random_reward_generator import RandomRewardFunctionGenerator
# Copy task structure from https://github.com/jonasrothfuss/ProMP/blob/master/meta_policy_search/envs/mujoco_envs/ant_rand_goal.py

class SyntheticEnv(SyntFBEnv):
    def __init__(self, config=None, n_tasks=2, tasks = {}, **kwargs):
        super().__init__(config=config, **kwargs)
        if tasks != {}:
            self.tasks = tasks
        else:
            self.tasks = self.sample_tasks(n_tasks)
        self.reset_task(0)

    def getReward(self):
        reward_vis = self.visual_reward()
        reward_aud = self.audio_reward()
        reward_mov, dir_reward = self.movement_reward()
        reward = self._reward(
            [reward_vis, reward_aud, -reward_mov - dir_reward])
        return reward, reward_vis, reward_aud, reward_mov, dir_reward

    def sample_tasks(self, num_tasks):
        generator = RandomRewardFunctionGenerator()
        r_funcs = generator.sample(num_tasks)
        return r_funcs

    def get_all_task_idx(self):
        return range(len(self.tasks))

    def reset_task(self, idx):
        """
        Take an id as parameter
        intialize a new task and set up a new reward function
        """
        self._task = self.tasks[idx]
        self._reward = self.tasks[idx]
        self.goal = self._reward.code_str
        self.reset()