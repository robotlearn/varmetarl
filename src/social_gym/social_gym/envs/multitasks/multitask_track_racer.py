from social_gym.envs.robotics.rbf_twod_infinite_track_racer import sample_component_independent_rfunc, RBFStateTwoDInfiniteTrackRacerContinuousActionEnv

class RacerEnv(RBFStateTwoDInfiniteTrackRacerContinuousActionEnv):
    def __init__(self, config=None, n_tasks=2,  tasks = {}, **kwargs):
        super().__init__(config=config, **kwargs)
        if tasks != {}:
            self.tasks = tasks
        else:
            self.tasks = self.sample_tasks(n_tasks)
        self.reset_task(0)
        self.num_weights = 6

    def sample_tasks(self, num_tasks):
        tasks = []
        for _ in range(num_tasks):
            r_func, str_r_func, _, goal = sample_component_independent_rfunc(
                self.config.markers.shape[0])
            tasks.append({'r_func': r_func, 'code_str': str_r_func, 'goal': goal})
        return tasks

    def get_all_task_idx(self):
        return range(len(self.tasks))

    def reset_task(self, idx):
        """
        Take an id as parameter
        intialize a new task and set up a new reward function
        """
        self._task = self.tasks[idx]
        self.reward_function = self._task['r_func']
        self.goal = self._task['goal']
        self.reset()
