import numpy as np
from social_gym.envs.robotics.syntheticFB import SyntFBEnv

# Copy task structure from https://github.com/jonasrothfuss/ProMP/blob/master/meta_policy_search/envs/mujoco_envs/ant_rand_goal.py


class SyntheticEnv(SyntFBEnv):
    def __init__(self, config=None, n_tasks=2, tasks = {}, **kwargs):
        super().__init__(config=config, **kwargs)
        if tasks != {}:
            self.tasks = tasks
        else:
            self.tasks = self.sample_tasks(n_tasks)
        self.reset_task(0)
        self.num_weights = 3

    def getReward(self):
        reward_vis = self.visual_reward()
        reward_aud = self.audio_reward()
        reward_mov, dir_reward = self.movement_reward()
        reward = self._vis * reward_vis + self._aud * reward_aud + \
            self._mov * reward_mov + self._mov * dir_reward
        return reward, reward_vis, reward_aud, reward_mov + dir_reward

    def sample_tasks(self, num_tasks):
        vis = np.random.uniform(0., 1.0, size=(num_tasks,))
        aud = np.random.uniform(0., 1.0-vis, size=(num_tasks,))
        mov = 1 - vis - aud
        tasks = [{'vis': v, 'aud': a, 'mov': m}
                 for v, a, m in zip(vis, aud, mov)]
        return tasks

    def get_all_task_idx(self):
        return range(len(self.tasks))

    def reset_task(self, idx):
        """
        Take an id as parameter
        intialize a new task and set up a new reward function
        """
        self._task = self.tasks[idx]
        self._vis = self._task['vis']
        self._aud = self._task['aud']
        self._mov = self._task['mov']
        self.goal = (self._vis, self._aud, self._mov)
        self.reset()
