import exputils as eu
import numpy as np
import gymnasium as gym
from social_gym.envs.robotics.minimal_mpi_env import MpiEnv

class SocialNavigationEnv(MpiEnv):
    
    def __init__(self, config=None, n_tasks=2, tasks = {}, **kwargs):
        super().__init__(config=config, **kwargs)
        if tasks != {}:
            self.tasks = tasks
        else:
            self.tasks = self.sample_tasks(n_tasks)
        self.reset_task(0)
        self.num_weights = 5
        
    def get_reward(self):
        goal_reward = self.get_goal_reward()
        collision_reward = self.get_collision_reward()
        social_reward = self.get_social_reward()
        approaching_reward = self.get_approaching_reward()
        velocity_reward = self.get_velocity_reward()
        reward = self.weight_goal*goal_reward + \
                self.weight_collision*collision_reward + \
                self.weight_social*social_reward + \
                self.weight_approaching*approaching_reward + \
                self.weight_velocity*velocity_reward
        return reward, goal_reward, collision_reward, social_reward, approaching_reward, velocity_reward         

    def sample_tasks(self, num_tasks):
        weight_goal = np.random.uniform(0., 1.0, size=(num_tasks,))
        weight_collision = np.random.uniform(0., 1.0, size=(num_tasks,))
        weight_social = np.random.uniform(0., 1.0, size=(num_tasks,))
        weight_approaching = np.random.uniform(0., 1.0, size=(num_tasks,))
        weight_velocity = np.random.uniform(0., 1.0, size=(num_tasks,))
        tasks = [{'goal': g, 'collision': c, 'social': s, 'approaching': a, 'velocity': v}
                 for g, c, s, a, v in zip(weight_goal, weight_collision, \
                     weight_social, weight_approaching, weight_velocity)]
        return tasks        

    def get_all_task_idx(self):
        return range(len(self.tasks))

    def reset_task(self, idx):
        """
        Take an id as parameter
        intialize a new task and set up a new reward function
        """
        self._task = self.tasks[idx]
        self.weight_goal = self._task['goal']
        self.weight_collision = self._task['collision']
        self.weight_social = self._task['social']
        self.weight_approaching = self._task['approaching']
        self.weight_velocity = self._task['velocity']
        self.goal = (self.weight_goal, self.weight_collision, self.weight_social, \
            self.weight_approaching, self.weight_velocity)
        self.reset()     
    