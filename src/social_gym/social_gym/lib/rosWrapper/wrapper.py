#!/usr/bin/env python
import time
import rospy
import message_filters

from litbot.msg import MSSL
from robot_com.msg import MotorsPosition, MotorsCmd, MotorCmd, MotorID

from sensor_msgs.msg import Image, CameraInfo

# import cv2
# import sys
# import numpy as np
import time
from cv_bridge import CvBridge, CvBridgeError

class Buffer:
    def __init__(self, image, motors_position, mssl_msg):
        self.image_left = image
        self.motors_position = motors_position
        self.mssl = mssl_msg
        self.faces = [] # list of 4-tuples : (top, left, bottom, right)



class Wrapper:
    def __init__(self, robot='nao'):
        rospy.myargv(argv="__ns:=litbot")
        rospy.init_node('listener', anonymous=False)
        self.connection = Connection(robot)
        #rospy.spin()

    def get_buffer(self):
        while self.connection.buffer is None:
            print("Waiting")
            try:
                time.sleep(1.5)
            except KeyboardInterruptError:
                break
        return self.connection.buffer

    def send_command(self, yaw=0.0, yaw_speed=0.0, pitch=0.0, pitch_speed=0.0, relative=True):
        self.connection.sendMotorCommand(yaw, yaw_speed, pitch, pitch_speed, relative)

    def reset_command(self):
        self.connection.resetMotorCommand()



class Connection:
    def __init__(self, robot):
        self.bridge = CvBridge()
        self.buffer = None

        if robot == 'nao':
            self.image_left_sub  = message_filters.Subscriber('stereo/left/image_raw', Image)
        elif robot == 'lito':
            self.image_left_sub  = message_filters.Subscriber('image_raw', Image)
        else:
            raise ValueError('Unknown robot type: {}. Only \'nao\' and \'lito\' are accepted.'.format(robot))
        self.motors_sub      = message_filters.Subscriber('motors_position', MotorsPosition)
        self.mssl_sub        = message_filters.Subscriber('mssl', MSSL)

        self.motors_command_pub = rospy.Publisher('motors_control', MotorsCmd, queue_size=10)

        self.ts = message_filters.ApproximateTimeSynchronizer([self.image_left_sub, self.motors_sub, self.mssl_sub], 10, 0.1)
        self.ts.registerCallback(self.callback_data)

        #set mssl peak threshold
        rospy.set_param('mssl_threshold', 0.03) #this value should work for Nao

    def callback_data(self, image, motors, mssl):
        # image
        cv_image = self.bridge.imgmsg_to_cv2(image, "bgr8")

        # motors_position
        info_msg = 'MotorsPosition [{time}]'.format(time=motors.header.stamp)
        msgs = []
        msgs.append(motors.header.stamp)
        for i in motors.motors_position:
            msgs.append((i.motor_id, i.position))
        # msgs: [timestamp, (motor_1, position_1), (motor_2, position_2)]

        # mssl
        mssl_msg = {"peaks":mssl.peaks, "ssl_map":mssl.sound_sources_map}
        self.buffer = Buffer(cv_image, msgs, mssl_msg)


    def sendMotorCommand(self, yaw=0.0, yaw_speed=0.0, pitch=0.0, pitch_speed=0.0, relative=True):
        motors_cmd = MotorsCmd()
        motors_cmd.header.stamp = rospy.Time.now()
        motors_cmd.type = MotorsCmd.CMD_TYPE_RELATIVE_POSITION if relative else MotorsCmd.CMD_TYPE_ABSOLUTE_POSITION

        if yaw_speed != 0.0:
            yaw_cmd = MotorCmd()
            yaw_cmd.motor_id = MotorID.HEAD_YAW
            yaw_cmd.goal_pos = yaw
            yaw_cmd.velocity = yaw_speed
            motors_cmd.cmd.append(yaw_cmd)

        if pitch_speed != 0.0:
            pitch_cmd = MotorCmd()
            pitch_cmd.motor_id = MotorID.HEAD_PITCH
            pitch_cmd.goal_pos = pitch
            pitch_cmd.velocity = pitch_speed
            motors_cmd.cmd.append(pitch_cmd)
        self.motors_command_pub.publish(motors_cmd)
        time.sleep(1)

    def resetMotorCommand(self, yaw=0.0, yaw_speed=0.3, pitch=0.0, pitch_speed=0.3):
        self.sendMotorCommand(yaw, yaw_speed, pitch, pitch_speed, False)


if __name__ == '__main__':
    Wrapper()
