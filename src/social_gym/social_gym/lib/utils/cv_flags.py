import cv2

def is_cv2():
    major = int(cv2.__version__.split(".")[0])
    if major == 2:
        return True
    elif major in [3, 4]:
        return False
    else:
        raise TypeError("Unsupported Opencv version")


def get_cv2flag_frame_count():
    return cv2.cv.CV_CAP_PROP_FRAME_COUNT if is_cv2() else cv2.CAP_PROP_FRAME_COUNT

def get_cv2flag_frame_pos():
    return cv2.cv.CV_CAP_PROP_POS_FRAMES if is_cv2() else cv2.CAP_PROP_POS_FRAMES

def get_fourcc(format):
    return cv2.cv.CV_FOURCC(*format) if is_cv2() else cv2.VideoWriter_fourcc(*format)
