import os
import sys
import datetime


def getPythonPath():
    return os.path.abspath(os.path.dirname(sys.argv[0]))

def getDateTime():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%dT%Hh%M")

def terminate():
    print(" ")
    print("Terminating...")
    sys.exit()


def pairwise(iterable):
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)
