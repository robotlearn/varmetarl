import cv2
import math
import numpy as np
import sys

from . import cv_flags


class VideoReader(object):

    def __init__(self, path):
        self.path  = path
        self.cap   = cv2.VideoCapture(path)
        self.frameCount = self.cap.get(cv_flags.get_cv2flag_frame_count())
        self.ret   = False
        self.videoFrame = 0
        self.key = 0
        self.pause = False

    def __del__(self):
        self.cap.release()

    def next(self):
        """
        Go to next frame
        Return True iff the next frame exists (video not finished)
        """
        self.ret, self.frame = self.cap.read()
        if self.ret:
            self.videoFrame += 1
        # Defensive programming
        assert self.videoFrame == self.cap.get(cv_flags.get_cv2flag_frame_pos())
        return self.ret

    def safeNext(self):
        self.goToFrame(self.videoFrame+1)

    def previous(self):
        self.goToFrame(self.videoFrame-1)

    def goToFrame(self, destFrame):
        mini = 1
        maxi = self.frameCount
        if destFrame<mini: destFrame=mini
        if destFrame>maxi: destFrame=maxi
        if destFrame == self.videoFrame:
            return
        elif destFrame == self.videoFrame+1:
            self.next()
        else:
            self.videoFrame = destFrame-1
            self.cap.set(cv_flags.get_cv2flag_frame_pos(), self.videoFrame)
            self.next()

    def getImage(self):
        if not self.ret:
            if self.videoFrame == 0:
                raise ValueError('Video before first frame')
            else:
                raise ValueError('Video exited')
        return self.frame


    def navigate(self, time=0):
        actual_time = 0 if self.pause else time
        self.key = cv2.waitKey(actual_time) & 0xFF
        if self.key == 27 or self.key == ord('n'):
            sys.exit()
        elif self.key == 82 or self.key == 81:
            self.previous()
        elif self.key == 84 or self.key == 83:
            self.safeNext()
        elif self.key == ord('p'):
            self.pause = not self.pause
        elif self.key == ord('w'):
            print("frame:", self.videoFrame)
