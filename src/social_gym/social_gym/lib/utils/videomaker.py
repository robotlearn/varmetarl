# Inspired from http://www.xavierdupre.fr/blog/2016-03-30_nojs.html for video
# Audio part comes from misc stackoverflow topics

import cv2
import uuid
import subprocess
import os

from . import cv_flags


class VideoMaker():

    def __init__(self, outfile, fps=25.0, size=None, is_color=True, format="MJPG"):
        if not os.path.exists("./video"):
            os.makedirs("./video")
        self.outfile = "./video/"+outfile
        self.tmp_outfile = "/tmp/video-"+str(uuid.uuid4())+".avi"
        self.size = size
        self.fps = float(fps)
        self.is_color = is_color
        self.fourcc = cv_flags.get_fourcc(format)
        self.format = format
        self.video = None
        self.complete = False
        self.include_sound = False

    def add(self, img):
        if self.complete:
            print("Warning: Trying to add images to complete video. Skipped.")
            return

        if self.video is None:
            if self.size is None:
                self.size = img.shape[1], img.shape[0]
            self.video = cv2.VideoWriter(
                self.tmp_outfile, self.fourcc, self.fps, self.size, self.is_color)

        if self.size[0] != img.shape[1] and self.size[1] != img.shape[0]:
            img = cv2.resize(img, self.size)

        self.video.write(img)

    def copy_sound(self, videoFileName):
        self.path_to_sound = "/tmp/sound-"+str(uuid.uuid4())+".mp3"
        command = "ffmpeg -i "+videoFileName+" -acodec copy "+self.path_to_sound
        subprocess.call(command, shell=True)
        self.include_sound = True

    def write(self):
        self.video.release()
        if self.include_sound:
            command = "ffmpeg -i "+self.tmp_outfile+" -i " + \
                self.path_to_sound+" -codec copy -shortest "+self.outfile
        else:
            command = "/bin/cp "+self.tmp_outfile+" "+self.outfile
        subprocess.call(command, shell=True)
        subprocess.call("rm "+self.tmp_outfile, shell=True)
        self.complete = True
