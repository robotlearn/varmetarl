import cv2
import numpy as np
import math
import matplotlib
from scipy.ndimage.filters import gaussian_filter

import sys
import os
os.environ["GLOG_minloglevel"] = "1"
import caffe

from .config_reader import config_reader
from . import utils


class FBpose:


    TOTAL_JOINTS= 18 # indexed 0 -> 17
    TOTAL_LIMBS = 19 # indexed 0 -> 18, linked to joints as below

    # the middle joints heatmap correpondence
    LIMB_HEATMAPS = [[31,32], [39,40], [33,34], [35,36], [41,42], [43,44], [19,20], [21,22], \
                     [23,24], [25,26], [27,28], [29,30], [47,48], [49,50], [53,54], [51,52], \
                     [55,56], [37,38], [45,46]]


    def __init__(self,configFile='./lib/poseEstimation/config'):
        self.param, self.model = config_reader(configFile)

        if self.param['use_gpu']:
            caffe.set_mode_gpu()
            caffe.set_device(self.param['GPUdeviceNumber']) # set to your device!
        else:
            caffe.set_mode_cpu()
        self.net = caffe.Net(self.model['deployFile'],1 , weights=self.model['caffemodel'])
        self.all_peaks=None
        self.skeletons=None
        self.oriImg=None
        self.paf_avg=None



    def computePose(self,oriImg,scaleFactor=1.0):
        """
        Compute the position of joints (heatmap peaks) in a given image
        """
        self.skeletons = None
        self.oriImg    = oriImg

        #### Compute heatmaps
        resizedImg=cv2.resize(oriImg, (0,0), fx=scaleFactor, fy=scaleFactor, interpolation=cv2.INTER_CUBIC)
        multiplier = [x * self.model['boxsize'] / resizedImg.shape[0] for x in self.param['scale_search']]

        heatmap_avg = np.zeros((resizedImg.shape[0], resizedImg.shape[1], 19))
        paf_avg = np.zeros((resizedImg.shape[0], resizedImg.shape[1], 38))
        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest = cv2.resize(resizedImg, (0,0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            imageToTest_padded, pad = utils.padRightDownCorner(imageToTest, self.model['stride'], self.model['padValue'])

            self.net.blobs['data'].reshape(*(1, 3, imageToTest_padded.shape[0], imageToTest_padded.shape[1]))
            #self.net.forward() # dry run
            self.net.blobs['data'].data[...] = np.transpose(np.float32(imageToTest_padded[:,:,:,np.newaxis]), (3,2,0,1))/256 - 0.5
            output_blobs = self.net.forward()

            # extract outputs, resize, and remove padding
            heatmap = np.transpose(np.squeeze(self.net.blobs[list(output_blobs.keys())[1]].data), (1,2,0))  # output 1 is heatmaps
            heatmap = cv2.resize(heatmap, (0,0), fx=self.model['stride'], fy=self.model['stride'], interpolation=cv2.INTER_CUBIC)
            heatmap = heatmap[:imageToTest_padded.shape[0]-pad[2], :imageToTest_padded.shape[1]-pad[3], :]
            heatmap = cv2.resize(heatmap, (resizedImg.shape[1], resizedImg.shape[0]), interpolation=cv2.INTER_CUBIC)

            paf = np.transpose(np.squeeze(self.net.blobs[list(output_blobs.keys())[0]].data), (1,2,0))  # output 0 is PAFs
            paf = cv2.resize(paf, (0,0), fx=self.model['stride'], fy=self.model['stride'], interpolation=cv2.INTER_CUBIC)
            paf = paf[:imageToTest_padded.shape[0]-pad[2], :imageToTest_padded.shape[1]-pad[3], :]
            paf = cv2.resize(paf, (resizedImg.shape[1], resizedImg.shape[0]), interpolation=cv2.INTER_CUBIC)

            # Weird hotfix because blob order seems to be random
            if heatmap.shape[2] == 38:
                print("WARNING: wrong order in output tensors (lib.poseEstimation.poseEstimation.FBPose)")
                heatmap, paf = paf, heatmap

            heatmap_avg = heatmap_avg + heatmap / len(multiplier)
            paf_avg = paf_avg + paf / len(multiplier)

        self.paf_avg= cv2.resize(paf_avg,(0,0), fx=1.0/scaleFactor, fy=1.0/scaleFactor, interpolation=cv2.INTER_CUBIC)


        #### Extract peaks from heatmaps
        self.all_peaks = []
        peak_counter = 0

        for part in range(19-1):
            x_list = []
            y_list = []
            map_ori = heatmap_avg[:,:,part]
            map = gaussian_filter(map_ori, sigma=3)

            map_left = np.zeros(map.shape)
            map_left[1:,:] = map[:-1,:]
            map_right = np.zeros(map.shape)
            map_right[:-1,:] = map[1:,:]
            map_up = np.zeros(map.shape)
            map_up[:,1:] = map[:,:-1]
            map_down = np.zeros(map.shape)
            map_down[:,:-1] = map[:,1:]

            peaks_binary = np.logical_and.reduce((map>=map_left, map>=map_right, map>=map_up, map>=map_down, map > self.param['thre1']))
            peaks = list(zip(np.nonzero(peaks_binary)[1], np.nonzero(peaks_binary)[0])) # note reverse
            peaksOri = list(zip((np.nonzero(peaks_binary)[1]/scaleFactor).astype(int), (np.nonzero(peaks_binary)[0]/scaleFactor).astype(int)))
            peaks_with_score = [xori + (map_ori[x[1],x[0]],) for xori,x in zip(peaksOri,peaks)]
            id = range(peak_counter, peak_counter + len(peaks))
            peaks_with_score_and_id = [peaks_with_score[i] + (id[i],) for i in range(len(id))]

            self.all_peaks.append(peaks_with_score_and_id)
            peak_counter += len(peaks)



    def computeLimbs(self):
        """
        Compute the position of limbs from a set of joints.
        Limbs are connections between valid pairs of joints
        """
        connection_all = []
        special_k = []
        mid_num = 10

        #### Compute limbs as most likely pair of joints (only valid pairs are considered)
        for k in range(FBpose.TOTAL_LIMBS):
            score_mid = self.paf_avg[:,:,[x-19 for x in FBpose.LIMB_HEATMAPS[k]]]
            candA = self.all_peaks[utils.LIMB_CONNECTIONS[k][0]]
            candB = self.all_peaks[utils.LIMB_CONNECTIONS[k][1]]
            nA = len(candA)
            nB = len(candB)
            if(nA != 0 and nB != 0):
                connection_candidate = []
                for i in range(nA):
                    for j in range(nB):
                        vec = np.subtract(candB[j][:2], candA[i][:2])
                        norm = math.sqrt(vec[0]*vec[0] + vec[1]*vec[1])
                        vec = np.divide(vec, norm)

                        startend = list(zip(np.linspace(candA[i][0], candB[j][0], num=mid_num), \
                                            np.linspace(candA[i][1], candB[j][1], num=mid_num)))

                        vec_x = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 0] \
                                          for I in range(len(startend))])
                        vec_y = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 1] \
                                          for I in range(len(startend))])

                        score_midpts = np.multiply(vec_x, vec[0]) + np.multiply(vec_y, vec[1])
                        if len(score_midpts)>0 and norm!=0:
                            score_with_dist_prior = sum(score_midpts)/len(score_midpts) + min(0.5*self.oriImg.shape[0]/norm-1, 0)
                            criterion1 = len(np.nonzero(score_midpts > self.param['thre2'])[0]) > 0.8 * len(score_midpts)
                            criterion2 = score_with_dist_prior > 0
                            if criterion1 and criterion2:
                                connection_candidate.append([i, j, score_with_dist_prior, score_with_dist_prior+candA[i][2]+candB[j][2]])

                connection_candidate = sorted(connection_candidate, key=lambda x: x[2], reverse=True)
                connection = np.zeros((0,5))
                for c in range(len(connection_candidate)):
                    i,j,s = connection_candidate[c][0:3]
                    if(i not in connection[:,3] and j not in connection[:,4]):
                        connection = np.vstack([connection, [candA[i][3], candB[j][3], s, i, j]])
                        if(len(connection) >= min(nA, nB)):
                            break

                connection_all.append(connection)
            else:
                special_k.append(k)
                connection_all.append([])


        #### Create skeleton by merging limbs having common joints

        # last number in each row is the total parts number of that person
        # the second last number in each row is the score of the overall configuration
        subset = np.ones((0, 20))
        self.indexed_peaks = np.array([item for sublist in self.all_peaks for item in sublist])

        for k in range(FBpose.TOTAL_LIMBS):
            if k not in special_k:
                partAs = connection_all[k][:,0]
                partBs = connection_all[k][:,1]
                indexA, indexB = np.array(utils.LIMB_CONNECTIONS[k])

                for i in range(len(connection_all[k])): #= 1:size(temp,1)
                    found = 0
                    subset_idx = [np.inf, np.inf]
                    for j in range(len(subset)): #1:size(subset,1):
                        if subset[j][indexA] == partAs[i] or subset[j][indexB] == partBs[i]:
                            subset_idx[found] = j
                            found += 1

                    if found == 1:
                        j = subset_idx[0]
                        if(subset[j][indexB] != partBs[i]):
                            subset[j][indexB] = partBs[i]
                            subset[j][-1] += 1
                            subset[j][-2] += self.indexed_peaks[partBs[i].astype(int), 2] + connection_all[k][i][2]
                    elif found == 2: # if found 2 and disjoint, merge them
                        j1, j2 = subset_idx

                        membership = ((subset[j1]>=0).astype(int) + (subset[j2]>=0).astype(int))[:-2]
                        if len(np.nonzero(membership == 2)[0]) == 0: #merge
                            subset[j1][:-2] += (subset[j2][:-2] + 1)
                            subset[j1][-2:] += subset[j2][-2:]
                            subset[j1][-2] += connection_all[k][i][2]
                            subset = np.delete(subset, j2, 0)
                        else: # as like found == 1
                            subset[j1][indexB] = partBs[i]
                            subset[j1][-1] += 1
                            subset[j1][-2] += self.indexed_peaks[partBs[i].astype(int), 2] + connection_all[k][i][2]

                    # if find no partA in the subset, create a new subset
                    elif not found and k < 17:
                        row = -1 * np.ones(20)
                        row[indexA] = partAs[i]
                        row[indexB] = partBs[i]
                        row[-1] = 2
                        row[-2] = sum(self.indexed_peaks[connection_all[k][i,:2].astype(int), 2]) + connection_all[k][i][2]
                        subset = np.vstack([subset, row])

        # delete some rows of subset which has few parts occur
        deleteIdx = [];
        for i in range(len(subset)):
            if subset[i][-1] < 4 or subset[i][-2]/subset[i][-1] < 0.4:
                deleteIdx.append(i)
        subset = np.delete(subset, deleteIdx, axis=0)

        # self.skeletons = subset
        self.skeletons = [[self.indexed_peaks[int(index)][:2].astype(int) if index >= 0 else np.array([np.inf, np.inf]) for index in skeleton[:-2]] for skeleton in subset]




    def getAnnotatedImage(self):
        """
        Annotate the original image with detected joints (and limbs if computed)
        """
        if self.all_peaks is None or self.oriImg is None:
            sys.exit("compute the pose before displaying")

        outImg = utils.annotateImage(self.oriImg.copy(), self.all_peaks, self.skeletons)

        return outImg





    def savePose(self,fileName):
        """
        Save the annotated image
        """
        cv2.imwrite(fileName, self.getAnnotatedImage())
