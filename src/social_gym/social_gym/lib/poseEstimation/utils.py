import numpy as np
import cv2
import itertools
import math




# For visualization
POSE_COLORS = [[255, 0, 0],   [255, 85, 0],  [255, 170, 0], \
               [255, 255, 0], [170, 255, 0], [85, 255, 0],  \
               [0, 255, 0],   [0, 255, 85],  [0, 255, 170], \
               [0, 255, 255], [0, 170, 255], [0, 85, 255],  \
               [0, 0, 255],   [85, 0, 255],  [170, 0, 255], \
               [255, 0, 255], [255, 0, 170], [255, 0, 85]   ]

LIMB_CONNECTIONS = [[ 1,  2], [ 1,  5], [ 2,  3], [ 3,  4], [ 5,  6],\
                    [ 6,  7], [ 1,  8], [ 8,  9], [ 9, 10], [ 1, 11],\
                    [11, 12], [12, 13], [ 1,  0], [ 0, 14], [14, 16],\
                    [ 0, 15], [15, 17], [ 2, 16], [ 5, 17]           ]

def padRightDownCorner(img, stride, padValue):
    h = img.shape[0]
    w = img.shape[1]

    pad = 4 * [None]
    pad[0] = 0 # up
    pad[1] = 0 # left
    pad[2] = 0 if (h%stride==0) else stride - (h % stride) # down
    pad[3] = 0 if (w%stride==0) else stride - (w % stride) # right

    img_padded = img
    pad_up = np.tile(img_padded[0:1,:,:]*0 + padValue, (pad[0], 1, 1))
    img_padded = np.concatenate((pad_up, img_padded), axis=0)
    pad_left = np.tile(img_padded[:,0:1,:]*0 + padValue, (1, pad[1], 1))
    img_padded = np.concatenate((pad_left, img_padded), axis=1)
    pad_down = np.tile(img_padded[-2:-1,:,:]*0 + padValue, (pad[2], 1, 1))
    img_padded = np.concatenate((img_padded, pad_down), axis=0)
    pad_right = np.tile(img_padded[:,-2:-1,:]*0 + padValue, (1, pad[3], 1))
    img_padded = np.concatenate((img_padded, pad_right), axis=1)

    return img_padded, pad

#if __name__ == "__main__":
#    config_reader()



def annotateImage(img, landmarks=None, skeletons=None):

    if landmarks is not None:
        for joint, color in zip(landmarks, POSE_COLORS):
            for detection in joint:
                cv2.circle(img, detection[0:2], 4, color, -1)

    stickwidth = 4
    if skeletons is not None:
        annot = img.copy()
        for skl in skeletons:
            for limb in LIMB_CONNECTIONS[:-2]:
                # Discard the 2 last "limbs" (shoulder to ear)
                j1x = skl[limb[0]][0]
                j1y = skl[limb[0]][1]
                j2x = skl[limb[1]][0]
                j2y = skl[limb[1]][1]
                if np.isinf(j1x) or np.isinf(j1y) or np.isinf(j2x) or np.isinf(j2y):
                    continue
                length = ((j1x - j2x) ** 2 + (j1y - j2y) ** 2) ** 0.5
                if np.isinf(length) or np.isnan(length):
                    continue
                angle = math.degrees(math.atan2(j1y - j2y, j1x - j2x))
                polygon = cv2.ellipse2Poly((int((j1x+j2x)/2),int((j1y+j2y)/2)), (int(length/2), stickwidth), int(angle), 0, 360, 1)
                cv2.fillConvexPoly(annot, polygon, POSE_COLORS[limb[1]])
        img = cv2.addWeighted(img, 0.4, annot, 0.6, 0)

    return img
