# import os
import numpy as np
import cv2
import socket
import itertools
# import time
import random

from social_gym.lib.poseEstimation.utils import annotateImage
from social_gym.lib.utils import pairwise
from social_gym.lib.data.avdia import AVDIA_ROOT_PATH


# if socket.gethostname()=="auriga" or socket.gethostname()=="bootes" or socket.gethostname()=="andromeda":
#     rootpathCluster="/mnt/beegfs/"
# else:
#     rootpathCluster="/services/scratch/"


def genSilhouetteList(fileName):
    """
    Reads a file of poses (each line contains "idx x1 y1 x2 y2 .... xn yn").
    Returns a list of tuples ([x1/s, x2/s, ..., xn/s], [y1/s, y2/s, ..., yn/s])
    Where s is a scaling factor based on vertical amplitude
    """
    print("times")
    with open(fileName) as f:
        lines = f.readlines()
    # coord=[map(lambda x: float(x), l.split(" "))[1:] for l in lines]
    coord = [list(map(lambda x: float(x), l.split(" ")))[1:] for l in lines]


    silhouettes=[]
    for det in coord:
        faceY=det[1]
        faceX=det[0]
        if faceX>0 and faceY>0:
            Ys  = [y for x,y in pairwise(det) if y>=0]
            s   = np.max(Ys) - np.min(Ys)
            sil = [[np.inf, np.inf] if x<0 else [(x-faceX)/s, (y-faceY)/s] for x,y in pairwise(det)]
            silhouettes.append(sil)

    return silhouettes



def distancePose(pose_1, pose_2):
    """
    Euclidean distance between poses.
    Non detected landmark are ignored (
    """
    sumdist = 0
    nbOK = 0
    for x1, x2 in zip(pose_1, pose_2):
        if np.isinf(x1[0]) or np.isinf(x2[0]):
            continue
        nbOK += 1
        sumdist += np.sqrt((x1[0]-x2[0])**2 + (x1[1]-x2[1])**2)
    if nbOK<4:
        return np.inf
    return sumdist/nbOK



import time
class Person():
    SILHOUETTES=genSilhouetteList(AVDIA_ROOT_PATH+"/allposes.txt")

    def __init__(self):
        self.i=random.uniform(0, 1) # Top and Bottom
        self.j=random.uniform(0, 1) # Left and Right

        if random.randint(0,1)==0:
            self.iVar=random.uniform(0.25, 0.5)
        else:
            self.iVar=-random.uniform(0.25, 0.5)
        if random.randint(0,1)==0:
            self.jVar=random.uniform(0.25, 0.5)
        else:
            self.jVar=-random.uniform(0.25, 0.5)
        self.alpha=0.0750
        self.beta=0.0
        self.speaking=False
        self.neverSeen=True
        self.changePose()
        #self.sizeSubsetPose=100
        self.sizeSubsetPose = 1
        self.itochange=True
        self.jtochange=True
        self.isMoving = random.choice([True, False])
        self.last_time = -np.inf
        self.last_position = (np.inf, np.inf)


    def changePose(self,sizeSet=1):
        """
        Switch pose to the closest within a random subset
        """

        if sizeSet==1:
            self.pose = random.choice(Person.SILHOUETTES)
        else:
            subsetP = [random.choice(Person.SILHOUETTES) for _ in range(sizeSet)]
            distP = [distancePose(self.pose, p) for p in subsetP]
            self.pose = subsetP[np.argmin(distP)]
            #print("closest silhouette:" + str(np.argmin(distP)))



    def stay(self,changeDirection=1.0):
        """

        """
        if random.uniform(0.0, 1.0) < changeDirection:
            self.itochange=True
            self.jtochange=True
        self.changePose(self.sizeSubsetPose)


    def move(self):
        """
        TODO: Understand this algorithm
        """
        if self.itochange:
            sign = random.choice([-1,1])
            self.iVar = sign * random.uniform(0.25, 0.5)

        if self.jtochange:
            sign = random.choice([-1,1])
            self.jVar = sign * random.uniform(0.25, 0.5)

        self.itochange=False
        self.jtochange=False

        VarVeloI=random.uniform(-1, 1)
        VarVeloJ=random.uniform(-1, 1)
        inew=self.i+self.alpha*self.iVar+self.beta*VarVeloI
        jnew=self.j+self.alpha*self.jVar+self.beta*VarVeloJ

        if inew > 0 and inew < 1 and jnew > 0 and jnew < 1:
            self.iVar = (inew - self.i) / self.alpha
            self.jVar = (jnew - self.j) / self.alpha

        else:
            if inew < 0 or inew > 1:
                self.iVar = -self.iVar

            if jnew < 0 or jnew > 1:
                if random.randint(0, 1) == 0:  # change side
                    jnew = abs(1 - abs(jnew))
                else:
                    self.jVar = -self.jVar

        self.i=inew
        self.j=jnew

        # change pose
        self.changePose(self.sizeSubsetPose)


    def getSkeleton(self, fWidth, fHeight):
        scale = 0.16 * (fHeight+fWidth)
        faceX = int(self.j * fWidth)
        faceY = int(self.i * fHeight)
        return [[np.inf, np.inf] if np.isinf(x+y) else [int(faceX+scale*x), int(faceY+scale*y)] for x,y in self.pose]


    def annotateImage(self,img):
        """
        Draw circle on top of an image
        """
        fHeight, fWidth = img.shape[:2]
        annotated = img.copy()

        skeleton = self.getSkeleton(fWidth, fHeight)

        cv2.circle(annotated,tuple(skeleton[0]),10,(255,255,255))
        if self.speaking:
            cv2.circle(annotated,tuple(skeleton[0]),45,(255,255,255),10)

        return annotateImage(annotated, None, [skeleton])



    def inField(self):
        return self.i>=0 and self.i<1 and self.j>=0 and self.j<1


    def isVisible(self, t, l, b, r):
        return self.j>=l and self.j<r and self.i>=t and self.i<b
