from .person import Person
import random
import numpy as np
import cv2


class SyntheticGenerator():

    def __init__(self, NbPeople, pMoving, pStaying):
        self.NbPeople = NbPeople
        self.people = []
        self.idSpeaking = self.NbPeople  # Nobody speaking
        self.pMoving = pMoving
        self.pStaying = pStaying
        self.time = 0
        self.map = np.array([7, 21])

    def reset(self, NbPeople=None, pMoving=None, pStaying=None):
        """
        Reset the number of people in the field of view
        """
        if NbPeople is not None:
            self.NbPeople = NbPeople
        self.people = [Person() for _ in range(self.NbPeople)]
        if pMoving is not None:
            self.pMoving = pMoving
        if pStaying is not None:
            self.pStaying = pStaying
        self.time = 0

    def step(self, t, l, b, r):
        """
        Algorithm to compute realistic behavior of people in a crowd
        """
        for index, pers in enumerate(self.people):
            # Location
            if not pers.inField():
                pers.isMoving = True
            if pers.isMoving:
                if random.uniform(0, 1) < self.pMoving:
                    pers.move()
                    pers.isMoving = True
                else:
                    pers.stay(changeDirection=0.5)
                    pers.isMoving = False
            else:
                if random.uniform(0, 1) < self.pStaying:
                    pers.stay(changeDirection=0.5)
                    pers.isMoving = False
                else:
                    pers.move()
                    pers.isMoving = True

        # Speaking behavior to change
        if self.idSpeaking == self.NbPeople:  # if nobody
            if random.uniform(0, 1) < 0.2:
                self.idSpeaking = random.randint(
                    0, self.NbPeople - 1)  # we chose a random speaker
        else:  # if someone is speaking
            if random.uniform(0, 1) < 0.02:
                self.idSpeaking = self.NbPeople  # nobody speaks

        for idx in range(len(self.people)):
            if idx == self.idSpeaking:
                self.people[idx].speaking = True
            else:
                self.people[idx].speaking = False
        self.time += 1
        
    def getSSL(self, obs_ssl_width, obs_ssl_height, sslAccuracy):
        """
        return the SSL observation from the speaking of the different people in the scene
        """
        ssl = np.zeros((obs_ssl_height, obs_ssl_width), dtype=np.float)
        for pers in self.people:
            # Speaking obs + reward
            if pers.speaking and pers.inField():
                xssl = int(pers.i * obs_ssl_width)
                yssl = int(pers.j * obs_ssl_height)
                if random.uniform(0, 1) < sslAccuracy:
                    ssl[yssl, xssl] = 1.0
        return ssl

    def getBlurredSSL(self, obs_ssl_width, obs_ssl_height, sslAccuracy):
        """
        return blurred SSL observation (in a fashion similar to the one in avdia)
        """
        ssl = self.getSSL(obs_ssl_width, obs_ssl_height, sslAccuracy)
        blurred = cv2.GaussianBlur(ssl, (5, 5), 0)
        if np.max(blurred) != 0:
            return blurred / np.max(blurred)
        else:
            return ssl

    def getVisual(self, obs_fov_width, obs_fov_height, obs_fov_dim, panoramaWidth, panoramaHeight, t, l, v_fov_height, v_fov_width, detectAccuracy):
        """
        return the key point from visual scene
        """
        vis = np.zeros((obs_fov_height, obs_fov_width,
                        obs_fov_dim), dtype=np.float)
        for pers in self.people:
            for (jx, jy), idx in zip(pers.getSkeleton(panoramaWidth, panoramaHeight), range(obs_fov_dim)):
                if np.isinf(jx) or np.isinf(jy):
                    continue
                vis_y = int((float(jy) / panoramaHeight - t) /
                            v_fov_height * obs_fov_height - 0.05)
                vis_x = int((float(jx) / panoramaWidth - l) /
                            v_fov_width * obs_fov_width - 0.05)
                if 0 <= vis_y < obs_fov_height and vis_x >= 0 and vis_x < obs_fov_width:
                    vis[vis_y, vis_x, idx] = 1.0
        return vis
