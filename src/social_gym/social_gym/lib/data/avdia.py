import numpy as np
import cv2
import scipy.io
import os

from social_gym.lib.utils import VideoReader
from social_gym.lib.utils import pairwise

#AVDIA_ROOT_PATH = "/scratch/andromeda/aballou/data"
AVDIA_ROOT_PATH = os.path.dirname(__file__) + "/../../data"
# AVDIA_ROOT_PATH = "/scratch/auriga/masse/artds/"
# AVDIA_ROOT_PATH = "/scratch/andromeda/aballou/.remi_projects/preference_based_learning"
# AVDIA_ROOT_PATH = "/home/tawy/Public/decrypted-data/datasets/artds/"
# AVDIA_ROOT_PATH = "/mnt/beegfs/perception/masse/datasets/artds"
# AVDIA_ROOT_PATH = "/mnt/beegfs/perception/slathuil/artds"
# AVDIA_ROOT_PATH = "/scratch/paragorn/masse/datasets/AVDiar/artds"


AVDIA_VIDEOS = [
    #AVDIA_ROOT_PATH+"/LivingRoom/fighting-05/",
    #AVDIA_ROOT_PATH+"/LivingRoom/fighting-06/",
    #AVDIA_ROOT_PATH+"/LivingRoom/fighting4/",
    #AVDIA_ROOT_PATH+"/LivingRoom/fighting/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Phone2/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Phone3/", ### NO SSL !
    #AVDIA_ROOT_PATH+"/LivingRoom/Phone4/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Phone/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Queue2/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Queue3/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Queue4/",
    #AVDIA_ROOT_PATH+"/LivingRoom/Queue/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-handshaking-MeetingRoom01/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-handshaking-MeetingRoom02/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-handshaking-MeetingRoom03/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-handshaking-MeetingRoom04/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-handshaking-MeetingRoom07/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Kiss-MeetingRoom01/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Kiss-MeetingRoom02/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Kiss-MeetingRoom04/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Phone-MeetingRoom01/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Phone-MeetingRoom03/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Phone-MeetingRoom04/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Queue-MeetingRoom01/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Queue-MeetingRoom03/",
    #AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Queue-MeetingRoom05/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario01-3/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario01-2/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario02-2/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario04-03/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario06-02/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario-07/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario08/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario10/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario01-MeetingRoom02/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario02-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario03-MeetingRoom02/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario04-MeetingRoom02/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario06-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario07-MeetingRoom05/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario08-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario09-MeetingRoom01/"
]
AVDIA_VIDEOS_TEST = [
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario02/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario04-02/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario04/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario06/",
    AVDIA_ROOT_PATH+"/LivingRoom/Scenario-07-02/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario01-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario03-MeetingRoom03/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario04-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario07-MeetingRoom01/",
    AVDIA_ROOT_PATH+"/MeetingRoom/Stephane-Scenario10-MeetingRoom01/"
]


AVDIA_W = 1024
AVDIA_H = 640


class DataReader():
    def __init__(self, folder, SSL_W=AVDIA_W, SSL_H=AVDIA_H, flip=False, use_cache=True, read_detections=True):
        # Setting up paths
        self.rootpath = folder.rstrip('/')
        scenarioName = self.rootpath.split('/')[-1]
        self.videoPath = self.rootpath+"/Video/"+scenarioName+"1024.avi"
        self.sslroot = self.rootpath+"/SSL/SSL"
        # Hotfix due to varying file architecture
        if not os.path.exists(self.sslroot):
            self.sslroot = self.rootpath+"/SSL"

        # Reading annotations
        if read_detections:
            self.readDetections()

        # Local variables
        self.idx = 0
        self.flip = flip
        #print("")
        #print("## flip : "+str(self.flip))

        self.SSL_W = SSL_W
        self.SSL_H = SSL_H
        # self.facUpToDate = False # Cache

        # For camera images
        self.brightness = 0.7
        self.video = None

        # SSL cache management
        self.sslcache = self.rootpath+"/SSLCache"
        self.use_cache = use_cache
        if self.use_cache and not os.path.exists(self.sslcache):
            os.makedirs(self.sslcache)
        self.sslUpToDate = False
        self.sslcache_id = "_"+str(SSL_W)+"x"+str(SSL_H)
        # self.sslcache_id = "_"+str(SSL_W)+"x"+str(SSL_H)+"_"+("flip" if self.flip else "norm")

    def countFrames(self):
        return len([name for name in os.listdir(self.sslroot) if len(name) == 14 and name[-3:] == "mat" and os.path.isfile(self.sslroot+"/"+name)])

    def next(self, deltaT=1):
        """
        Move forward in the current AVDiar video to the frame indexed (CURRENT_FRAME + DELTA_T).
        """

        self.idx += deltaT
        self.sslUpToDate = False
        # self.facUpToDate = False
        self.SSLFile = self.sslroot+"/ssl-"+format(self.idx, '06')+'.mat'
        self.SSLFileSerial = self.sslcache+"/ssl-" + \
            format(self.idx, '06')+self.sslcache_id+'.mat'
        return os.path.exists(self.SSLFile)  # True if SSL file exists

    def readDetections(self):
        """
        Reads annotation file containing full body pose estimations
        """
        BBs = open(self.rootpath+"/annotations/FBpose.txt")
        lines = BBs.readlines()
        idPrev = 1
        self.detections = {}
        currentDet = []
        for idx, line in enumerate(lines):
            elements = line.split()
            idxFrame = int(elements[0])
            if idxFrame != idPrev:
                for i in range(idPrev, idxFrame):
                    self.detections[i] = currentDet
                    currentDet = []
            currentDet.append([int(x) for x in elements])
            idPrev = idxFrame

    def getFaces(self):
        """
        From a set of detections {idx -> [idx,x,y,...]}, returns the set (x,y) s.t. idx=self.idx
        x and y are expected to be the coordinates of a nose landmark
        """
        detections = [det[1:3] for det in self.detections[self.idx]
                      ] if self.idx in self.detections else []
        if self.flip:
            return [[AVDIA_W-det[0], det[1]] for det in detections]
        else:
            return detections

    def getDetections(self):
        """
        Return a list containing (x,y) for all 18 landmarks
        Coordinates are in pixel, or (inf, inf) for undetected landmarks
        """
        # detections = [det[1:] for det in self.detections[self.idx]] if self.idx in self.detections else []
        detections = [
            [list(point) if point[0] >= 0 else [np.inf, np.inf]
             for point in pairwise(det[1:])]
            for det in self.detections[self.idx]
        ] if self.idx in self.detections else []

        if self.flip:
            return [[[AVDIA_W-p[0], p[1]] for p in det] for det in detections]
        else:
            return detections

    def getSSL(self):
        """
        Return the raw SSL heatmap (punctual because unblurred)
        """
        SSL = scipy.io.loadmat(self.SSLFile)['x']
        # SSL = cv2.resize(SSL,(self.SSL_W, self.SSL_H))
        if self.flip:
            SSL = cv2.flip(SSL, 1)
        return SSL

    def getCleanSSL(self):
        """
        Return the SSL heatmap in which potential NaN have been removed (still punctual)
        """
        SSL = self.getSSL()
        if np.isnan(np.sum(SSL)):
            SSL = np.vectorize(lambda x: 0 if np.isnan(x) else x)(SSL)
        return SSL

    def getBlurredSSL(self):
        """
        Return the continous SSL, applying gaussian blur
        (adaptive blur scale, relative to desired output size)
        """
        SSL = self.getCleanSSL()
        print("SSL", np.max(SSL))
        print(SSL)
        blurScale = int(((AVDIA_W+AVDIA_H)/16)*2+1)
        print("blurScale")
        print(blurScale)
        Blurred = blurScale * cv2.GaussianBlur(SSL, (blurScale, blurScale), 0)
        print("Blurred1", Blurred.shape, np.max(Blurred))
        print(Blurred)
        Blurred = np.minimum(1.0, Blurred*1000)
        print("Blurred2", Blurred.shape, np.max(Blurred))
        print(Blurred)
        print("resize", cv2.resize(Blurred, (self.SSL_W, self.SSL_H)).shape,
              np.max(cv2.resize(Blurred, (self.SSL_W, self.SSL_H))))
        print(cv2.resize(Blurred, (self.SSL_W, self.SSL_H)))
        return cv2.resize(Blurred, (self.SSL_W, self.SSL_H))

    def getSerialBlurredSSL(self):
        """
        Same output as getBlurredSSL but use cache and memory management for efficientcy
        """
        ssl = self.getBlurredSSL()
        if not self.sslUpToDate:
            try:
                ssl = scipy.io.loadmat(self.SSLFileSerial)['x']
                # Only store unflipped SSL
                self.ssl = cv2.flip(ssl, 1) if self.flip else ssl
            except:
                self.ssl = self.getBlurredSSL()
                if self.use_cache:
                    ssl = cv2.flip(self.ssl, 1) if self.flip else self.ssl
                    scipy.io.savemat(self.SSLFileSerial, {'x': ssl})
            self.sslUpToDate = True
        return self.ssl

    # VIDEO READING
    # Video is never read unless one of these functions has been called
    # - getVideoImage
    # - getFusionSSLVideo

    def getVideoImage(self):
        if self.video is None:
            self.video = VideoReader(self.videoPath)
        self.video.goToFrame(self.idx)
        image = self.video.getImage()
        if self.idx != self.video.videoFrame:
            print("Warning: the image may not be synchronized with annotations (annotation from frame "
                  + str(self.idx)+", image from frame "+str(self.video.videoFrame)+"/"+str(self.video.frameCount)+".)")

        if self.flip:
            image = cv2.flip(image, 1)
        return image

    def getFusionSSLVideo(self):
        video_image = self.getVideoImage().copy()
        ssl = self.getSerialBlurredSSL()
        mask = cv2.resize(ssl, video_image.shape[1::-1])
        mask = cv2.applyColorMap((255*mask).astype(np.uint8), cv2.COLORMAP_JET)
        self.fusion = cv2.addWeighted(
            video_image, self.brightness, mask, 1-self.brightness, 0)
        return self.fusion
