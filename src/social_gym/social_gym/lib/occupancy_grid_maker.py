import numpy as np
from skimage.transform import resize
import mpi_sim
import cv2

class OccupancyGrid():
    ''' Create an Occupancy Grid with the position of static objects and other parameters '''
    def __init__(self, nb_channels, local_map_depth, desired_grid_size, ground_boundaries_in_coords, resize = True):
        self.nb_channels = nb_channels
        self.desired_grid_size = desired_grid_size 
        self.local_map_depth = local_map_depth
        self.row, self.col = self.desired_grid_size
        self.resize = resize
        self.ground_boundaries_in_coords = ground_boundaries_in_coords



    def global_coord_to_local_coord(self, ari_infos, entities_positions_global):
        ''' Take the global coordinates (in float) and transform it to local coordinates (in float) '''
        (position_ari , orientation_ari) = ari_infos
        # Translation (ARI is now the reference position)
        entities_positions_local = np.array(entities_positions_global) - np.array(position_ari)
        # Rotation (ARI is now the reference orientation)
        rotation_matrix = np.array([[np.cos(orientation_ari), -np.sin(orientation_ari)],[np.sin(orientation_ari), np.cos(orientation_ari)]])
        entities_positions_local = np.transpose(np.dot(rotation_matrix, np.transpose(entities_positions_local)))
        assert len(entities_positions_local) == len(entities_positions_global)
        return entities_positions_local
    
    def position_to_px_local_grid(self, position):
        map_area = ((-self.local_map_depth, self.local_map_depth), (-self.local_map_depth, self.local_map_depth))
        resolution = 2*self.local_map_depth / self.row # new resolution in the new resized map 
        return (mpi_sim.utils.transform_position_to_map_coordinate(position, map_area, resolution))

    
    def grid_generator(self, complete_occupancy_grid, static_occupancy_grid, ari_infos, entities_positions_global, entities_relative_orientation, entities_velocity):
        ''' Generates an occupancy grid with the desired nb of channels (either 1, 3, 4) '''
        # Creation of occupancy grid
        if self.resize :
            self.row, self.col  = self.desired_grid_size
            static_occupancy_grid = resize(static_occupancy_grid, self.desired_grid_size, anti_aliasing=True)
        
        if self.nb_channels == 1 :
            complete_occupancy_grid = static_occupancy_grid
            #print(np.shape(complete_occupancy_grid))
            
        elif self.nb_channels == 3 :
            # Adding the static map
            complete_occupancy_grid[0,:,:] = static_occupancy_grid
            complete_occupancy_grid[1,:,:] = np.where(complete_occupancy_grid[1,:,:] == 1, 0, 0)
            complete_occupancy_grid[2,:,:] = np.where(complete_occupancy_grid[2,:,:] == 1, 0, 0)
            
            # Adding the humans if they exists
            if len(entities_positions_global) > 0 :
                # Computing the new coordinates in the local reference
                entities_coord_local = self.global_coord_to_local_coord(ari_infos, entities_positions_global)
                #print(entities_coord_local)
                for id in range(len(entities_coord_local)):
                    entitity_coord = entities_coord_local[id]
                    if (entitity_coord[0] > - self.local_map_depth
                        and entitity_coord[0] < self.local_map_depth
                        ) and (
                            entitity_coord[1] > - self.local_map_depth 
                            and entitity_coord[1] < self.local_map_depth
                            ) :
                        # Transform float coordinates in int coordinates
                        x_entity, y_entity = self.position_to_px_local_grid(entities_coord_local[id])
                        complete_occupancy_grid[1, x_entity, y_entity] = 1
                        complete_occupancy_grid[2, x_entity, y_entity] = entities_relative_orientation[id]

        elif self.nb_channels == 4 :
            # Adding the static map
            complete_occupancy_grid[0,:,:] = static_occupancy_grid
            complete_occupancy_grid[1,:,:] = np.where(complete_occupancy_grid[1,:,:] == 1, 0, 0)
            complete_occupancy_grid[2,:,:] = np.where(complete_occupancy_grid[2,:,:] == 1, 0, 0)
            complete_occupancy_grid[3,:,:] = np.where(complete_occupancy_grid[3,:,:] == 1, 0, 0)
            
            # Adding the humans if they exists
            if len(entities_positions_global) > 0 :
                # Computing the new coordinates in the local reference
                entities_coord_local = self.global_coord_to_local_coord(ari_infos, entities_positions_global)
                #print(entities_coord_local)
                for id in range(len(entities_coord_local)):
                    entitity_coord = entities_coord_local[id]
                    if (entitity_coord[0] > - self.local_map_depth
                        and entitity_coord[0] < self.local_map_depth
                        ) and (
                            entitity_coord[1] > - self.local_map_depth 
                            and entitity_coord[1] < self.local_map_depth
                            ) :
                        # Transform float coordinates in int coordinates
                        x_entity, y_entity = self.position_to_px_local_grid(entities_coord_local[id])
                        complete_occupancy_grid[1, x_entity, y_entity] = 1
                        complete_occupancy_grid[2, x_entity, y_entity] = entities_relative_orientation[id]
                        complete_occupancy_grid[3, x_entity, y_entity] = entities_velocity[id]
        else :
            print("Error with the nb of channels, it can only be equal to 1, 3 or 4")
            
        
        
        

