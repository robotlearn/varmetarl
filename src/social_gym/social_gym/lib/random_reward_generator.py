#import exputils as eu
import numpy as np
import exputils as eu
import exputils.data.logging as log

def sample_discrete_distribution(distr):
    tmp = np.cumsum(distr)
    sampled_idx = np.where(tmp >= np.random.rand())[0][0]
    return sampled_idx

class NumpyRandomRewardFunction():
    """
    Randomly generated reward function that is computed via numpy operations.

    Usage:
        reward_func = NumpyRandomRewardFunction(neurons=neurons)
        r = reward_func(input_values)
    """
    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            neurons=[],
            return_type='scalar'  # 'scalar', 'list', 'ndarray'
        )
        return dc

    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # create the reward function from the given list of neurons
        my_func_str = ""
        output_list = ''
        for neuron_idx, neuron in enumerate(self.config.neurons):

            if neuron['type'] == 'input':
                # get input value
                my_func_str += "n_{} = input_value[{}]\n".format(neuron_idx, neuron['index'])

            elif neuron['type'] in ['hidden', 'output']:
                # calc net input

                my_func_str += "\n# Neuron {}\n".format(neuron_idx)

                # get all input neurons as array
                temp = ['n_{}'.format(n_idx) for n_idx in neuron['input_neurons']]
                my_func_str += "n_{}_inputs = np.array([{}])\n".format(
                    neuron_idx,
                    ', '.join(temp)
                )

                # get weights
                temp = ['{}'.format(w) for w in neuron['weights']]
                my_func_str += "n_{}_weights = np.array([{}])\n".format(
                    neuron_idx,
                    ', '.join(temp)
                )

                # compute net input to neuron
                my_func_str += "n_{}_net_input = np.{}(n_{}_inputs * n_{}_weights)\n".format(
                    neuron_idx,
                    neuron['net_input_func'][0],
                    neuron_idx,
                    neuron_idx
                )

                # process activation functions
                my_func_str += "n_{} = {} + ".format(neuron_idx, neuron['bias'])
                if neuron['activation_func'][0] is None or neuron['activation_func'][0] == '':
                    my_func_str += "n_{}_net_input\n".format(neuron_idx)

                elif neuron['activation_func'][0] == 'exp' and len(neuron['activation_func']) == 1:
                    my_func_str += "np.exp(n_{}_net_input)\n".format(neuron_idx)
                elif neuron['activation_func'][0] == 'exp' and len(neuron['activation_func']) == 2:
                    my_func_str += "{}**n_{}_net_input\n".format(neuron['activation_func'][1], neuron_idx)

                elif neuron['activation_func'][0] == 'log' and len(neuron['activation_func']) == 1:
                    my_func_str += "np.log(np.abs(n_{}_net_input))\n".format(neuron_idx)
                elif neuron['activation_func'][0] == 'log' and len(neuron['activation_func']) == 2:
                    my_func_str += "np.log(np.abs(n_{}_net_input))/np.log({})\n".format(neuron_idx, neuron['activation_func'][1])

                elif neuron['activation_func'][0] == 'sqrt':
                    my_func_str += "np.sqrt(n_{}_net_input)\n".format(neuron_idx)

                elif neuron['activation_func'][0] == 'power':
                    my_func_str += "n_{}_net_input**{}\n".format(neuron_idx, neuron['activation_func'][1])

                elif neuron['activation_func'][0] == 'sigm':
                    my_func_str += "1/(1 + np.exp(-n_{}_net_input))\n".format(neuron_idx)

                elif neuron['activation_func'][0] == 'logistic' and len(neuron['activation_func']) == 4:
                    my_func_str += "{}/(1 + np.exp(-{} * (n_{}_net_input - {})))\n".format(
                        neuron['activation_func'][1],
                        neuron['activation_func'][2],
                        neuron_idx,
                        neuron['activation_func'][3])

                else:
                    raise ValueError(
                        'Unknown activation function {!r} for neuron {}!'.format(neuron['activation_func'], neuron_idx))

                # create list of output neurons for the return statement
                if neuron['type'] == 'output':
                    if output_list == '':
                        output_list += "n_{}".format(neuron_idx)
                    else:
                        output_list += ", n_{}".format(neuron_idx)

            else:
                raise ValueError('Unknown neuron type {!r} for neuron {}!'.format(neuron['type'], neuron_idx))

        # create return statement
        if self.config.return_type == 'scalar':
            my_func_str += "output_value = {}".format(output_list)
        elif self.config.return_type == 'list':
            my_func_str += "output_value = [{}]".format(output_list)
        elif self.config.return_type == 'ndarray':
            my_func_str += "output_value = np.array([{}])".format(output_list)
        else:
            raise ValueError('Unknown return_type {!r}!'.format(self.config.return_type))

        self.code_str = my_func_str

        self.func_code_obj = compile(
            self.code_str,
            '',
            'exec')


    def __call__(self, input_value):
        loc = dict(input_value=input_value)
        exec(self.func_code_obj, globals(), loc)
        return loc['output_value']


class RandomRewardFunctionGenerator:
    """
    Generates random reward functions.

    Usage:
        generator = RandomRewardFunctionGenerator()
        reward_functions = generator.sample(10)


    Their are two types of configuration samplers:
        - value_sampler: Uses either the given value or if a function is specified, uses the function to sample
                         a value.
            Examples:
                        # every reward function has 3 input neurons:
                        n_in = 3

                        # sample for each reward function if it has 2, 3, or 4 inputs
                        n_in = lambda: np.random.choice([2,3,4])

        - function_sampler: Definition or sampling of a function (with potential parameters).
                            Samples the functions accroding the to given discrete probabilities.
                            Is a list with tuples. Each tuple holds: [0] the probability for the function,
                            [1] the name of the function, [2:] parameters for the function (which can also be
                            value_samplers).

            Examples:
                        # all neurons have no activation function
                        activation_functions = None

                        # neurons have 0.75 probability to have no activation function
                        # or 0.25 probability of aving an exponential activation function
                        activation_functions = [
                            (0.75, None),
                            (0.25, 'exp')]

                        # neurons have 0.75 probability to have no activation function
                        # or 0.25 probability of having a power activation function with a parameter b that is
                        # either 2, 3, or 4: a(x) = x^b
                        activation_functions = [
                            (0.75, None),
                            (0.25, 'power', lambda: np.random.choice[2,3,4])]

    Configuration:

        n_in: Number of inputs. (value_sampler)

        n_out: Number of outputs. (value_sampler)

        n_hidden_neurons: Number of hidden neurons. (value_sampler)

        n_hidden_layers: Number of hidden layers in which the neurons are oragnized.
                         Controls the complexity of the reward function.
                         (value_sampler)

        net_input_functions: Distribution over network input functions. Possible functions: 'sum', 'prod'.
                            (function_sampler)

        activation_functions: Distribution over activation functions. (function_sampler)
            Possible functions:
                - None: x
                - 'log': log(x)
                - ('log', a): log_a(x)
                - 'exp': exp(x)
                - ('exp', a): a^x
                - ('power', a): x^a
                - 'sqrt': sqrt(x)
                - 'sigm': 1/(1+e^(-x))
                - ('logistic', L, k, x0): L/(1+e^(-k(x - x0))  [https://en.wikipedia.org/wiki/Logistic_function]

        synapse_probability: Probability of a synapse between the current neuron and any neuron in a layer
                             below.

        weights: Synapse weight. (value_sampler)

        bias: Bias. (value_sampler)

        input_weights: List with weights (value_sampler) for synapses from input i to any other neuron.
                       i is the index in the list.
    """
    @staticmethod
    def default_config():
        dc = eu.AttrDict(

            n_in=3,

            n_out=1,

            n_hidden_neurons=lambda: sample_discrete_distribution([0.7, 0.2, 0.1]),

            n_hidden_layers=lambda: sample_discrete_distribution([0.5, 0.5]) + 1,

            net_input_functions=[
                (1.0, 'sum')],

            activation_functions=[
                (0.7, None),  # x
                (0.3, 'logistic', # L/(1+e^(-k(x - x0))
                      lambda: np.random.rand() * 2.5 + 0.5,     # L = U(0.5, 3)
                      lambda: np.random.rand() * 4.0 - 2.0,     # k = U(-2, 2)
                      lambda: np.random.rand() * 10.0 - 5.0),   # x0 = U(-5 , 5)
            ],

            synapse_probability = 0.8,

            weights = lambda: np.random.rand() * 2.0 - 1.0,     # U(-1.0, 1.0)

            bias = 0.0,

            input_weights = [
                lambda: np.random.rand(),           # U(0.0, 1.0)
                lambda: np.random.rand(),           # U(0.0, 1.0)
                lambda: np.random.rand() - 1.0,     # U(-1.0, 0.0)
            ]
        )
        return dc

    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

    def sample(self, n_networks=1):

        func_list = []
        for network_idx in range(n_networks):
            neurons = self._sample_feedforward_network()
            func_list.append(NumpyRandomRewardFunction(neurons=neurons))
        return func_list

    def _sample_feedforward_network(self):
        """
        Samples a feedforward network.
        Returns a list of neurons. Each neuron can have connections to previous neurons in the list but
        not to neurons in front of the list.

        Each neuron is defined by a dictionary with the following attributes:
            type: Neuron type which is either 'in', 'hidden', or 'out'.
            input_neurons: Numpy array with indexes of previous neurons the current neuron receives input from.
            weights: Numpy array with connection weights from input neurons.
            net_input_function: Network input function of the neuron.
            activation_function: Activation function of the neuron.
        """

        # number of input or output neurons
        n_in = self._sample_value(self.config.n_in)
        n_out = self._sample_value(self.config.n_out)

        # sample number of neurons
        n_hidden_neurons = self._sample_value(self.config.n_hidden_neurons)

        # sample number of layers
        # can only be of same size as number of neurons
        n_hidden_layers = self._sample_value(self.config.n_hidden_layers)
        n_hidden_layers = min(n_hidden_layers, n_hidden_neurons)

        # sample number of neurons per layer
        neuron_idx_per_layer = np.random.choice(n_hidden_layers, n_hidden_layers)
        n_neurons_per_layer = [np.sum(neuron_idx_per_layer == layer_idx) for layer_idx in range(n_hidden_layers)]

        # add input neurons to list of neurons
        neurons = [{'type': 'input', 'index': idx} for idx in range(n_in)]

        # create neurons layer by layer and sample which former neurons are connect to the new neuron
        for layer_idx in range(n_hidden_layers):

            n_neurons_in_previous_layers = len(neurons)

            for neuron_idx in range(n_neurons_per_layer[layer_idx]):
                input_neurons_idxs = self._sample_input_neurons(n_neurons_in_previous_layers, neurons)
                neurons.append({
                    'type': 'hidden',
                    'input_neurons': input_neurons_idxs,
                    'weights': self._sample_weights(input_neurons_idxs),
                    'bias': self._sample_value(self.config.bias)
                })

        # add the outputs
        n_neurons_in_previous_layers = len(neurons)
        for neuron_idx in range(n_out):
            input_neurons_idxs = self._sample_input_neurons(n_neurons_in_previous_layers, neurons)
            neurons.append({
                'type': 'output',
                'index': neuron_idx,
                'input_neurons': input_neurons_idxs,
                'weights': self._sample_weights(input_neurons_idxs),
                'bias': self._sample_value(self.config.bias)
            })

        # check if all hidden neurons are connected to another neuron, if not create a connection
        # only inputs and outputs do not need to be connected
        used_neurons = set()
        for neuron_idx in np.flip(np.arange(len(neurons))):
            neuron = neurons[neuron_idx]

            # add input neurons to set of used neurons
            if neuron['type'] != 'input':
                used_neurons.update(neuron['input_neurons'])

            if neuron['type'] == 'hidden':
                # check if neuron was used, if not, then use it as input for one of the neurons above it
                if neuron_idx not in used_neurons:
                    # draw random neuron to add the current as input
                    add_to_neuron_idx = np.random.choice(np.arange(neuron_idx + 1, len(neurons)))
                    neurons[add_to_neuron_idx]['input_neurons'].append(neuron_idx)
                    neurons[add_to_neuron_idx]['weights'].append(self._sample_weights(neuron_idx))
                    used_neurons.add(neuron_idx)

        # sample input and activation functions for all non-input neurons
        for neuron in neurons:
            if neuron['type'] != 'input':
                neuron['net_input_func'] = self._sample_function(self.config.net_input_functions)
                neuron['activation_func'] = self._sample_function(self.config.activation_functions)

        return neurons

    def _sample_input_neurons(self, n_neurons_in_previous_layers, neurons):

        is_synapse = (np.random.rand(n_neurons_in_previous_layers) <= self.config.synapse_probability)
        # at least one synapse must exists
        if not np.any(is_synapse):
            is_synapse[np.random.randint(len(is_synapse))] = True
        input_neurons_idxs = np.where(is_synapse)[0].tolist()
        return input_neurons_idxs

    def _sample_weights(self, input_neurons_idxs):

        is_single_input = False
        if not isinstance(input_neurons_idxs, list) and not isinstance(input_neurons_idxs, np.ndarray):
            input_neurons_idxs = [input_neurons_idxs]
            is_single_input = True

        weights = []
        for input_neurons_idx in input_neurons_idxs:

            if self.config.input_weights is not None and input_neurons_idx < len(self.config.input_weights):
                weight = self._sample_value(self.config.input_weights[input_neurons_idx])
            else:
                weight = self._sample_value(self.config.weights)

            weights.append(weight)

        if is_single_input:
            return weights[0]
        else:
            return weights

    @staticmethod
    def _sample_value(value_definition):
        sampled_value = value_definition
        if isinstance(value_definition, list) or isinstance(value_definition, np.ndarray):
            sampled_value = sample_discrete_distribution(value_definition)
        elif callable(value_definition):
            sampled_value = value_definition()
        return sampled_value

    @staticmethod
    def _sample_function(function_distribution):

        # handle situations where not a distribution is given, but a single function
        if function_distribution is None or isinstance(function_distribution, str) or isinstance(function_distribution,
                                                                                                 tuple):
            function_distribution = [(1.0, function_distribution)]

        # collect probabilities
        pr_distr = [f_descr[0] for f_descr in function_distribution]

        # check correctness
        if not np.isclose(np.sum(pr_distr), 1.0):
            raise ValueError('Probabilities over a function distribution must sum to 1!')

        sampled_func_idx = sample_discrete_distribution(pr_distr)
        function_descr = function_distribution[sampled_func_idx]

        func_name = function_descr[1]
        params = []

        # get potential parameters:
        if len(function_descr) > 2:
            params = []
            for param_idx, param_descr in enumerate(function_descr[2:]):
                # check if it is a parameter with a name or not
                if isinstance(param_descr, tuple) and isinstance(param_descr[0], str) and len(param_descr) == 2:
                    params.append((param_descr[0], RandomRewardFunctionGenerator._sample_value(param_descr[1])))
                else:
                    params.append(RandomRewardFunctionGenerator._sample_value(param_descr))

        return tuple([func_name] + params)


# generator = RandomRewardFunctionGenerator()
# r_funcs = generator.sample(100)
# log.add_single_object('r_funcs', r_funcs)
# print('ok')
# log.save(directory='./data/')
# import dill
# with open('./data/r_funcs.dill', 'rb') as fh:
#     obj = dill.load(fh)

# new_funcs = obj
# print(new_funcs[0])