import numpy as np
from gymnasium import ActionWrapper, ObservationWrapper, RewardWrapper, Wrapper

import gymnasium as gym
from gymnasium.spaces import Box, Discrete, Dict

def sum_two_tuples(a, b):
    return tuple(map(sum, zip(a, b)))

class MultiTaskWeight(ObservationWrapper):
    def __init__(self, env, feature_index = []):
        super().__init__(env)
        self.feature_index = feature_index
        if self.feature_index:
            self.num_weights += -len(self.feature_index)
        if isinstance(self.observation_space, Box):
            shape = sum_two_tuples(env.observation_space.shape, (self.num_weights,))
            low = np.concatenate((env.observation_space.low, np.zeros(self.num_weights)))
            high = np.concatenate((env.observation_space.high, np.ones(self.num_weights)))
            self.observation_space = Box(shape=shape, low=low, high=high)
        elif isinstance(env.observation_space, Dict):
            self.observation_space['weights'] = Box(shape=self.num_weights, low=0, high=1)
            

    def observation(self, observation):
        if self.feature_index:
            goal = [goal_cp for index, goal_cp in enumerate(self.goal) if index not in self.feature_index]
        else:
            goal = self.goal
        if isinstance(observation, dict):
            observation['weights'] = np.array(goal)
        else:
            observation = np.concatenate((observation, np.array(goal)))
        return observation   