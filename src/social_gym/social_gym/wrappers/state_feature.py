import numpy as np
from gymnasium import Wrapper

import gymnasium as gym
from gymnasium.spaces import Box, Discrete

def sum_two_tuples(a, b):
    return tuple(map(sum, zip(a, b)))

class StateFeatureWrapper(Wrapper):
    """Superclass of wrappers that can modify observations using :meth:`observation` for :meth:`reset` and :meth:`step`.

    If you would like to apply a function to only the observation before
    passing it to the learning code, you can simply inherit from :class:`ObservationWrapper` and overwrite the method
    :meth:`observation` to implement that transformation. The transformation defined in that method must be
    reflected by the :attr:`env` observation space. Otherwise, you need to specify the new observation space of the
    wrapper by setting :attr:`self.observation_space` in the :meth:`__init__` method of your wrapper.

    Among others, Gymnasium provides the observation wrapper :class:`TimeAwareObservation`, which adds information about the
    index of the timestep to the observation.
    """

    def __init__(self, env, *args):
        """Constructor for the observation wrapper."""
        super().__init__(env)
        self.env.num_features = env.observation_space.shape[0]

    def reset(self, seed = None, options = None):
        """Modifies the :attr:`env` after calling :meth:`reset`, returning a modified observation using :meth:`self.observation`."""
        obs, info = self.env.reset(seed=seed, options=options)
        return obs, info

    def step(self, action):
        """Modifies the :attr:`env` after calling :meth:`step` using :meth:`self.observation` on the returned observations."""
        observation, reward, terminated, truncated, info = self.env.step(action)
        return observation, reward, terminated, truncated, self.info(info, observation)

    def info(self, info, observation):
        """Returns a modified observation.

        Args:
            observation: The :attr:`env` observation

        Returns:
            The modified observation
        """
        info['features'] = observation
        return info
    
