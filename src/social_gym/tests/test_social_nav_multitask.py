import social_gym
import gymnasium as gym

if __name__ == '__main__':
    env = gym.make("MultiTaskSocialNavigation-v0")
    env.reset_task(0)
    state, _ = env.reset()
    action = env.action_space.sample()
    next_state, reward, terminated, truncated, _ = env.step(action)  