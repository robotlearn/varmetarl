import gymnasium as gym
import social_gym
import time
import mpi_sim
import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap

env = gym.make('SocialNavigation-v0', n_max_humans = 3)
fig, (ax1, ax2, ax3) = plt.subplots(1, 3, layout='constrained', sharey=True)
_, info = env.reset()
time.sleep(1)
print(info['human_map_global'])
im1 = ax1.imshow(np.rot90(info['visibility']), animated=True) 
im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
ax3.imshow(np.rot90(info['human_map_global']), animated=True)
ax3.set_xticks(np.arange(-0.5, 31., 1), labels=[])
ax3.set_yticks(np.arange(-0.5, 31., 1), labels=[])
ax3.grid()
ax1.xaxis.set_ticklabels([])
ax2.xaxis.set_ticklabels([])
step = 0
ims1 = [[im1]]
ims2 = [[im2]]
col =0
for _ in range(100):
    #env.render()
    obs, _, terminated, truncated, info = env.step(env.action_space.sample())
    step += 1
    time.sleep(.1)
    if info['features'][1] == -1:
        
        col += 1
    # print(env.agent.position, env.goal_position)
    # print(obs)
    #print(info['agent_map'].shape)
    im1 = ax1.imshow(np.rot90(info['visibility']), animated=True)
    ims1.append([im1])
    im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
    ims2.append([im2])
    # if terminated or truncated:
    #     break
# env.render()
print(col)
env.close()
ani = animation.ArtistAnimation(fig, ims1, interval=250, blit=True,
                            repeat_delay=1000, repeat = True)
ani2 = animation.ArtistAnimation(fig, ims2, interval=250, blit=True,
                            repeat_delay=1000, repeat = True)    
plt.show()
print(info['visibility'].shape)
print(info['human_map_global'].shape)
print(np.stack((info['robot_map_global'], info['human_map_global'], info['visibility'])).shape)
env.reset()
step = 0
for _ in range(40):
    #env.render()
    obs, _, terminated, truncated, *_ = env.step([1, .5])
    step += 1
    time.sleep(.1)
    # print(env.agent.position, env.goal_position)
    # print(obs)
    if terminated or truncated:
        break
print(step)
# print(env.agent.mapping.local_map)