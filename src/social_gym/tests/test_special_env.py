import social_gym 
import gymnasium as gym
import exputils as eu
import time
import mpi_sim
import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap

human_1 = mpi_sim.objects.Human(
    position=[.5, 0],
    orientation=-np.pi/2)

# human_2 = mpi_sim.objects.Human(
#     position=[.5, 0.0],
#     orientation=-np.pi/4)

# human_3 = mpi_sim.objects.Human(
#     position=[1.1, 0.0],
#     orientation=np.pi/4)



env = gym.make('ScenarioSimulation-v0', render_mode = 'human', config= eu.AttrDict(
            random_start_position = False,
            random_goal_position = False,
            start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                    start_position = [0., 0.],
                    start_orientation = 0,
                    goal_position = [0., 0.],
                    goal_orientation = 0.,
                ),
                
            humans = [human_1],
                        
            groups = []
    
   ))

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, layout='constrained', sharey=True)
_, info = env.reset()
time.sleep(1)
print(info['human_map_global'])
im1 = ax1.imshow(np.rot90(info['visibility']), animated=True) 
im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
ax3.imshow(np.rot90(info['human_map_global']), animated=True)
ax3.set_xticks(np.arange(-0.5, 31., 1), labels=[])
ax3.set_yticks(np.arange(-0.5, 31., 1), labels=[])
ax3.grid()
ax1.xaxis.set_ticklabels([])
ax2.xaxis.set_ticklabels([])
step = 0
ims1 = [[im1]]
ims2 = [[im2]]
col =0
human_1.navigation.goal_position = [-2., 0.]
# human_2.navigation.goal_position = [0., 0.]
# human_3.navigation.goal_position = [0., 0.]
print(mpi_sim.utils.measure_center_distance(env.agent, human_1))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_2))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_3))
time.sleep(1)
env.step([-1, -1])
print(mpi_sim.utils.measure_center_distance(env.agent, human_1))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_2))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_3))
time.sleep(1)
env.step([-1, -1])
print(mpi_sim.utils.measure_center_distance(env.agent, human_1))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_2))
# print(mpi_sim.utils.measure_center_distance(env.agent, human_3))
time.sleep(1)
for _ in range(100):
    obs, _, terminated, truncated, info = env.step([-1, -1])
    # print(env.agent.position, env.goal_position)
    # print(obs)
    #print(info['agent_map'].shape)
    im1 = ax1.imshow(np.rot90(info['visibility']), animated=True)
    ims1.append([im1])
    im2 = ax2.imshow(np.rot90(info['human_map_global']), animated=True)
    ims2.append([im2])
    # if terminated or truncated:
    #     break
# env.render()    
    time.sleep(.1)
ani = animation.ArtistAnimation(fig, ims1, interval=250, blit=True,
                            repeat_delay=1000, repeat = True)
ani2 = animation.ArtistAnimation(fig, ims2, interval=250, blit=True,
                            repeat_delay=1000, repeat = True)    
plt.show()
print(env.agent.orientation)
