import gymnasium as gym
import social_gym
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.cm import get_cmap
import mpi_sim.core.module_library as modlib
import uuid

np.random.seed(6)
env =  gym.make('SocialNavigation-v0', render_mode='human')
env.simulation.processes['gui'].start_recording()
obs, info = env.reset()
done = False
step = 0
while not done:
    action = env.action_space.sample()
    obs, _, terminated, truncated, info = env.step(action)
    done = terminated or truncated
    step += 1
dict_lst = env.render()
print(step)
env.simulation.processes['gui'].end_recording(path='video', video_id='test_1.mp4')
env.close()
print(len(dict_lst))
trajectories = {}

for d in dict_lst:
    for key, value in d.items():
        if key in trajectories:
            trajectories[key] = np.vstack((trajectories[key], np.array(d[key])))
        else:
            trajectories[key] = np.array(d[key])

# print(trajectories)

def plot(trajectories, goal_position, save_plots='', trajectory_grid_show=False):
    
    fig, ax = plt.subplots()
    
    for human in [key for key in trajectories if key != 'robot']:
        trajectories[human] = trajectories[human].reshape(-1, 2)
        x = trajectories[human][:, 0]
        y = trajectories[human][:, 1]
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        norm = plt.Normalize(0, len(x))
        lc = LineCollection(segments, cmap='Greys', norm=norm, label = 'human_'+str(human))
        # Set the values used for colormapping
        lc.set_array(list(range(len(x))))
        lc.set_linewidth(3)
        line = ax.add_collection(lc)
        
    trajectories['robot'] = trajectories['robot'].reshape(-1, 2)
    x = trajectories['robot'][:, 0]
    y = trajectories['robot'][:, 1]
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    
    norm = plt.Normalize(0, len(x))
    lc = LineCollection(segments, cmap='viridis', norm=norm, label = 'robot')
    # Set the values used for colormapping
    lc.set_array(list(range(len(x))))
    lc.set_linewidth(4)
    line = ax.add_collection(lc)
            
    ax.set_yticks([])
    ax.set_xticks([])
    
    start = trajectories['robot'][0,:]
    ax.scatter(start[0], start[1], marker="^",color="black", label="start positions")
    for human in [key for key in trajectories if key != 'robot']:
        start = trajectories[human][0,:]
        ax.scatter(start[0], start[1], marker="^",color="black")

    # Plot the goal point
    goal = goal_position
    ax.scatter(goal[0], goal[1], color="red", label="robot's goal position") 
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)

    ax.legend(loc='upper right',bbox_to_anchor=(1, 0),fancybox=True, shadow=True, ncol=3)
    if save_plots :
        fig.savefig(save_plots)
    if trajectory_grid_show:
        plt.show()
       
plot(trajectories, info['goal_position'], save_plots=str(uuid.uuid4()))
