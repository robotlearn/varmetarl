import numpy as np
import random
import exputils as eu
import social_gym
import gymnasium as gym
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation

np.random.seed(0)
random.seed(0)    

if __name__ == "__main__":
    config = eu.AttrDict(
            start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
                    start_orientation = -np.pi/4,
                    start_position = [-2.8, -2.8],
                    goal_position = [2.8, 2.8],
                    goal_orientation = 0.,
                ),
            random_start_position = False,
            random_goal_position = False,
            humans_start_region = [[2.8, 2.8], [2.8, 2.8]], # [[x_min, x_max], [y_min, y_max]]
            humans_goal_region = [[-2.8, -2.8], [-2.8, -2.8]], # [[x_min, x_max], [y_min, y_max]]
            human_goal_position_radius = 0.05,       
            
            # Parameters about humans
            n_max_humans = 3, # used when random or not
            )
    fig, ax = plt.subplots()
    env = gym.make('MinimalSocialNavigation-v0', config = config)
    step = 0
    _, info = env.reset()
    for _ in range(100):
        obs, reward, terminated, truncated, info = env.step([1, 0])
        print(reward, info['features'])
        step += 1
    # array_list = env.render()
    # ims = []
    # for i in range(0, len(array_list)):
    #     im = ax.imshow(array_list[i], animated = True)
    #     if  i == 0:
    #         ax.imshow(array_list[i])
    #     ims.append([im])
    # ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True,
    #                             repeat_delay=1000)
    # plt.show()
    env.close()
