import numpy as np
import random
import exputils as eu
import social_gym
import gymnasium as gym
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import mpi_sim as sim
from social_gym.envs.robotics.minimal_mpi_env import visible, coming

np.random.seed(0)
random.seed(0)    

if __name__ == "__main__":
    
    human_1 = sim.objects.Human(
        position=[-2., 0.],
        orientation=-np.pi/2)
    
    human_2 = sim.objects.Human(
        position=[0.0, 0.],
        orientation=-np.pi/2)
    
    human_3 = sim.objects.Human(
        position=[-1.4, 0.],
        orientation=np.pi/2)
    
    config = eu.AttrDict(
            start_goal_infos = eu.AttrDict(
                    start_orientation = -np.pi/2,
                    start_position = [-.5, -np.sqrt(3)/2],
                    goal_position = [2.5, 2.5],
                    goal_orientation = 0.,
                ),
            
            humans = [human_1, human_2, human_3],
                      
            groups = []
            
            )
    fig, ax = plt.subplots()
    env = gym.make('ScenarioSimulation-v0', config = config, render_mode = "human")
    print(visible(env.agent, human_2))
    print(coming(env.agent, human_2))
    robot_to_human = np.abs(sim.utils.measure_relative_angle(env.agent, human_2))
    human_to_robot = np.abs(sim.utils.measure_relative_angle(human_2, env.agent))
    print(human_to_robot, robot_to_human)
    step = 0
    _, info = env.reset()
    for _ in range(100):
        obs, _, terminated, truncated, info = env.step([-1, 0])
        step += 1
        time.sleep(.1)
    array_list = env.render()
    ims = []
    for i in range(0, len(array_list)):
        im = ax.imshow(array_list[i], animated = True)
        if  i == 0:
            ax.imshow(array_list[i])
        ims.append([im])
    ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True,
                                repeat_delay=1000)
    plt.show()
    env.close()