# np.random.seed(0)
# random.seed(0)        
# if __name__ == "__main__":
#     config = eu.AttrDict(
#             random_start_position = False,
#             random_goal_position = True,
#             start_goal_infos = eu.AttrDict(  # Used only if random_start_position and random_goal_position are zeros
#                     start_orientation = 0,
#                     start_position = [0, -1.5],
#                     goal_position = [1.5, 1.5],
#                     goal_orientation = 0.,
#                 ))
#     env = MpiEnv(config = config, render_mode='dict')
#     fig, (ax1, ax2, ax3) = plt.subplots(1, 3, layout='constrained', sharey=True)
#     _, info = env.reset()
#     env.step([-1, 1])
#     im1 = ax1.imshow(info['robot_map_global'], animated=True) 
#     im2 = ax2.imshow(info['human_map_global'], animated=True)
#     ax3.imshow(info['human_map_global'], animated=True)
#     ax3.set_xticks(np.arange(-0.5, 20., 1))
#     ax3.set_yticks(np.arange(-0.5, 20., 1))
#     ax3.grid()
#     step = 0
#     ims1 = [[im1]]
#     ims2 = [[im2]]
#     for _ in range(40):
#         #env.render()
#         obs, _, terminated, truncated, info = env.step([-1, 1])
#         step += 1
#         time.sleep(10)
#         # print(env.agent.position, env.goal_position)
#         # print(obs)
#         #print(info['agent_map'].shape)
#         im1 = ax1.imshow(info['robot_map_global'], animated=True)
#         ims1.append([im1])
#         im2 = ax2.imshow(info['human_map_global'], animated=True)
#         ims2.append([im2])
#         # if terminated or truncated:
#         #     break
#     # env.render()
#     env.close()
#     ani = animation.ArtistAnimation(fig, ims1, interval=250, blit=True,
#                                 repeat_delay=1000, repeat = True)
#     ani2 = animation.ArtistAnimation(fig, ims2, interval=250, blit=True,
#                                 repeat_delay=1000, repeat = True)    
#     plt.show()