"""
soft actor critic training algorithm
"""
import copy
import logging
import numpy as np
import torch
import torch.nn as nn
from varmetarl.core import MetaRlAlgorithm
from varmetarl.networks import QNetworkMulti, ValueNetworkMulti
from varmetarl.policy import TanhPolicyMultiTask
from varmetarl.utils import generate_optimizers
import exputils as eu
logger = logging.getLogger('pearl')


class MultiTaskSac(MetaRlAlgorithm):
    """
    pearl algorithm.

    Implemented as defined in the work of Rakelly and al, 2019
    """
    
    @staticmethod
    def default_config():
        parent_config = MetaRlAlgorithm.default_config()
        default_config = eu.AttrDict(
            seed = 0,
            q_network=eu.AttrDict(
                cls = QNetworkMulti,
                config = eu.AttrDict()
            ),
            q_learning_rate=3e-4,
            policy = eu.AttrDict(
                cls = TanhPolicyMultiTask,
                config = eu.AttrDict()
            ),
            policy_learning_rate=3e-4,
            v_network=eu.AttrDict(
                cls = ValueNetworkMulti,
                config = eu.AttrDict()
            ),
            v_learning_rate=3e-4,
            latent_dim = 3,
            kl_lambda = 1,
            alpha = 5,
            tau=0.005,
            gamma=0.99,
        )
        default_config = eu.combine_dicts(default_config, parent_config)        
        return default_config 

    def __init__(self, env, train_tasks, test_tasks, config = None, **kwargs):
        super().__init__(env, train_tasks, test_tasks, config=config, **kwargs)
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        
        q1 = eu.misc.create_object_from_config(self.config.q_network, self.obs_dim, self.action_dim, self.config.latent_dim).to(self.device)
        q2 = eu.misc.create_object_from_config(self.config.q_network, self.obs_dim, self.action_dim, self.config.latent_dim).to(self.device)
        v_network = eu.misc.create_object_from_config(self.config.v_network, self.obs_dim, self.config.latent_dim).to(self.device)
        
        self.networks.update({"q1": q1, "q2": q2, "v_network": v_network})
        self.target_v_network = copy.deepcopy(self.v_network)
        learning_rate = {"policy": {"lr": self.config.policy_learning_rate},
                         "q1": {"lr": self.config.q_learning_rate},
                         "q2": {"lr": self.config.q_learning_rate},
                         "v_network": {"lr": self.config.v_learning_rate},
                         }
        self.optimizers = generate_optimizers(
            self.networks, learning_rate, torch.optim.Adam)
        
        logger.info("Initialisation finished, kl = %f, alpha = %f, gamma = %f, policy is %s",
            self.config.kl_lambda,
            self.config.alpha,
            self.config.gamma,
            self.policy.__class__.__name__)

    @property
    def q1(self):
        return self.networks["q1"]

    @property
    def q2(self):
        return self.networks["q2"]

    @property
    def v_network(self):
        return self.networks["v_network"]

    def _init_training(self):
        self.runner.initialize_replay_buffer(
            self.networks, self.meta_replay_buffer)

    def _training_step(self, i, j, list_idx, n_adaptation_steps):
        # obs, action, reward, next_obs = self.meta_replay_buffer.sample(
        obs, action, reward, next_obs = self.meta_replay_buffer.sample(
            list_idx, self.config.batch_size)

        self.optimizers['q1'].zero_grad()
        self.optimizers['q2'].zero_grad()
        x1, x2, y, q_loss = self.q_loss(
            obs, action, reward, next_obs)
        self.writer.log_q_function(i, j, q_loss, x1=x1, x2=x2, y=y)
        q_loss.backward()
        self.optimizers['q1'].step()
        self.optimizers['q2'].step()
        
        self.optimizers['v_network'].zero_grad()
        x, y, z, value_loss = self.value_loss(obs)
        self.writer.log_value_function(
            i, j, value_loss, x=x, y=y, mse=z)
        value_loss.backward()
        self.optimizers['v_network'].step()
        
        self.optimizers['policy'].zero_grad()
        l_prob, y, policy_loss = self.policy_loss(obs)
        self.writer.log_policy(
            i, j, policy_loss, l_prob=l_prob, y=y)
        policy_loss.backward()
        self.optimizers['policy'].step()
        
        self.update_target_v_network()

    def compute_metrics(self):
        return {"nothing": 0}

    def policy_loss(self, obs):
        action, _, log_prob = self.log_policy(
            {'obs': obs}, reparametrize=True)
        y = self.min_q({'obs': obs, 'action': action,})
        loss = self.config.alpha * log_prob - y
        return log_prob.mean(), y.mean(), loss.mean()

    def value_loss(self, obs):
        loss_fn = nn.MSELoss()
        x = self.v_network({'obs': obs})
        with torch.no_grad():
            action, _, log_prob = self.log_policy(
                {'obs': obs})
            y = self.min_q({'obs': obs, 
                            'action': action}) - self.config.alpha * log_prob
        z = torch.square(x - y)
        return x.mean(), y.mean(), z.mean(), loss_fn(x, y)

    def q_loss(self, obs, action, reward, next_obs):
        loss_fn = nn.MSELoss()
        x1 = self.q1({'obs': obs, 'action': action})
        x2 = self.q2({'obs': obs, 'action': action})
        with torch.no_grad():
            target_value = self.target_v_network({'obs': next_obs})
        y = reward + self.config.gamma * target_value
        loss = 1 / 2 * (loss_fn(x1, y) + loss_fn(x2, y))
        return x1.mean(), x2.mean(), y.mean(), loss

    def log_policy(self, tensor_dict, reparametrize=False):
        return self.policy.log(tensor_dict, reparametrize)

    def min_q(self, tensor_dict):
        return torch.min(self.q1(tensor_dict), self.q2(tensor_dict))

    def update_target_v_network(self):
        for target_param, value_param in zip(self.target_v_network.parameters(),
                                             self.v_network.parameters()):
            target_param.data.copy_(target_param * (1.0 - self.config.tau)
                                    + value_param * self.config.tau)
