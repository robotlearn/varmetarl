"""Run policy in environment (policy agnostic)."""
# import copy
import random
import numpy as np
from varmetarl.replay_buffer import ReplayBuffer
# from multiprocessing import Pool, Process
# from torch import multiprocessing as mp
import exputils as eu
import torch

class Runner:
    """Runner class to collect samples and do testing.

    Parameters
    ----------
    env : gym.Env
        gym env used during the training and testing.
    train_env: list
        index of environments used for training
    test_env: list
        index of environments used for testing
    train_step : int
        Number of training step after adaptation for each iteration
    context_training_step : int
        Number of training step before adaptation for each iteration
    n_adaptation_steps : int
        Number of adapation steps at test time
    test_ep : int
        Number of tests episodes.
    device : str
        either cuda or cpu.
    """


    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            train_step = 600,
            context_training_step = 400,
            n_adaptation_steps = 200,
            test_ep = 5
        )
        return default_config
    
    def __init__(self, env, train_env, test_env, config = None, **kwargs):
        """Short summary."""
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.env = env
        self.train_env = train_env
        self.test_env = test_env

    def _run_for_n_step(self, n_step, policy, meta_replay_buffer, task_z=None):
        """Run the policy or a random sampling policy during n steps.

        Parameters
        ----------
        n_step : int
            number of running steps.
        meta_replay_buffer : MetaReplayBuffer
            replay buffer to store sample.
        policy : Policy
            policy followed by the agent.
        task_z : torch.Tensor
            If not given, action are sampled randomly and not according
            to the policy.

        Returns
        -------
        int
            The number of step during which the policy was run.
        """
        step = 0
        state, _ = self.env.reset()
        done = False
        total_reward = 0
        for _ in range(int(n_step)):
            if done:
                state, _ = self.env.reset()
            if task_z is not None:
                action = policy.select_action(state, task_z)
            else:
                action = self.env.action_space.sample()
            next_state, reward, terminated, truncated, _ = self.env.step(action)
            done = terminated or truncated
            total_reward += reward
            meta_replay_buffer.add(state, action, reward, next_state)
            state = next_state
            step += 1
        return step

    def initialize_replay_buffer(self, networks, meta_replay_buffer):
        """Use to initialize the replay buffer.

        Parameters
        ----------
        policy : Policy
            policy followed by the agent.
        encoder : Encoder
            output a latent task variable to infer how to solve a new task from
            small amounts of experience.
        meta_replay_buffer : MetaReplayBuffer
            replay buffer to store sample.

        Returns
        -------
        int
            The number of step during which the policy was run.
        """
        step = 0
        meta_replay_buffer.collect_encoder_data(True)
        for idx in self.train_env:
            #print(idx)
            self.env.reset_task(idx)
            meta_replay_buffer.set_idx(idx)
            step += self._run_for_n_step(meta_replay_buffer.get_init_step(),
                                        networks["policy"], meta_replay_buffer)
        meta_replay_buffer.collect_encoder_data(False)
        return step

    def _gather_data(self, policy, encoder, meta_replay_buffer):
        """Use to gather data without any knowledge on the task for n steps.

        Parameters
        ----------
        policy : Policy
            policy followed by the agent.
        encoder : Encoder
            output a latent task variable to infer how to solve a new task from
            small amounts of experience.
        meta_replay_buffer : MetaReplayBuffer
            replay buffer to store sample.

        Returns
        -------
        int
            The number of step during which the policy was run.
        """
        step = 0
        meta_replay_buffer.collect_encoder_data(True)
        task_z, _, _ = encoder.sample_from_prior()
        step += self._run_for_n_step(self.config.context_training_step,
                                    policy, meta_replay_buffer, task_z=task_z)
        meta_replay_buffer.collect_encoder_data(False)
        return step

    def _adapt(self, context, policy, encoder, meta_replay_buffer):
        """Use to gather data after sampling a task variable from the transitions stored in the replay buffer.

        Parameters
        ----------
        policy : Policy
            policy followed by the agent.
        encoder : Encoder
            output a latent task variable to infer how to solve a new task from
            small amounts of experience.
        meta_replay_buffer : MetaReplayBuffer
            replay buffer to store sample.

        Returns
        -------
        int
            The number of step during which the policy was run.
        """
        step = 0
        obs, action, reward, next_obs = context
        task_z, _, _ = encoder.compute_posterior(
            obs, action, reward, next_obs)
        step += self._run_for_n_step(self.config.train_step,
                                    policy, meta_replay_buffer, task_z=task_z)
        return step

    def run(self, num_tasks_sample, networks, meta_replay_buffer):
        policy = networks['policy']
        encoder = networks['encoder']
        # print(env_step, num_episodes)
        step = 0
        for _ in range(num_tasks_sample):
            idx = random.choice(self.train_env)
            self.env.reset_task(idx)  # TODO change this -> why ?
            # clear the data of this idx of the encoder replay buffer
            meta_replay_buffer.set_idx(idx)
            step += self._gather_data(policy, encoder, meta_replay_buffer)
            context = meta_replay_buffer.sample_context(
                idx, self.config.n_adaptation_steps)
            step += self._adapt(context, policy, encoder, meta_replay_buffer)
        return step

    def test(self, networks):
        policy = networks['policy']
        encoder = networks['encoder']
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(self.config.test_ep):
                context = ReplayBuffer(
                    self.config.n_adaptation_steps, 0, device=self.device)
                task_z, _, _ = encoder.sample_from_prior()
                self._run_for_n_step(self.config.n_adaptation_steps, policy,
                                    context, task_z=task_z)
                obs, action, reward, next_obs = context.sample(
                    self.config.n_adaptation_steps)
                task_z, _, _ = encoder.compute_posterior(
                    obs, action, reward, next_obs)
                state, _ = self.env.reset()
                done = False
                step = 0
                sum_reward = 0
                while not done:
                    action = policy.select_action(state, task_z, evaluate=True)
                    next_state, reward, terminated, truncated, _ = self.env.step(action)
                    sum_reward += reward
                    state = next_state
                    step += 1
                    done = terminated or truncated
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    def pearl_test(self, networks, n_iterations, max_steps=200, n_sampling=2):
        policy = networks['policy']
        encoder = networks['encoder']
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(n_iterations):
                context = ReplayBuffer(
                    max_steps*n_sampling, 0, device=self.device)
                sum_reward = 0
                task_z, _, _ = encoder.sample_from_prior()
                for sampling in range(n_sampling+1):
                    step = 0
                    state, _ = self.env.reset()
                    done = False
                    total_reward = 0
                    for _ in range(int(max_steps)):
                        if done:
                            state, _ = self.env.reset()
                        action = policy.select_action(
                            state, task_z, evaluate=True)
                        next_state, reward, terminated, truncated, _ = self.env.step(action)
                        done = terminated or truncated
                        context.add(state, action, reward, next_state)
                        total_reward += reward
                        state = next_state
                        step += 1
                    obs, action, reward, next_obs = context.sample(
                        max_steps*(sampling+1))
                    task_z, _, _ = encoder.compute_posterior(
                        obs, action, reward, next_obs)
                    sum_reward += total_reward
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    def render(self, policy, encoder, idx, max_step, n_adaptation_steps=0):
        self.env.reset_task(idx)
        task_z, _, _ = encoder.sample_from_prior()
        if n_adaptation_steps:
            context = ReplayBuffer(n_adaptation_steps, 0, device='cuda')
            self._run_for_n_step(n_adaptation_steps, policy,
                                context, task_z=task_z)
            obs, action, reward, next_obs = context.sample(
                n_adaptation_steps)
            task_z, _, _ = encoder.compute_posterior(
                obs, action, reward, next_obs)
        state, _ = self.env.reset()
        self.env.render()
        done = False
        step = 0
        total_reward = 0
        while (not done and step < max_step):
            # if done:
            #     state = self.env.reset()
            action = policy.select_action(state, task_z, evaluate=True)
            next_state, reward, terminated, truncated, _ = self.env.step(action)
            done = terminated or truncated
            self.env.render()
            step += 1
            state = next_state
            total_reward += reward
        self.env.close()
        print(total_reward)

    def run_random(self):
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(self.config.test_ep):
                self.env.reset()
                done = False
                step = 0
                sum_reward = 0
                while not done:
                    action = self.env.action_space.sample()
                    _, reward, terminated, truncated, _ = self.env.step(action)
                    sum_reward += reward
                    step += 1
                    done = terminated or truncated
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    # def test_parallel(self, policy, encoder, n_iterations, max_steps=1024):
    #     list_task_reward = {}
    #
    #     # print('AVANT SPAWN')
    #     #
    #     # mp.spawn(unit_test,
    #     #          args=(), nprocs=n_iterations)
    #     #
    #     # print('APRES SPAWN')
    #
    #     # return
    #
    #     # mp.set_start_method('spawn')
    #     for idx in self.test_env:
    #         list_reward = []
    #         self.env.reset_task(idx)
    #         # with Pool(n_iterations) as p:
    #         #     list_reward = p.map(self.unit_test, [(copy.deepcopy(policy), copy.deepcopy(encoder), max_steps),
    #         #                                          (copy.deepcopy(policy), copy.deepcopy(
    #         #                                              encoder), max_steps),
    #         #                                          (copy.deepcopy(policy), copy.deepcopy(
    #         #                                              encoder), max_steps),
    #         #                                          (copy.deepcopy(policy), copy.deepcopy(
    #         #                                              encoder), max_steps),
    #         #                                          (copy.deepcopy(policy), copy.deepcopy(encoder), max_steps)])
    #         #args = [() for _ in range(n_iterations)]
    #         # mp.spawn(self.unit_test,
    #         mp.spawn(self.toto,
    #                  args=(policy, encoder, max_steps), nprocs=n_iterations)
    #         # processes = []
    #         # for _ in range(n_iterations):
    #         #     p = mp.Process(target=self.unit_test,
    #         #                    args=(policy, encoder, max_steps))
    #         #     p.start()
    #         #     processes.append(p)
    #         # list_reward = []
    #         # for p in processes:
    #         #     p.join()
    #         # list_reward.append(reward)
    #         list_task_reward[idx] = {'mean': np.mean(
    #             list_reward), 'std': np.std(list_reward)}
    #     return list_task_reward
    #
    # def toto(self, i, *args):
    #     print('job n°', i)

        # def unit_test(self, policy, encoder, max_steps):
        #     context = ReplayBuffer(
        #         self.n_adaptation_steps, 0, device=self.device)
        #     task_z, _, _ = encoder.sample_from_prior()
        #     self.run_for_n_step(self.n_adaptation_steps, policy,
        #                         context, task_z=task_z)
        #     obs, action, reward, next_obs = context.sample(
        #         self.n_adaptation_steps)
        #     task_z, _, _ = encoder.compute_posterior(
        #         obs, action, reward, next_obs)
        #     state = self.env.reset()
        #     done = False
        #     step = 0
        #     sum_reward = 0
        #     while not done:
        #         action = policy.select_action(state, task_z, evaluate=True)
        #         next_state, reward, done, _ = self.env.step(action)
        #         sum_reward += reward
        #         state = next_state
        #         step += 1
        #         if step > max_steps:
        #             break
        #     return step, sum_reward
