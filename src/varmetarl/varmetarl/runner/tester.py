"""
Run policy in environment (policy agnostic)
"""
import copy
import numpy as np
import random
from varmetarl.replay_buffer import ReplayBuffer
# from multiprocessing import Pool, Process
from torch import multiprocessing as mp


class Tester:
    """
    Soft actor critic algorithm,
    implemented as defined in the work of Haarnoja and al, 2018
    """

    def __init__(self, env, train_env, test_env, train_step, context_training_step, n_adaptation_steps, test_ep, device='cpu'):
        self.env = env
        self.test_ep = test_ep
        self.train_step = train_step
        self.train_env = train_env
        self.test_env = test_env
        self.context_training_step = context_training_step
        self.n_adaptation_steps = n_adaptation_steps
        self.device = device

    def run_for_n_step(self, n_step, policy, meta_replay_buffer, task_z=None):
        step = 0
        state = self.env.reset()
        done = False
        total_reward = 0
        for _ in range(int(n_step)):
            if done:
                state = self.env.reset()
            if task_z is not None:
                action = policy.select_action(state, task_z)
            else:
                action = self.env.action_space.sample()
            next_state, reward, done, _ = self.env.step(action)
            total_reward += reward
            meta_replay_buffer.add(state, action, reward, next_state)
            state = next_state
            step += 1
        return step

    def test(self, policy, encoder, n_iterations, max_steps=20000):
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(n_iterations):
                context = ReplayBuffer(
                    self.n_adaptation_steps, 0, device=self.device)
                task_z, _, _ = encoder.sample_from_prior()
                self.run_for_n_step(self.n_adaptation_steps, policy,
                                    context, task_z=task_z)
                obs, action, reward, next_obs = context.sample(
                    self.n_adaptation_steps)
                task_z, _, _ = encoder.compute_posterior(
                    obs, action, reward, next_obs)
                state = self.env.reset()
                done = False
                step = 0
                sum_reward = 0
                while not done:
                    action = policy.select_action(state, task_z, evaluate=True)
                    next_state, reward, done, _ = self.env.step(action)
                    sum_reward += reward
                    state = next_state
                    step += 1
                    if step > max_steps:
                        break
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    def pearl_test(self, policy, encoder, n_iterations, max_steps=200, n_sampling=2):
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(n_iterations):
                context = ReplayBuffer(
                    max_steps*n_sampling, 0, device=self.device)
                sum_reward = 0
                task_z, _, _ = encoder.sample_from_prior()
                for sampling in range(n_sampling+1):
                    step = 0
                    state = self.env.reset()
                    done = False
                    total_reward = 0
                    for _ in range(int(max_steps)):
                        if done:
                            state = self.env.reset()
                        action = policy.select_action(
                            state, task_z, evaluate=True)
                        next_state, reward, done, _ = self.env.step(action)
                        context.add(state, action, reward, next_state)
                        total_reward += reward
                        state = next_state
                        step += 1
                    obs, action, reward, next_obs = context.sample(
                        max_steps*(sampling+1))
                    task_z, _, _ = encoder.compute_posterior(
                        obs, action, reward, next_obs)
                    sum_reward += total_reward
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    def render(self, policy, encoder, idx, max_step, n_adaptation_steps=0):
        self.env.reset_task(idx)
        task_z, _, _ = encoder.sample_from_prior()
        if n_adaptation_steps:
            context = ReplayBuffer(n_adaptation_steps, 0, device='cuda')
            self.run_for_n_step(n_adaptation_steps, policy,
                                context, task_z=task_z)
            obs, action, reward, next_obs = context.sample(
                n_adaptation_steps)
            task_z, _, _ = encoder.compute_posterior(
                obs, action, reward, next_obs)
        state = self.env.reset()
        self.env.render()
        done = False
        step = 0
        total_reward = 0
        while (not done and step < max_step):
            # if done:
            #     state = self.env.reset()
            action = policy.select_action(state, task_z, evaluate=True)
            next_state, reward, done, _ = self.env.step(action)
            self.env.render()
            step += 1
            state = next_state
            total_reward += reward
        self.env.close()
        print(total_reward)

    def test_idx(self, policy, encoder, idx, max_step, n_adaptation_steps=0):
        self.env.reset_task(idx)
        task_z, _, _ = encoder.sample_from_prior()
        if n_adaptation_steps:
            context = ReplayBuffer(n_adaptation_steps, 0, device='cuda')
            self.run_for_n_step(n_adaptation_steps, policy,
                                context, task_z=task_z)
            obs, action, reward, next_obs = context.sample(
                n_adaptation_steps)
            task_z, _, _ = encoder.compute_posterior(
                obs, action, reward, next_obs)
        state = self.env.reset()
        done = False
        step = 0
        total_reward = 0
        while (not done and step < max_step):
            # if done:
            #     state = self.env.reset()
            action = policy.select_action(state, task_z, evaluate=True)
            next_state, reward, done, _ = self.env.step(action)
            step += 1
            state = next_state
            total_reward += reward
        print(total_reward)

    def run_random(self, n_iterations, max_steps=1000):
        list_task_reward = {}
        for idx in self.test_env:
            list_reward = []
            self.env.reset_task(idx)
            for _ in range(n_iterations):
                self.env.reset()
                done = False
                step = 0
                sum_reward = 0
                while not done:
                    action = self.env.action_space.sample()
                    _, reward, done, _ = self.env.step(action)
                    sum_reward += reward
                    step += 1
                    if step > max_steps:
                        break
                list_reward.append(sum_reward)
            list_task_reward[idx] = {'mean': np.mean(
                list_reward), 'std': np.std(list_reward)}
        return list_task_reward

    def plot_encoder_representation(self, policy, encoder, num_env, max_steps=20000):
        list_task_reward = {}
        for idx in range(num_env):
            list_mean_z = []
            list_var_z = []
            self.env.reset_task(idx)
            context = ReplayBuffer(
                self.n_adaptation_steps, 0, device=self.device)
            task_z, _, _ = encoder.sample_from_prior()
            self.run_for_n_step(self.n_adaptation_steps, policy,
                                context, task_z=task_z)
            obs, action, reward, next_obs = context.sample(
                self.n_adaptation_steps)
            task_z, mean_z, var_z = encoder.compute_posterior(
                obs, action, reward, next_obs)
            list_mean_z.append(mean_z)
            list_var_z.append(var_z)
        return list_mean_z, list_var_z
