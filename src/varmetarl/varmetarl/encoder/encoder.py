import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal
import exputils as eu
LOG_SIG_MAX = 2
LOG_SIG_MIN = -20

# Initialize Policy weights


def weights_init_(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(
            m.weight, gain=nn.init.calculate_gain('relu'))
        torch.nn.init.constant_(m.bias, 0)


def _product_of_gaussians(mus, sigmas_squared):
    '''
    compute mu, sigma of product of gaussians
    '''
    sigmas_squared = torch.clamp(sigmas_squared, min=1e-7)
    sigma_squared = 1. / torch.sum(torch.reciprocal(sigmas_squared), dim=0)
    mu = sigma_squared * torch.sum(mus / sigmas_squared, dim=0)
    return mu, sigma_squared


class Encoder(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            gaussian_map = None
        )
        return default_config          
    
    
    def __init__(self, num_inputs, num_actions, num_rewards, latent_dim, config = None, **kwargs):
        super().__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.latent_dim = latent_dim
        self.linear1 = nn.Linear(
            num_inputs+num_actions+num_rewards, self.config.hidden_dim)
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.mean_linear = nn.Linear(self.config.hidden_dim, latent_dim)
        self.log_std_linear = nn.Linear(self.config.hidden_dim, latent_dim)

        self.device_indicator = nn.Parameter(torch.empty(0))

        self.apply(weights_init_)

    def forward(self, tensor_dict):
        state = tensor_dict['obs']
        action = tensor_dict['action']
        reward = tensor_dict['reward']
        xu = torch.cat([state, action, reward], 1)
        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        mean = self.mean_linear(x)
        log_std = self.log_std_linear(x)
        log_std = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        std = torch.exp(log_std)
        z_means, z_vars = _product_of_gaussians(mean, std)
        posteriors = [torch.distributions.Normal(m, torch.sqrt(s)) for m, s in zip(
            torch.unbind(z_means), torch.unbind(z_vars))]
        z = [d.rsample() for d in posteriors]
        z = torch.stack(z)
        return z, z_means, z_vars

    @torch.no_grad()
    def compute_posterior(self, state, action, reward, next_state):     
        return self({'obs': state, 'action': action, 'reward': reward})

    @torch.no_grad()
    def sample_from_prior(self):
        task_z = torch.distributions.Normal(
            0, 1).rsample(sample_shape=(self.latent_dim,))
        return task_z.to(self.device_indicator.device), torch.Tensor([0]).to(self.device_indicator.device), torch.Tensor([1]).to(self.device_indicator.device)

    @torch.no_grad()
    def generate_prior(self):
        prior = torch.distributions.Normal(torch.zeros(self.latent_dim, device=self.device_indicator.device),
                                           torch.ones(self.latent_dim, device=self.device_indicator.device))
        return prior

    def register_gradients(self):
        dict_gradients = {}
        dict_gradients['linear1'] = {'mean': self.linear1.weight.grad.mean().cpu().numpy(),
                                     'std': self.linear1.weight.grad.std().cpu().numpy(),
                                     'max': self.linear1.weight.grad.max().cpu().numpy(),
                                     'min': self.linear1.weight.grad.min().cpu().numpy()}
        dict_gradients['linear2'] = {'mean': self.linear2.weight.grad.mean().cpu().numpy(),
                                     'std': self.linear2.weight.grad.std().cpu().numpy(),
                                     'max': self.linear2.weight.grad.max().cpu().numpy(),
                                     'min': self.linear2.weight.grad.min().cpu().numpy()}
        dict_gradients['mean_linear'] = {'mean': self.mean_linear.weight.grad.mean().cpu().numpy(),
                                         'std': self.mean_linear.weight.grad.std().cpu().numpy(),
                                         'max': self.mean_linear.weight.grad.max().cpu().numpy(),
                                         'min': self.mean_linear.weight.grad.min().cpu().numpy()}
        dict_gradients['log_std_linear'] = {'mean': self.log_std_linear.weight.grad.mean().cpu().numpy(),
                                            'std': self.log_std_linear.weight.grad.std().cpu().numpy(),
                                            'max': self.log_std_linear.weight.grad.max().cpu().numpy(),
                                            'min': self.log_std_linear.weight.grad.min().cpu().numpy()}
        return dict_gradients
