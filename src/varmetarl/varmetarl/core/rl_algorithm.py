"""
soft actor critic training algorithm
"""
import copy
import logging
import os
import sys
import json
import pickle
import time
# import exputils.data.logging as log
from abc import ABC, abstractmethod
import exputils as eu
import numpy as np
import torch
import os
import gymnasium as gym
import exputils.data.logging as log
from alive_progress import alive_bar
from varmetarl.policy import TanhPolicy
from varmetarl.replay_buffer import MetaReplayBuffer
from varmetarl.runner import Runner
from varmetarl.utils import Logger, get_time, config_logger

logger = logging.getLogger('pearl')


class MetaRlAlgorithm(ABC):
    """
    Base class for rl algorithm.
    """
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            policy = eu.AttrDict(
                cls = TanhPolicy,
                config = eu.AttrDict()
            ),
            meta_replay_buffer = eu.AttrDict(
                cls = MetaReplayBuffer,
                config = eu.AttrDict()
            ),
            runner = eu.AttrDict(
                cls = Runner,
                config = eu.AttrDict()
            ),
            num_tasks_sample=5,
            meta_batch=16,
            iteration=1000,
            gradient_step=100,
            batch_size=256,
            work_dir = os.getcwd(),            
        )
        
        return default_config

    def __init__(self, env, train_tasks, test_tasks, config = None, **kwargs):

        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.env = env
        if type(self.env.observation_space) is gym.spaces.Box:
            self.obs_dim = int(np.prod(env.observation_space.shape))
        else:
            self.obs_dim = {key: self.env.observation_space[key].shape for key in self.env.observation_space}

        self.action_dim = int(np.prod(env.action_space.shape))
        self.reward_dim = 1
        
        policy = eu.misc.create_object_from_config(self.config.policy, self.obs_dim, self.action_dim, self.config.latent_dim).to(self.device)
        self.runner = eu.misc.create_object_from_config(self.config.runner, env, train_tasks, test_tasks)
        self.meta_replay_buffer = eu.misc.create_object_from_config(self.config.meta_replay_buffer, train_tasks)
        
        self.networks = {"policy": policy}
        self.debut = get_time()
 
        # if not os.path.exists(f'output/models/{self.debut}'):
        #     os.makedirs(f'output/models/{self.debut}')
        # self.writer = Logger(f"output/models/{self.debut}/",
        #                      self.config.gradient_step,
        #                      self.config.iteration, self.config.batch_size,
        #                      sampling_rate=500)
        # config_logger(f"output/models/{self.debut}/")
        if not os.path.exists(self.config.work_dir):
            os.makedirs(self.config.work_dir)
        self.writer = Logger(self.config.work_dir,
                             self.config.gradient_step,
                             self.config.iteration, self.config.batch_size,
                             sampling_rate=500)
        config_logger(self.config.work_dir)
        log.activate_tensorboard()

    @property
    def policy(self):
        return self.networks["policy"]

    def train(self):
        self.save_attributes()
        deb_time = time.time()
        self._init_training()
        fin_time = time.time()
        logger.info("Training intialization finished in %f", fin_time-deb_time)
        if sys.stdout.isatty():
            logger.info("Training bar will be displayed")
            with alive_bar(self.config.iteration) as step_bar:
                self.train_loop(step_bar=step_bar)
        else:
            logger.info("There will be no training bar")
            self.train_loop()
        self.save()
        logger.info("Final weight of the model are saved at %s",
                    f"output/models/{self.debut}/")
        self.writer.close()
        self.end_training()

    def train_loop(self, step_bar=None):
        train_step = self.meta_replay_buffer.get_init_step()*len(self.runner.train_env)
        deb_time = time.time()
        self.test(epoch=0, random=True,
                  train_step=train_step)
        fin_time = time.time()
        logger.info("Testing finished in %f", fin_time-deb_time)
        for i in range(self.config.iteration):
            deb_time = time.time()
            train_step = self.runner.run(self.config.num_tasks_sample,
                                         self.networks,
                                         self.meta_replay_buffer)
            fin_time = time.time()
            logger.info("Running finished in %f", fin_time-deb_time)
            deb_time = time.time()
            for j in range(int(self.config.gradient_step)):
                list_idx = np.random.choice(
                    self.runner.train_env, self.config.meta_batch)
                self._training_step(
                    i, j, list_idx, self.runner.config.n_adaptation_steps)
            fin_time = time.time()
            logger.info("Training finished in %f", fin_time-deb_time)
            deb_time = time.time()
            self.test(epoch=i+1, random=True, train_step=train_step)
            fin_time = time.time()
            logger.info("Testing finished in %f", fin_time-deb_time)
            if not i % 10:
                self.save()
            if step_bar:
                step_bar()
            logger.info("Iteration %d finished", i)

    def test(self, epoch=None, random=False, train_step=0):
        if train_step:
            list_task_reward = self.runner.test(self.networks)
            mean_return = np.mean([list_task_reward[idx]['mean']
                                   for idx in list_task_reward])
            kwargs = self.compute_metrics()
            if random:
                list_task_reward_random = self.runner.run_random()
                random_return = np.mean([list_task_reward_random[idx]['mean']
                                         for idx in list_task_reward_random])
                self.writer.log_return(train_step,
                                       mean_return,
                                       random=random_return,
                                       **kwargs)
            else:
                self.writer.log_return(train_step,
                                       mean_return,
                                       **kwargs)
        else:
            list_task_reward = self.runner.test(self.networks)
            mean_return = np.mean([list_task_reward[idx]['mean']
                                   for idx in list_task_reward])
        # log.add_value('mean_return', mean_return)
        # TODO: log performance in each  test environment
        self.save_test_perf(list_task_reward, epoch)
        logger.info("after epoch %d (%d training_step), \
the mean return is %f \
on %d episodes for each task", epoch, train_step, mean_return, self.runner.config.test_ep)
        return mean_return, list_task_reward

    def render(self, max_steps):
        self.runner.render(self.policy, max_steps)

    def save_test_perf(self, list_info, epoch):
        test_path = os.path.join(self.config.work_dir, 'test_perf')
        if not os.path.exists(test_path):
            os.makedirs(test_path)
        pkl_file = os.path.join(test_path, f'list_reward_epoch_{epoch}.pkl')
        with open(pkl_file, "wb") as info_file:
            pickle.dump(list_info, info_file)


    def save_attributes(self):
        # if not os.path.exists(self.config.work_dir+f'{self.debut}'):
        #     os.makedirs(self.config.work_dir+f'{self.debut}')
        parameter_file = os.path.join(self.config.work_dir, 'parameters.txt')
        with open(parameter_file, "w") as f:
            f.write(str(self.__dict__)+'\n')
            f.write(str(self.runner.__dict__)+'\n')
            f.write(str(self.meta_replay_buffer.__dict__)+'\n')
        meta_file = os.path.join(self.config.work_dir, 'metadata.pkl')
        pickle.dump({'cfg': self.config, 'env': self.env.config}, open(meta_file, "wb"))


    def save(self, actor_path=None, critic_path=None):
        # if not os.path.exists(f'output/models/{self.debut}'):
        #     os.makedirs(f'output/models/{self.debut}')

        # if actor_path is None:
        #     actor_path = f"output/models/{self.debut}/pearl_actor_{self.runner.env.name}"
        # torch.save(self.policy.state_dict(), actor_path)
        for key, value in self.networks.items():
            path = os.path.join(self.config.work_dir, f'pearl_{key}_{self.runner.env.name}')
            torch.save(value.state_dict(),
                       path)
        logger.info(f'Saving models to output/models/{self.debut}/pearl_*')

    def load(self, path=None):
        for key, value in self.networks.items():
            network_path = os.path.join(path, f'pearl_{key}_{self.runner.env.name}')
            weight = torch.load(
                network_path)
            value.load_state_dict(weight)
        logger.info(f'Load models from output/models/{path}/pearl_*')

    def end_training(self):
        # log.save()
        # log.add_single_object('final_policy', self.policy)
        num_episodes = 5
        mean, list_reward = self.test(num_episodes)
        log.save()        
        # if not os.path.exists(f'output/models/{self.debut}'):
        #     os.makedirs(f'output/models/{self.debut}')
        summary_file = os.path.join(self.config.work_dir, "summary.txt")
        with open(summary_file, "w") as f:
            f.write(
                f"Training began at {self.debut} finished without problem at {get_time()}"+'\n')
            f.write(
                f"Final performance is {mean} ({list_reward}) on {num_episodes} episodes"+'\n')
            f.write(
                f"Training curves are available at runs/{self.debut}_andromeda")

    @abstractmethod
    def compute_metrics(self):
        return {}

    @abstractmethod
    def _init_training(self):
        print("not yet implemented")
        pass

    @abstractmethod
    def _training_step(self, i, j, list_idx, n_adaptation_steps):
        print("not yet implemented")
        pass
