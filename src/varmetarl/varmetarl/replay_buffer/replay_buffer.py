import random
import torch
import numpy as np


def stack(elements):
    if isinstance(elements[0], dict):
        stack = {key: torch.stack(list(map(lambda x: x[key], elements))) for key in elements[0]}
    else:
        stack = torch.stack(elements)
    return stack

class ReplayBuffer:
    def __init__(self, capacity, init_step, device='cpu'):
        #random.seed(seed)
        self.capacity = int(capacity)
        self.init_step = int(init_step)
        self.buffer = []
        self.position = 0
        self.device = device

    def get_init_step(self):
        return self.init_step

    def add(self, state, action, reward, next_state):
        if len(self.buffer) < self.capacity:
            self.buffer.append(None)
        if isinstance(state, dict):
            state = {key: torch.from_numpy(np.float32(value))
                     for key, value in state.items()}
        else:
            state = torch.from_numpy(np.float32(state))
        action = torch.as_tensor(action, dtype=torch.float32).reshape(-1,)
        reward = torch.as_tensor(reward, dtype=torch.float32).reshape(-1,)
        if isinstance(next_state, dict):
            next_state = {key: torch.from_numpy(np.float32(value))
                          for key, value in next_state.items()}
        else:
            next_state = torch.from_numpy(np.float32(next_state))
        self.buffer[self.position] = (state, action, reward, next_state)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        batch = random.sample(self.buffer, batch_size)
        #print(batch[0][1])
        state, action, reward, next_state = map(stack, zip(*batch))
        if isinstance(state, dict):
            return {key: value.to(self.device)
                     for key, value in state.items()},\
                action.to(self.device),\
                reward.to(self.device),\
                {key: value.to(self.device)
                     for key, value in next_state.items()}
        else:
            return state.to(self.device),\
                   action.to(self.device),\
                   reward.to(self.device),\
                   next_state.to(self.device)

    def __len__(self):
        return len(self.buffer)

    def clear(self):
        self.buffer = []
        self.position = 0