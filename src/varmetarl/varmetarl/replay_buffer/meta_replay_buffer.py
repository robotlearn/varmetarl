import random

import numpy as np
import torch
import exputils as eu
from .replay_buffer import ReplayBuffer


class MetaReplayBuffer:
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            capacity = 1e6,
            init_step = 2e3
        )
        return default_config
    
    def __init__(self, tasks, config = None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.init_step = self.config.init_step
        self.idx = random.choice(tasks)
        self.tasks_replay_buffer = {task: ReplayBuffer(
            self.config.capacity, self.config.init_step, self.device) for task in tasks}
        self.current_replay_buffer = self.tasks_replay_buffer[self.idx]
        self.enc_meta_replay_buffer = {task: ReplayBuffer(
            self.config.capacity, self.config.init_step, self.device) for task in tasks}
        self.enc_replay_buffer = self.enc_meta_replay_buffer[self.idx]
        self.encoder_data = False

    def get_init_step(self):
        return self.init_step

    def collect_encoder_data(self, value):
        self.encoder_data = value

    def add(self, state, action, reward, next_state):
        self.current_replay_buffer.add(state, action, reward, next_state)
        if self.encoder_data:
            self.enc_replay_buffer.add(state, action, reward, next_state)

    def set_idx(self, idx):
        self.idx = idx
        self.current_replay_buffer = self.tasks_replay_buffer[self.idx]
        self.enc_replay_buffer = self.enc_meta_replay_buffer[self.idx]
        self.enc_replay_buffer.clear()

    def sample_context(self, list_idx, n_adaptation_steps):
        if not hasattr(list_idx, '__iter__'):
            return self.enc_meta_replay_buffer[list_idx].sample(n_adaptation_steps)
        return [self.enc_meta_replay_buffer[idx].sample(n_adaptation_steps) for idx in list_idx]

    def sample(self, list_idx, batch_size):
        if not hasattr(list_idx, '__iter__'):
            return self.tasks_replay_buffer[list_idx].sample(batch_size)
        else:
            meta_transitions = [self.tasks_replay_buffer[idx].sample(
                batch_size) for idx in list_idx]
            obs, action, reward, next_obs = self.unpack(meta_transitions)
        if isinstance(obs, dict):
            return {key: value.to(self.device)
                     for key, value in obs.items()},\
                action.to(self.device),\
                reward.to(self.device),\
                {key: value.to(self.device)
                     for key, value in next_obs.items()}
        else:
            return obs.to(self.device),\
                   action.to(self.device),\
                   reward.to(self.device),\
                   next_obs.to(self.device)

    def unpack(self, meta_transitions):
        transitions = []
        for task_transition in meta_transitions:
            obs, action, reward, next_obs = task_transition
            transitions.append({'obs': obs,
                                'action': action,
                                'reward': reward,
                                'next_obs': next_obs})
        if isinstance(obs, dict):
            obs = {key: torch.cat([transition['obs'][key] for transition in transitions])
                    for key in obs.keys()}
            next_obs = {key: torch.cat([transition['obs'][key] for transition in transitions])
                    for key in next_obs.keys()}
        else:
            obs = torch.cat([transition['obs'] for transition in transitions])
            next_obs = torch.cat([transition['next_obs']
                              for transition in transitions])
        action = torch.cat([transition['action']
                            for transition in transitions])
        reward = torch.cat([transition['reward']
                            for transition in transitions])
        return obs, action, reward, next_obs
