import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
from .gaussian_map import GaussMappingLayer
# Initialize Policy weights
import exputils as eu
logger = logging.getLogger('pearl')


def weights_init_(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight, gain=1)
        torch.nn.init.constant_(m.bias, 0)
        

class ValueNetworkCnn(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            gaussian_map = None
        )
        return default_config          
    
    def __init__(self, *args, config = None, **kwargs):
        super(ValueNetworkCnn, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        latent_dim = args[1]
        obs_dim = args[0]
        self.grid_dim = obs_dim['occupancy_grid']
        self.input_for_FC_net_dim = obs_dim['input_for_FC_net']     
        
        self.conv = nn.Sequential(
            nn.Conv2d(self.grid_dim[0], 32, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU(),
            nn.Flatten(),
        )
        
        conv_out_size = self._conv_out()
        
        self.intermediate_layer = nn.Sequential(
            nn.Linear(conv_out_size, 128),
            nn.ReLU(),
            nn.Linear(128, 28),
            nn.ReLU())           
                
        # print(self._conv_out())
        inter_out_size = 28     
        inter_out_size += self.input_for_FC_net_dim[0] 
        
        if self.config.gaussian_map:
            if isinstance(self.config.gaussian_map, dict):
                logger.info("Using Gaussian map")
                self.config.gaussian_map.update({"size_in": latent_dim})
                self.gaussian_map = GaussMappingLayer(**self.config.gaussian_map)
            else:
                self.gaussian_map = self.config.gaussian_map
            inter_out_size += latent_dim*self.gaussian_map.n_neuron_per_input
            self.linear1 = nn.Linear(
                    inter_out_size, self.config.hidden_dim)
        else:
            inter_out_size += latent_dim
            self.linear1 = nn.Linear(inter_out_size, self.config.hidden_dim)

        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.linear3 = nn.Linear(self.config.hidden_dim, 1)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)

    @property
    def device(self):
        return self.device_indicator.device

    def forward(self, tensor_dict):
        state = tensor_dict['obs']
        grid = state['occupancy_grid']
        input_for_FC_net = state['input_for_FC_net']
 
        task_z = tensor_dict['task_z']
        if hasattr(self, 'gaussian_map'):
            task_z = self.gaussian_map(task_z)

        conv_out = self.conv(grid)
        inter_out = self.intermediate_layer(conv_out)     
        xu = torch.cat((inter_out, input_for_FC_net, task_z), 1)

        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        x = self.linear3(x)
        return x
    
    def _conv_out(self):
        ''' Compute the input flatten dimension of the FC net '''
        o = self.conv(torch.zeros((1, self.grid_dim[0], self.grid_dim[1], self.grid_dim[2])))
        return int(o.shape[1])           
    
class QNetworkCnn(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            gaussian_map = None
        )
        return default_config  
        
    def __init__(self, *args, config = None, **kwargs):
        super(QNetworkCnn, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        latent_dim = args[2]
        obs_dim = args[0]
        self.grid_dim = obs_dim['occupancy_grid']
        self.input_for_FC_net_dim = obs_dim['input_for_FC_net']   
        num_actions = args[1]
        
        self.conv = nn.Sequential(
            nn.Conv2d(self.grid_dim[0], 32, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU(),
            nn.Flatten(),
        )
        
        conv_out_size = self._conv_out()
        
        self.intermediate_layer = nn.Sequential(
            nn.Linear(conv_out_size, 128),
            nn.ReLU(),
            nn.Linear(128, 28),
            nn.ReLU())           
                
        # print(self._conv_out())
        inter_out_size = 28     
        inter_out_size += self.input_for_FC_net_dim[0] 
        inter_out_size += num_actions
        
        if self.config.gaussian_map:
            if isinstance(self.config.gaussian_map, dict):
                logger.info("Using Gaussian map")
                self.config.gaussian_map.update({"size_in": latent_dim})
                self.gaussian_map = GaussMappingLayer(**self.config.gaussian_map)
            else:
                self.gaussian_map = self.config.gaussian_map
            inter_out_size += latent_dim*self.gaussian_map.n_neuron_per_input
            self.linear1 = nn.Linear(
                    inter_out_size, self.config.hidden_dim)
        else:
            inter_out_size += latent_dim
            self.linear1 = nn.Linear(inter_out_size, self.config.hidden_dim)
            
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.linear3 = nn.Linear(self.config.hidden_dim, 1)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)


    def forward(self, tensor_dict):
        state = tensor_dict['obs']
        grid = state['occupancy_grid']
        action = tensor_dict['action']
        input_for_FC_net = state['input_for_FC_net']
 
        task_z = tensor_dict['task_z']
        if hasattr(self, 'gaussian_map'):
            task_z = self.gaussian_map(task_z)

        conv_out = self.conv(grid)
        inter_out = self.intermediate_layer(conv_out)     
        xu = torch.cat((inter_out, input_for_FC_net, action, task_z), 1)
        x1 = F.relu(self.linear1(xu))
        x1 = F.relu(self.linear2(x1))
        x1 = self.linear3(x1)
        return x1

    def _conv_out(self):
        ''' Compute the input flatten dimension of the FC net '''
        o = self.conv(torch.zeros((1, self.grid_dim[0], self.grid_dim[1], self.grid_dim[2])))
        return int(o.shape[1])       