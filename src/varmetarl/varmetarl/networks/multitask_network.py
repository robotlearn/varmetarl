import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
from .gaussian_map import GaussMappingLayer
# Initialize Policy weights
import exputils as eu
logger = logging.getLogger('pearl')


def weights_init_(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight, gain=1)
        torch.nn.init.constant_(m.bias, 0)


class ValueNetworkMulti(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            gaussian_map = None
        )
        return default_config
    
    def __init__(self, *args, config = None, **kwargs):
        super(ValueNetworkMulti, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        num_inputs = args[0]        
        self.linear1 = nn.Linear(num_inputs, self.config.hidden_dim)
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.linear3 = nn.Linear(self.config.hidden_dim, 1)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)

    @property
    def device(self):
        return self.device_indicator.device

    def forward(self, tensor_dict):
        state = tensor_dict['obs']
        xu = torch.cat([state], 1)
        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        x = self.linear3(x)
        return x

    def register_gradients(self):
        dict_gradients = {}
        dict_gradients['linear1'] = {'mean': self.linear1.weight.grad.mean(),
                                     'std': self.linear1.weight.grad.std(),
                                     'max': self.linear1.weight.grad.max(),
                                     'min': self.linear1.weight.grad.min()}
        dict_gradients['linear2'] = {'mean': self.linear2.weight.grad.mean(),
                                     'std': self.linear2.weight.grad.std(),
                                     'max': self.linear2.weight.grad.max(),
                                     'min': self.linear2.weight.grad.min()}
        dict_gradients['linear3'] = {'mean': self.linear3.weight.grad.mean(),
                                     'std': self.linear3.weight.grad.std(),
                                     'max': self.linear3.weight.grad.max(),
                                     'min': self.linear3.weight.grad.min()}
        return dict_gradients


class QNetworkMulti(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            gaussian_map = None
        )
        return default_config  
        
    def __init__(self, *args, config = None, **kwargs):
        super(QNetworkMulti, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        num_inputs = args[0]
        num_actions = args[1]
        self.linear1 = nn.Linear(
                num_inputs+num_actions, self.config.hidden_dim)
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.linear3 = nn.Linear(self.config.hidden_dim, 1)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)

    def forward(self, tensor_dict):
        state = tensor_dict['obs']
        action = tensor_dict['action']
        xu = torch.cat([state, action], 1)
        x1 = F.relu(self.linear1(xu))
        x1 = F.relu(self.linear2(x1))
        x1 = self.linear3(x1)
        return x1

    def register_gradients(self):
        dict_gradients = {}
        dict_gradients['linear1'] = {'mean': self.linear1.weight.grad.mean(),
                                     'std': self.linear1.weight.grad.std(),
                                     'max': self.linear1.weight.grad.max(),
                                     'min': self.linear1.weight.grad.min()}
        dict_gradients['linear2'] = {'mean': self.linear2.weight.grad.mean(),
                                     'std': self.linear2.weight.grad.std(),
                                     'max': self.linear2.weight.grad.max(),
                                     'min': self.linear2.weight.grad.min()}
        dict_gradients['linear3'] = {'mean': self.linear3.weight.grad.mean(),
                                     'std': self.linear3.weight.grad.std(),
                                     'max': self.linear3.weight.grad.max(),
                                     'min': self.linear3.weight.grad.min()}
        return dict_gradients
