import numpy as np
import torch
import collections


class GaussMappingLayer(torch.nn.Module):
    def __init__(
        self, size_in, n_neuron_per_input=5, ranges=None, sigma=None, is_trainable=False
    ):
        super().__init__()

        self.size_in = size_in
        self.n_neuron_per_input = n_neuron_per_input
        self.is_trainable = is_trainable

        if ranges is None:
            self.ranges = np.array([[-1.0, 1.0]] * self.size_in)
        elif np.ndim(ranges) == 1:
            self.ranges = np.array([ranges] * self.size_in)
        else:
            self.ranges = np.array(ranges)

        self.size_out = self.size_in * self.n_neuron_per_input

        self.peaks = torch.Tensor(self.size_out)
        for input_idx in range(self.size_in):
            start_idx = input_idx * self.n_neuron_per_input
            end_idx = start_idx + self.n_neuron_per_input
            self.peaks[start_idx:end_idx] = torch.linspace(
                self.ranges[input_idx][0],
                self.ranges[input_idx][1],
                self.n_neuron_per_input,
            )

        # handle different types of sigma parameters and convert them to a list with one sigma per input
        if sigma is None:
            self.sigma = np.zeros(self.size_in)
            for input_idx in range(self.size_in):
                dist = (
                    self.peaks[input_idx * self.n_neuron_per_input + 1]
                    - self.peaks[input_idx * self.n_neuron_per_input]
                )
                self.sigma[input_idx] = np.log(
                    (-(dist ** 2) / (8 * np.log(0.5))) ** 0.5)
            self.min_sigma = -3
        elif not isinstance(sigma, collections.Sequence):
            self.sigma = np.ones(self.size_in) * np.log(sigma)
            self.min_sigma = -3
        else:
            self.sigma = np.log(sigma)
            self.min_sigma = -3

        self.sigmas = torch.Tensor(self.size_out)
        for input_idx in range(self.size_in):
            start_idx = input_idx * self.n_neuron_per_input
            end_idx = start_idx + self.n_neuron_per_input
            self.sigmas[start_idx:end_idx] = self.sigma[input_idx]
        if self.is_trainable:
            self.peaks = torch.nn.Parameter(self.peaks)
            self.sigmas = torch.nn.Parameter(self.sigmas)
        else:
            self.peaks = torch.nn.Parameter(self.peaks, requires_grad=False)
            self.sigmas = torch.nn.Parameter(self.sigmas, requires_grad=False)

    def forward(self, x):
        # reapeat input vector so that every map-neuron gets its accordingly input
        # example: n_neuron_per_inpu = 3 then [[1,2,3]] --> [[1,1,1,2,2,2,3,3,3]]
        x = x.repeat_interleave(repeats=self.n_neuron_per_input, dim=-1)

        x = torch.exp(-0.5 * ((x - self.peaks) / torch.exp(self.sigmas)) ** 2)

        return x

    def clip_variance(self):
        self.sigmas = torch.nn.Parameter(torch.clamp(self.sigmas, min=-1.6))
