import numpy as np
import gymnasium
from gymnasium import spaces


def return_one_hot(indice, size):
    one_hot = np.zeros(size)
    np.put(one_hot, [indice], [1])
    return one_hot


class DiscreteAction(gymnasium.ActionWrapper):
    r"""Rescales the continuous action space of the environment to a range [min_action, max_action].
    Example::
        >>> RescaleAction(env, min_action, max_action).action_space == Box(min_action, max_action)
        True
    """

    def __init__(self, env):
        assert isinstance(
            env.action_space, spaces.Discrete
        ), "expected Discrete action space, got {}".format(type(env.action_space))

        super(DiscreteAction, self).__init__(env)
        self.env.action_space.sample = lambda: return_one_hot(
            self.env.action_space.np_random.randint(self.env.action_space.n), self.env.action_space.n)

    def action(self, action):
        return np.argmax(action)
