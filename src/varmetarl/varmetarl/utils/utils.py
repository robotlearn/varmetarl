import datetime
import functools
import os
import logging
import time

logger = logging.getLogger('pearl')

def config_logger(path):

    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s : %(levelname)s - %(module)s - %(funcName)s : %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    if not os.path.exists(path):
        os.makedirs(path)
    file_path = os.path.join(path,"logger.log")
    file = logging.FileHandler(file_path)
    file.setLevel(logging.DEBUG)
    file.setFormatter(formatter)
    logger.addHandler(file)

def get_time():
    date = datetime.datetime.now()
    return date.strftime("%Y-%m-%d_%H:%M:%S")


def generate_optimizers(networks, parameters, optim):
    return {key: optim(value.parameters(),
                       **parameters[key]) for (key, value) in networks.items()}


def debug(func):
    """Print the function signature and return value."""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


def slow_down(func):
    """Sleep 1 second before calling the function"""
    @functools.wraps(func)
    def wrapper_slow_down(*args, **kwargs):
        time.sleep(1)
        return func(*args, **kwargs)
    return wrapper_slow_down
