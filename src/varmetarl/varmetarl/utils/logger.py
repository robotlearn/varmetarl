import torch
import os
import torch.nn as nn
import numpy as np
from torch.utils.tensorboard import SummaryWriter
import exputils.data.logging as log

class Logger:
    """
    """

    def __init__(self, path, gradient_step, iterations, batch_size, sampling_rate=1):
        #self.env_step = env_step
        self.path = path
        self.total_step = 0
        self.gradient_step = gradient_step
        self.iterations = iterations
        self.batch_size = batch_size
        self.sampling_rate = sampling_rate
        # self.writer = SummaryWriter(log_dir=self.path)

    def log_policy(self, epoch, step, policy_loss, **kwargs):
        if not step % self.sampling_rate:
            log.add_scalar("Loss/policy_loss", policy_loss.detach().cpu(),
                                   tb_global_step=epoch*self.gradient_step + step)
            if kwargs:
                log.add_scalar("policy_loss/loss", policy_loss.detach().cpu(),
                                       tb_global_step=epoch*self.gradient_step + step)
                for key in kwargs:
                    log.add_scalar(f"policy_loss/{key}", kwargs[key].detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)

    def log_q_function(self, epoch, step, q_loss, **kwargs):
        if not step % self.sampling_rate:
            log.add_scalar("Loss/q_loss", q_loss.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
            if kwargs:
                log.add_scalar("q_loss/loss", q_loss.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
                for key in kwargs:
                    log.add_scalar(f"q_loss/{key}", kwargs[key].detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)

    def log_value_function(self, epoch, step, value_loss, **kwargs):
        if not step % self.sampling_rate:
            log.add_scalar("Loss/value_loss", value_loss.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
            if kwargs:
                log.add_scalar("value_loss/loss", value_loss.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
                for key in kwargs:
                    log.add_scalar(f"value_loss/{key}", kwargs[key].detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)

    def log_return(self, train_step, mean_return, **kwargs):
        self.total_step += train_step
        if kwargs:
            if 'random' in kwargs:
                log.add_scalar("Loss/average_return_policy", mean_return, tb_global_step=self.total_step)
                
                log.add_scalar("Loss/average_return_random", kwargs['random'], tb_global_step=self.total_step)
                                        
            else:
                log.add_scalar(
                    "Loss/average_return", mean_return, tb_global_step=self.total_step)
            log.add_scalar("return/loss", mean_return, tb_global_step=self.total_step)
            for key in kwargs:
                log.add_scalar(
                    f"return/{key}", kwargs[key], tb_global_step=self.total_step)
        else:
            log.add_scalar("Loss/average_return",
                                   mean_return, tb_global_step=self.total_step)
        log.add_value('total_step', self.total_step)

    def log_encoder_function(self, epoch, step, kl_loss, **kwargs):
        if not step % self.sampling_rate:
            log.add_scalar("Loss/kl_loss", kl_loss.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
            if kwargs:
                log.add_scalar("kl_loss/loss", kl_loss,
                                       epoch*self.gradient_step + step)
                for key in kwargs:
                    if key=='z_var':
                        for index, var in enumerate(torch.unbind(kwargs['z_var'])):
                            #print(var)
                            log.add_scalar(f"kl_loss/z_var_{index}", var.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
                    elif key=='kl_divs':
                        for index, kl in enumerate(torch.unbind(kwargs['kl_divs'][:,0])):
                            #print(kl)
                            log.add_scalar(f"kl_loss/kl_divs_{index}", kl.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
                    elif key=='z_mean':
                        for index, mean in enumerate(torch.unbind(kwargs['z_mean'])):
                            #print(mean)
                            log.add_scalar(f"kl_loss/z_mean_{index}", mean.detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)
                    else:
                        log.add_scalar(
                            f"kl_loss/{key}", kwargs[key].detach().cpu(),
                                           tb_global_step=epoch*self.gradient_step + step)

    def log_encoder(self, epoch, step, encoder, supl = ""):
        if not step % self.sampling_rate:
            if not os.path.exists(self.path+'/grad_stat'):
                os.makedirs(self.path+'/grad_stat')
            all_grad_stat = np.zeros(4,)
            dico = encoder.register_gradients()
            for key in dico.keys():
                grad_stat = np.hstack((dico[key]["mean"],
                                       dico[key]["std"],
                                       dico[key]["max"],
                                       dico[key]["min"]))
                all_grad_stat = np.vstack((all_grad_stat, grad_stat))
            if supl == "":
                np.save(self.path+'/grad_stat/'+f"grad_stat_step_{epoch*self.gradient_step + step}", all_grad_stat[1:])
            else:
                np.save(self.path+'/grad_stat/'+f"grad_stat_step_{epoch*self.gradient_step + step}"+supl, all_grad_stat[1:])



    def close(self):
        pass
