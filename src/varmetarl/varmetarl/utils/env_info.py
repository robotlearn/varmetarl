import gymnasium

class EnvInfo(gymnasium.Wrapper):
    """
    Add properties to the environment like the name of the environement to make them easily accessible
    Example::
        >>> EnvInfo(gym.make("Pendulum-v0")).id
        Pendulum-v0
    """

    @property
    def id(self):
        return self.env.spec.id

    @property
    def name(self):
        return self.env.spec._env_name

    @property
    def max_episode_steps(self):
        return self.env.spec.max_episode_steps
