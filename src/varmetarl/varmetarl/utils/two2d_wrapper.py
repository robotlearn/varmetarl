import numpy as np
import gymnasium as gym
from gymnasium import spaces


def return_one_hot(indice, size):
    one_hot = np.zeros(size)
    np.put(one_hot, [indice], [1])
    return one_hot


class Two2dWrapper(gym.ObservationWrapper):
    r"""Rescales the continuous action space of the environment to a range [min_action, max_action].
    Example::
        >>> RescaleAction(env, min_action, max_action).action_space == Box(min_action, max_action)
        True
    """

    def __init__(self, env):

        super(Two2dWrapper, self).__init__(env)

    def observation(self, observation):
        observation = observation['observation']
        #print(observation)
        return observation
