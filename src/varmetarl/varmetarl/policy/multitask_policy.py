import logging
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from varmetarl.policy.distributions import TanhNormal
from torch.distributions import Categorical
from varmetarl.networks import GaussMappingLayer
import exputils as eu

LOG_SIG_MAX = 2
LOG_SIG_MIN = -20

# Initialize Policy weights
logger = logging.getLogger('pearl')

def weights_init_(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(
            m.weight, gain=nn.init.calculate_gain('relu'))
        torch.nn.init.constant_(m.bias, 0)


class TanhPolicyMultiTask(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            epsilon = 0.0001,
            gaussian_map = None
        )
        return default_config          
    
    def __init__(self, *args, config = None, **kwargs):
        super().__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        num_inputs = args[0]
        num_actions = args[1]
        self.linear1 = nn.Linear(num_inputs, self.config.hidden_dim)
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.mean_linear = nn.Linear(self.config.hidden_dim, num_actions)
        self.log_std_linear = nn.Linear(self.config.hidden_dim, num_actions)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)

    def forward(self, tensor_dict):
        # print(state)
        state = tensor_dict['obs']
        xu = torch.cat([state], -1)
        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        mean = self.mean_linear(x)
        log_std = self.log_std_linear(x)
        log_std = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        std = torch.exp(log_std)
        return mean, std

    @torch.no_grad()
    def select_action(self, state, evaluate=False):
        """
        Select an action knowing the state of the environment
        and according to the current policy.

        Parameters
        ----------
        state : 'numpy.ndarray'
            state of the environment

        Returns
        -------
        'numpy.ndarray'
            action taken by the policy.
        """
        state = torch.from_numpy(np.float32(state))
        state = state.to(self.device_indicator.device)
        # state = state.reshape(1, -1)
        # task_z = task_z.reshape(1, -1)
        mean, std = self({'obs': state})
        tanh_normal = TanhNormal(mean, std)
        if evaluate:
            action = torch.tanh(mean)
        else:
            action, _ = tanh_normal.sample(return_pretanh_value=True)
        return action.cpu().numpy()

    def log(self, tensor_dict, reparametrize):
        mean, std = self(tensor_dict)
        tanh_normal = TanhNormal(mean, std)
        if reparametrize:
            action, pre_tanh_value = tanh_normal.rsample(
                return_pretanh_value=True)
        else:
            action, pre_tanh_value = tanh_normal.sample(
                return_pretanh_value=True)
        log_prob = tanh_normal.log_prob(action,
                                        pre_tanh_value=pre_tanh_value)
        log_prob = log_prob.sum(dim=1, keepdim=True)
        return action, pre_tanh_value, log_prob

