from .pearl_policy import TanhPolicy
from .multitask_policy import TanhPolicyMultiTask
from .pearl_policy_cnn import TanhPolicyCnn