import logging
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from varmetarl.policy.distributions import TanhNormal
from torch.distributions import Categorical
from varmetarl.networks import GaussMappingLayer
import exputils as eu

LOG_SIG_MAX = 2
LOG_SIG_MIN = -20

# Initialize Policy weights
logger = logging.getLogger('pearl')

def weights_init_(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(
            m.weight, gain=nn.init.calculate_gain('relu'))
        torch.nn.init.constant_(m.bias, 0)


class TanhPolicy(nn.Module):
    
    @staticmethod
    def default_config():
        default_config = eu.AttrDict(
            hidden_dim = 256,
            epsilon = 0.0001,
            gaussian_map = None
        )
        return default_config          
    
    def __init__(self, *args, config = None, **kwargs):
        super().__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        latent_dim = args[2]
        num_inputs = args[0]
        num_actions = args[1]
        if self.config.gaussian_map:
            if isinstance(self.config.gaussian_map, dict):
                logger.info("Using Gaussian map")
                self.config.gaussian_map.update({"size_in": latent_dim})
                self.gaussian_map = GaussMappingLayer(**self.config.gaussian_map)
            else:
                self.gaussian_map = self.config.gaussian_map
            self.linear1 = nn.Linear(
                num_inputs+latent_dim*self.gaussian_map.n_neuron_per_input, self.config.hidden_dim)
        else:
            self.linear1 = nn.Linear(num_inputs+latent_dim, self.config.hidden_dim)
        self.linear2 = nn.Linear(self.config.hidden_dim, self.config.hidden_dim)
        self.mean_linear = nn.Linear(self.config.hidden_dim, num_actions)
        self.log_std_linear = nn.Linear(self.config.hidden_dim, num_actions)

        self.device_indicator = nn.Parameter(torch.empty(0))
        # self.gaussian_map.to(self.device_indicator)
        self.apply(weights_init_)

    def forward(self, tensor_dict):
        # print(state)
        state = tensor_dict['obs']
        task_z = tensor_dict['task_z']
        if hasattr(self, 'gaussian_map'):
            task_z = self.gaussian_map(task_z)
        xu = torch.cat([state, task_z], -1)
        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        mean = self.mean_linear(x)
        log_std = self.log_std_linear(x)
        log_std = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        std = torch.exp(log_std)
        return mean, std

    @torch.no_grad()
    def select_action(self, state, task_z, evaluate=False):
        """
        Select an action knowing the state of the environment
        and according to the current policy.

        Parameters
        ----------
        state : 'numpy.ndarray'
            state of the environment

        Returns
        -------
        'numpy.ndarray'
            action taken by the policy.
        """
        state = torch.from_numpy(np.float32(state))
        state = state.to(self.device_indicator.device)
        # state = state.reshape(1, -1)
        # task_z = task_z.reshape(1, -1)
        state.unsqueeze(0)
        task_z.unsqueeze(0)
        mean, std = self({'obs': state, 'task_z': task_z})
        tanh_normal = TanhNormal(mean, std)
        if evaluate:
            action = torch.tanh(mean)
        else:
            action, _ = tanh_normal.sample(return_pretanh_value=True)
        return action.cpu().numpy()

    def log(self, tensor_dict, reparametrize):
        mean, std = self(tensor_dict)
        tanh_normal = TanhNormal(mean, std)
        if reparametrize:
            action, pre_tanh_value = tanh_normal.rsample(
                return_pretanh_value=True)
        else:
            action, pre_tanh_value = tanh_normal.sample(
                return_pretanh_value=True)
        log_prob = tanh_normal.log_prob(action,
                                        pre_tanh_value=pre_tanh_value)
        log_prob = log_prob.sum(dim=1, keepdim=True)
        return action, pre_tanh_value, log_prob

    def register_gradients(self):
        dict_gradients = {}
        dict_gradients['linear1'] = {'mean': self.linear1.weight.grad.mean(),
                                     'std': self.linear1.weight.grad.std(),
                                     'max': self.linear1.weight.grad.max(),
                                     'min': self.linear1.weight.grad.min()}
        dict_gradients['linear2'] = {'mean': self.linear2.weight.grad.mean(),
                                     'std': self.linear2.weight.grad.std(),
                                     'max': self.linear2.weight.grad.max(),
                                     'min': self.linear2.weight.grad.min()}
        dict_gradients['mean_linear'] = {'mean': self.mean_linear.weight.grad.mean(),
                                         'std': self.mean_linear.weight.grad.std(),
                                         'max': self.mean_linear.weight.grad.max(),
                                         'min': self.mean_linear.weight.grad.min()}
        dict_gradients['log_std_linear'] = {'mean': self.log_std_linear.weight.grad.mean(),
                                            'std': self.log_std_linear.weight.grad.std(),
                                            'max': self.log_std_linear.weight.grad.max(),
                                            'min': self.log_std_linear.weight.grad.min()}
        return dict_gradients


class DiscretePolicy(nn.Module):
    def __init__(self, num_inputs, num_actions, latent_dim, hidden_dim, gaussian_map=None):
        super().__init__()
        self.gaussian_map = gaussian_map
        if self.gaussian_map:
            self.linear1 = nn.Linear(
                num_inputs+latent_dim*gaussian_map.n_neuron_per_input, hidden_dim)
        else:
            self.linear1 = nn.Linear(num_inputs+latent_dim, hidden_dim)
        self.linear2 = nn.Linear(hidden_dim, hidden_dim)
        self.linear3 = nn.Linear(hidden_dim, num_actions)
        self.softmax = nn.Softmax(dim=1)
        self.device_indicator = nn.Parameter(torch.empty(0))
        self.num_actions = num_actions
        self.apply(weights_init_)

    def forward(self, tensor_dict):
        # print(state)
        state = tensor_dict['obs']
        task_z = tensor_dict['task_z']
        if self.gaussian_map:
            task_z = self.gaussian_map(task_z)
        xu = torch.cat([state, task_z], -1)
        x = F.relu(self.linear1(xu))
        x = F.relu(self.linear2(x))
        output = self.linear3(x)
        probs = self.softmax(output)
        return probs

    @torch.no_grad()
    def select_action(self, state, task_z, evaluate=False):
        """
        Select an action knowing the state of the environment
        and according to the current policy.

        Parameters
        ----------
        state : 'numpy.ndarray'
            state of the environment

        Returns
        -------
        'numpy.ndarray'
            action taken by the policy.
        """
        state = torch.from_numpy(np.float32(state))
        state = state.to(self.device_indicator.device)
        # state = state.reshape(1, -1)
        # task_z = task_z.reshape(1, -1)
        probs = self({'obs': state, 'task_z': task_z})
        categorical = Categorical(probs)
        if evaluate:
            action = torch.argmax(probs)
        else:
            action, _ = categorical.sample()
        action = F.one_hot(action, num_classes=self.num_actions)
        return action.cpu().numpy()

    def log(self, tensor_dict, reparametrize):
        probs = self(tensor_dict)
        categorical = Categorical(probs)
        if reparametrize:
            action, pre_tanh_value = categorical.rsample()
        else:
            action, pre_tanh_value = categorical.sample()
        log_prob = categorical.log_prob(action)
        log_prob = log_prob.sum(dim=1, keepdim=True)
        return action, pre_tanh_value, log_prob
