"""
soft actor critic training algorithm
"""
import copy
import logging
import numpy as np
import torch
import torch.nn as nn
from varmetarl.core import MetaRlAlgorithm
from varmetarl.encoder import Encoder
from varmetarl.networks import QNetwork, ValueNetwork
from varmetarl.policy import TanhPolicy
from varmetarl.utils import generate_optimizers
import exputils as eu
logger = logging.getLogger('pearl')


class PEARL(MetaRlAlgorithm):
    """
    pearl algorithm.

    Implemented as defined in the work of Rakelly and al, 2019
    """
    
    @staticmethod
    def default_config():
        parent_config = MetaRlAlgorithm.default_config()
        default_config = eu.AttrDict(
            seed = 0,
            q_network=eu.AttrDict(
                cls = QNetwork,
                config = eu.AttrDict()
            ),
            q_learning_rate=3e-4,
            policy = eu.AttrDict(
                cls = TanhPolicy,
                config = eu.AttrDict()
            ),
            policy_learning_rate=3e-4,
            v_network=eu.AttrDict(
                cls = ValueNetwork,
                config = eu.AttrDict()
            ),
            v_learning_rate=3e-4,
            encoder = eu.AttrDict(
                cls = Encoder,
                config = eu.AttrDict()
            ),
            encoder_learning_rate=3e-4,
            latent_dim = 3,
            kl_lambda = 1,
            alpha = 5,
            tau=0.005,
            gamma=0.99,
        )
        default_config = eu.combine_dicts(default_config, parent_config)        
        return default_config 

    def __init__(self, env, train_tasks, test_tasks, config = None, **kwargs):
        super().__init__(env, train_tasks, test_tasks, config=config, **kwargs)
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        
        q1 = eu.misc.create_object_from_config(self.config.q_network, self.obs_dim, self.action_dim, self.config.latent_dim).to(self.device)
        q2 = eu.misc.create_object_from_config(self.config.q_network, self.obs_dim, self.action_dim, self.config.latent_dim).to(self.device)
        v_network = eu.misc.create_object_from_config(self.config.v_network, self.obs_dim, self.config.latent_dim).to(self.device)
        encoder = eu.misc.create_object_from_config(self.config.encoder, self.obs_dim, self.action_dim, self.reward_dim, self.config.latent_dim).to(self.device)
        
        self.networks.update({"q1": q1, "q2": q2, "v_network": v_network, "encoder": encoder})
        self.target_v_network = copy.deepcopy(self.v_network)
        learning_rate = {"policy": {"lr": self.config.policy_learning_rate},
                         "q1": {"lr": self.config.q_learning_rate},
                         "q2": {"lr": self.config.q_learning_rate},
                         "v_network": {"lr": self.config.v_learning_rate},
                         "encoder": {"lr": self.config.encoder_learning_rate}
                         }
        self.optimizers = generate_optimizers(
            self.networks, learning_rate, torch.optim.Adam)
        
        logger.info("Initialisation finished, kl = %f, alpha = %f, gamma = %f, policy is %s",
            self.config.kl_lambda,
            self.config.alpha,
            self.config.gamma,
            self.policy.__class__.__name__)

    @property
    def q1(self):
        return self.networks["q1"]

    @property
    def q2(self):
        return self.networks["q2"]

    @property
    def v_network(self):
        return self.networks["v_network"]
    
    @property
    def encoder(self):
        return self.networks["encoder"]

    def _init_training(self):
        self.runner.initialize_replay_buffer(
            self.networks, self.meta_replay_buffer)

    def _training_step(self, i, j, list_idx, n_adaptation_steps):
        context = self.meta_replay_buffer.sample_context(
            list_idx, n_adaptation_steps)
        # print(context)
        task_z, mean_z, std_z = self.compute_latent(context)
        # obs, action, reward, next_obs = self.meta_replay_buffer.sample(
        obs, action, reward, next_obs = self.meta_replay_buffer.sample(
            list_idx, self.config.batch_size)
        self.optimizers['encoder'].zero_grad()
        z_var, kl_divs, z_mean, kl_loss = self.kl_loss(mean_z, std_z)
        kl_loss.backward(retain_graph=True)
        # self.writer.log_encoder(i, j, self.encoder)
        self.optimizers['q1'].zero_grad()
        self.optimizers['q2'].zero_grad()
        x1, x2, y, q_loss = self.q_loss(
            obs, action, reward, next_obs, task_z)
        self.writer.log_q_function(i, j, q_loss, x1=x1, x2=x2, y=y)
        q_loss.backward()
        # self.writer.log_encoder(i, j, self.encoder, supl = "after_q")
        self.writer.log_encoder_function(
            i, j, kl_loss, z_var=z_var[:self.encoder.latent_dim], kl_divs=kl_divs[:self.encoder.latent_dim], z_mean=z_mean[:self.encoder.latent_dim])
        self.optimizers['q1'].step()
        self.optimizers['q2'].step()
        self.optimizers['encoder'].step()
        self.optimizers['v_network'].zero_grad()
        x, y, z, value_loss = self.value_loss(obs, task_z)
        self.writer.log_value_function(
            i, j, value_loss, x=x, y=y, mse=z)
        value_loss.backward()
        self.optimizers['v_network'].step()
        self.optimizers['policy'].zero_grad()
        l_prob, y, policy_loss = self.policy_loss(obs, task_z)
        self.writer.log_policy(
            i, j, policy_loss, l_prob=l_prob, y=y)
        policy_loss.backward()
        self.optimizers['policy'].step()
        self.update_target_v_network()

    def compute_metrics(self):
        return {"nothing": 0}

    def compute_latent(self, context):
        latents = []
        for task_context in context:
            #Normally here each vector should be of size 1*latent_dim
            obs, action, reward, next_obs = task_context
            task_z, mean_z, std_z = self.encoder({'obs': obs,
                                                  'action': action,
                                                  'reward': reward,
                                                  'next_obs': next_obs})
            latents.append(
                {'task_z': task_z, 'mean_z': mean_z, 'std_z': std_z})
        task_z = [latent['task_z'].repeat(
            self.config.batch_size, 1) for latent in latents]
        task_z = torch.cat(task_z, dim=0)
        mean_z = torch.cat([latent['mean_z'] for latent in latents])
        std_z = torch.cat([latent['std_z'] for latent in latents])
        return task_z, mean_z, std_z

    def kl_loss(self,  z_mean, z_var):
        prior = self.encoder.generate_prior()
        posteriors = [torch.distributions.Normal(mu, torch.sqrt(var)) for mu, var in zip(torch.unbind(z_mean),
                                                                                         torch.unbind(z_var))]
        kl_divs = [torch.distributions.kl.kl_divergence(
         post, prior) for post in posteriors]
        kl_div_sum = torch.sum(torch.stack(kl_divs))
        # print(z_var, z_mean, torch.stack(kl_divs))
        return z_var, torch.stack(kl_divs), z_mean, kl_div_sum

    def policy_loss(self, obs, task_z):
        action, _, log_prob = self.log_policy(
            {'obs': obs, 'task_z': task_z.detach()}, reparametrize=True)
        y = self.min_q({'obs': obs, 'action': action,
                        'task_z': task_z.detach()})
        loss = self.config.alpha * log_prob - y
        return log_prob.mean(), y.mean(), loss.mean()

    def value_loss(self, obs, task_z):
        loss_fn = nn.MSELoss()
        x = self.v_network({'obs': obs, 'task_z': task_z.detach()})
        with torch.no_grad():
            action, _, log_prob = self.log_policy(
                {'obs': obs, 'task_z': task_z})
            y = self.min_q({'obs': obs, 'action': action,
                            'task_z': task_z}) - self.config.alpha * log_prob
        z = torch.square(x - y)
        return x.mean(), y.mean(), z.mean(), loss_fn(x, y)

    def q_loss(self, obs, action, reward, next_obs, task_z):
        loss_fn = nn.MSELoss()
        x1 = self.q1({'obs': obs, 'action': action, 'task_z': task_z})
        x2 = self.q2({'obs': obs, 'action': action, 'task_z': task_z})
        with torch.no_grad():
            target_value = self.target_v_network({'obs': next_obs,
                                                  'task_z': task_z})
        y = reward + self.config.gamma * target_value
        loss = 1 / 2 * (loss_fn(x1, y) + loss_fn(x2, y))
        return x1.mean(), x2.mean(), y.mean(), loss

    def log_policy(self, tensor_dict, reparametrize=False):
        return self.policy.log(tensor_dict, reparametrize)

    def min_q(self, tensor_dict):
        return torch.min(self.q1(tensor_dict), self.q2(tensor_dict))

    def update_target_v_network(self):
        for target_param, value_param in zip(self.target_v_network.parameters(),
                                             self.v_network.parameters()):
            target_param.data.copy_(target_param * (1.0 - self.config.tau)
                                    + value_param * self.config.tau)
