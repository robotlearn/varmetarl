from pearl.replay_buffer import MetaReplayBuffer
import unittest
import numpy as np
import torch

class MetaTestReplayBuffer(unittest.TestCase):
    def setUp(self) -> None:
        self.capacity = 10
        self.init_step = 1
        self.device = "cpu"
        self.tasks = list(range(5))
        self.replay_buffer = MetaReplayBuffer(self.capacity, self.init_step, self.tasks)
        
    def test_get_init_step(self):
        self.assertEqual(self.replay_buffer.get_init_step(), self.init_step)

    def test_add_sample_numpy(self):
        self.replay_buffer.set_idx(3)
        state = np.random.rand(3,2)
        action = np.random.rand(3,)
        reward = np.random.rand(1,)
        next_state = np.random.rand(3,2)
        self.replay_buffer.add(state, action, reward, next_state)
        self.assertEqual(len(self.replay_buffer.tasks_replay_buffer[3]), 1)
        self.replay_buffer.collect_encoder_data(True)
        self.replay_buffer.add(state, action, reward, next_state)
        self.assertEqual(len(self.replay_buffer.enc_meta_replay_buffer[3]), 1)
        self.replay_buffer.collect_encoder_data(False)
        self.replay_buffer.add(state, action, reward, next_state)
        self.assertEqual(len(self.replay_buffer.enc_meta_replay_buffer[3]), 1)
        self.assertEqual(len(self.replay_buffer.tasks_replay_buffer[3]), 3)

        
    def test_sample_context(self):
        self.replay_buffer.set_idx(3)
        self.replay_buffer.collect_encoder_data(True)
        for _ in range(self.capacity):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)
        self.replay_buffer.set_idx(1)
        self.replay_buffer.collect_encoder_data(True)
        for _ in range(4):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)    
            
        self.replay_buffer.set_idx(2)
        self.replay_buffer.collect_encoder_data(True)
        for _ in range(20):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state) 
        samples = self.replay_buffer.sample_context([1, 2, 3], 4)    
        self.assertEqual(len(samples), 3)
        samples = self.replay_buffer.sample_context(1, 4)    
        self.assertEqual(len(samples), 4)
        # samples = self.replay_buffer.sample_context(1, 5)    
        # self.assertEqual(len(samples), 4)
        
    def test_sample(self):
        self.replay_buffer.set_idx(3)
        for _ in range(self.capacity):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)
        self.replay_buffer.set_idx(1)
        for _ in range(4):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)    
            
        self.replay_buffer.set_idx(2)
        for _ in range(20):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state) 
        state, action, reward, next_state = self.replay_buffer.sample([1, 2, 3], 4)    
        self.assertEqual(state.data.shape, (4*3,3,2))
        self.assertEqual(action.data.shape, (4*3,3))
        self.assertEqual(reward.data.shape, (4*3,1))    
        
        state, action, reward, next_state = self.replay_buffer.sample(1, 1)
        self.assertEqual(state.data.shape, (1,3,2))
        self.assertEqual(action.data.shape, (1,3))
        self.assertEqual(reward.data.shape, (1,1))        
        
        
    def test_set_idx(self):
        self.replay_buffer.set_idx(3)
        self.replay_buffer.collect_encoder_data(True)
        for _ in range(self.capacity):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)
        self.replay_buffer.set_idx(3)
        self.replay_buffer.collect_encoder_data(True)
        for _ in range(4):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state) 
        self.assertEqual(len(self.replay_buffer.enc_meta_replay_buffer[3]), 4)       
        
    def test_sample_dict_replay_buffer(self):
        self.replay_buffer.set_idx(3)
        for _ in range(self.capacity):
            state = {'obs1': np.random.rand(3,2),
                     'obs2': np.random.rand(2,)}
            action = np.random.rand(3,)
            reward = 1.3
            next_state = {'obs1': np.random.rand(3,2),
                          'obs2': np.random.rand(2,)}
            self.replay_buffer.add(state, action, reward, next_state)
        self.replay_buffer.set_idx(1)
        for _ in range(4):
            state = {'obs1': np.random.rand(3,2),
                     'obs2': np.random.rand(2,)}
            action = np.random.rand(3,)
            reward = 1.3
            next_state = {'obs1': np.random.rand(3,2),
                          'obs2': np.random.rand(2,)}
            self.replay_buffer.add(state, action, reward, next_state) 
        self.replay_buffer.set_idx(2)
        for _ in range(40):
            state = {'obs1': np.random.rand(3,2),
                     'obs2': np.random.rand(2,)}
            action = np.random.rand(3,)
            reward = 1.3
            next_state = {'obs1': np.random.rand(3,2),
                          'obs2': np.random.rand(2,)}
            self.replay_buffer.add(state, action, reward, next_state)                         
        state, action, reward, next_state = self.replay_buffer.sample([1, 2, 3], 4)
        self.assertEqual(state['obs1'].data.shape, (4*3,3,2))
        self.assertEqual(state['obs2'].data.shape, (4*3,2))
        self.assertEqual(action.data.shape, (4*3,3))
        self.assertEqual(reward.data.shape, (4*3,1))   
        
        state, action, reward, next_state = self.replay_buffer.sample(1, 4)
        self.assertEqual(state['obs1'].data.shape, (4,3,2))
        self.assertEqual(state['obs2'].data.shape, (4,2))
        self.assertEqual(action.data.shape, (4,3))
        self.assertEqual(reward.data.shape, (4,1)) 
        
        self.assertFalse(torch.equal(state['obs1'], next_state['obs1']))
        self.assertFalse(torch.equal(state['obs2'], next_state['obs2']))

