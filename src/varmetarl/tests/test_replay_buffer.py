from pearl.replay_buffer import ReplayBuffer
import unittest
import numpy as np
import torch

class TestReplayBuffer(unittest.TestCase):
    def setUp(self) -> None:
        self.capacity = 10
        self.init_step = 1
        self.device = "cpu"
        self.replay_buffer = ReplayBuffer(self.capacity, self.init_step)
        
    def test_get_init_step(self):
        self.assertEqual(self.replay_buffer.get_init_step(), self.init_step)

    def test_add_sample_numpy(self):
        state = np.random.rand(3,2)
        action = np.random.rand(3,)
        reward = np.random.rand(1,)
        next_state = np.random.rand(3,2)
        self.replay_buffer.add(state, action, reward, next_state)
        self.assertIsNotNone(self.replay_buffer.buffer)
        self.replay_buffer.add(state, action, reward, next_state)
        self.assertEqual(self.replay_buffer.position, 2)
        
    def test_sample_replay_buffer(self):
        for _ in range(self.capacity):
            state = np.random.rand(3,2)
            action = np.random.rand(3,)
            reward = 1.3
            next_state = np.random.rand(3,2)
            self.replay_buffer.add(state, action, reward, next_state)
        state, action, reward, next_state = self.replay_buffer.sample(4)
        self.assertEqual(state.data.shape, (4,3,2))
        self.assertEqual(action.data.shape, (4,3))
        self.assertEqual(reward.data.shape, (4,1))   
        
        state, action, reward, next_state = self.replay_buffer.sample(1)
        self.assertEqual(state.data.shape, (1,3,2))
        self.assertEqual(action.data.shape, (1,3))
        self.assertEqual(reward.data.shape, (1,1))        
        
        
    def test_clear_replay_buffer(self):
        self.replay_buffer.clear()
        self.assertTrue(not self.replay_buffer.buffer)
        
    def test_sample_dict_replay_buffer(self):
        for _ in range(self.capacity):
            state = {'obs1': np.random.rand(3,2),
                     'obs2': np.random.rand(2,)}
            action = np.random.rand(3,)
            reward = 1.3
            next_state = {'obs1': np.random.rand(3,2),
                          'obs2': np.random.rand(2,)}
            self.replay_buffer.add(state, action, reward, next_state)
        state, action, reward, next_state = self.replay_buffer.sample(5)
        self.assertEqual(state['obs1'].data.shape, (5,3,2))
        self.assertEqual(state['obs2'].data.shape, (5,2))
        self.assertEqual(action.data.shape, (5,3))
        self.assertEqual(reward.data.shape, (5,1))   
        
        state, action, reward, next_state = self.replay_buffer.sample(1)
        self.assertEqual(state['obs1'].data.shape, (1,3,2))
        self.assertEqual(state['obs2'].data.shape, (1,2))
        self.assertEqual(action.data.shape, (1,3))
        self.assertEqual(reward.data.shape, (1,1)) 
        
        self.assertFalse(torch.equal(state['obs1'], next_state['obs1']))
        self.assertFalse(torch.equal(state['obs2'], next_state['obs2']))

